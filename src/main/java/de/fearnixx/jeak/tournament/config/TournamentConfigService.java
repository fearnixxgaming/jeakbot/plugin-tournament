package de.fearnixx.jeak.tournament.config;

import de.mlessmann.confort.api.IConfig;
import de.mlessmann.confort.api.except.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class TournamentConfigService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TournamentConfigService.class);

    private IConfig config;

    private int leaderTokenTimeout;

    private int tournamentContainerChannel;
    private int channelGroupOfTournamentParticipants;
    private int waitingChannel;
    private int hosterServerGroup;

    private String tokenMessage;
    private static final String TOKEN_MESSAGE_DEFAULT = "%[token]";

    public TournamentConfigService(IConfig config) {
        this.config = config;
        refresh();
    }

    public void refresh() {
        try {
            config.load();
        } catch (IOException | ParseException e) {
            LOGGER.warn("Failed to load config", e);
            return;
        }

        this.leaderTokenTimeout = getIntegerNode("leaderTokenTimeout");
        this.tournamentContainerChannel = getIntegerNode("tournamentContainerChannel");
        this.channelGroupOfTournamentParticipants =
                getIntegerNode("channelGroupOfTournamentParticipants");
        this.waitingChannel = getIntegerNode("waitingChannel");
        this.hosterServerGroup = getIntegerNode("hosterServerGroup");
        this.tokenMessage = getStringNode("tokenMessage");
    }

    private int getIntegerNode(String nodeId) {
        return config.getRoot().getNode(nodeId).asInteger();
    }

    private String getStringNode(String nodeId) {
        return config.getRoot().getNode(nodeId).optString().orElse(TOKEN_MESSAGE_DEFAULT);
    }

    public int getLeaderTokenTimeout() {
        return leaderTokenTimeout;
    }

    public int getTournamentContainerChannel() {
        return tournamentContainerChannel;
    }

    public int getChannelGroupOfTournamentParticipants() {
        return channelGroupOfTournamentParticipants;
    }

    public int getWaitingChannel() {
        return waitingChannel;
    }

    public int getHosterServerGroup() {
        return hosterServerGroup;
    }

    public String getTokenMessage() {
        return tokenMessage;
    }
}
