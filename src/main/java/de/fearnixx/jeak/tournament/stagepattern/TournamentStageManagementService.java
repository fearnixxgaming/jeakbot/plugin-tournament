package de.fearnixx.jeak.tournament.stagepattern;

import de.fearnixx.jeak.tournament.entities.tournament.Tournament;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentParameter;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.atn.PredictionMode;

import java.io.IOException;
import java.io.StringReader;
import java.util.*;
import java.util.function.Supplier;

public class TournamentStageManagementService {

    private final Map<Integer, List<Stage>> tournamentStageMap = new HashMap<>();
    private static final Supplier<IllegalArgumentException> stagePatternMissing =
            () -> new IllegalArgumentException("The tournament is missing a stage pattern!");

    public void registerTournament(Tournament tournament) {
        synchronized (tournamentStageMap) {
            tournamentStageMap.put(
                    tournament.getId(),
                    createStages(tournament.optParameterValue(TournamentParameter.STAGE_PATTERN).orElseThrow(
                            stagePatternMissing
                    ))
            );
        }
    }

    public void removeTournament(Tournament tournament) {
        synchronized (tournamentStageMap) {
            tournamentStageMap.remove(tournament.getId());
        }
    }

    public void clearCache() {
        synchronized (tournamentStageMap) {
            tournamentStageMap.clear();
        }
    }

    public Optional<Stage> getCurrentTournamentStage(Tournament tournament) {
        return getTournamentStage(tournament, false);
    }

    public Optional<Stage> getTournamentStage(Tournament tournament, boolean nextStage) {
        synchronized (tournamentStageMap) {

            List<Stage> stages;
            if (tournamentStageMap.containsKey(tournament.getId())) {
                stages = tournamentStageMap.get(tournament.getId());
            } else {
                stages = createStages(tournament.optParameterValue(TournamentParameter.STAGE_PATTERN)
                        .orElseThrow(stagePatternMissing));
                tournamentStageMap.put(tournament.getId(), stages);
            }

            final de.fearnixx.jeak.tournament.entities.stage.Stage currentStage = tournament.getCurrentStage();
            int index = 0;

            if (currentStage != null) {
                index = tournament.getStages().indexOf(currentStage) + (nextStage ? 1 : 0);
            }

            if (index < stages.size()) {
                return Optional.of(stages.get(index));
            }

            Stage lastStage = stages.get(stages.size() - 1);
            if (lastStage.isInfinite()) {
                return Optional.of(lastStage);
            } else {
                return Optional.empty();
            }
        }
    }

    private List<Stage> createStages(String pattern) {
        CharStream charStream;
        try {
            charStream = CharStreams.fromReader(new StringReader(pattern));
        } catch (IOException e) {
            throw new IllegalStateException("Failed to create CharStream!");
        }

        StagePatternLexer lexer = new StagePatternLexer(charStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        StagePatternParser parser = new StagePatternParser(tokens);

        parser.getInterpreter().setPredictionMode(PredictionMode.SLL);

        StagePatternParser.StagePatternContext context = parser.stagePattern();

        List<Stage> stages = new ArrayList<>();
        StageVisitor visitor = new StageVisitor(stages::add);

        visitor.visit(context);

        return stages;
    }
}