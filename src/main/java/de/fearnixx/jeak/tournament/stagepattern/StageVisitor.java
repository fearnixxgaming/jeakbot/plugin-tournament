package de.fearnixx.jeak.tournament.stagepattern;

import org.antlr.v4.runtime.tree.ParseTree;

import de.fearnixx.jeak.tournament.stagepattern.StagePatternBaseVisitor;
import de.fearnixx.jeak.tournament.stagepattern.StagePatternParser;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class StageVisitor extends StagePatternBaseVisitor<Void> {

    private Consumer<Stage> stageConsumer;

    public StageVisitor(Consumer<Stage> stageConsumer) {
        this.stageConsumer = stageConsumer;
    }

    @Override
    public Void visit(ParseTree tree) {
        if (tree instanceof StagePatternParser.StagePatternContext) {
            return visitStagePattern((StagePatternParser.StagePatternContext) tree);
        }

        throw new IllegalArgumentException("Provided tree is not valid.");
    }

    @Override
    public Void visitStagePattern(StagePatternParser.StagePatternContext ctx) {
        ctx.stageDef().stream().map(this::createStage).forEach(stageConsumer);
        return null;
    }

    private Stage createStage(StagePatternParser.StageDefContext stageDefContext) {
        StagePatternParser.StageContext stageCtx = stageDefContext.stage();

        String identifier = stageCtx.Identifier().getText();
        boolean isInfinite = stageDefContext.Asterisk() != null;

        List<Integer> params = new ArrayList<>();

        if (stageCtx.params() != null) {

            stageCtx.params().Number()
                    .stream()
                    .map(ParseTree::getText)
                    .map(Integer::parseInt)
                    .forEach(params::add);
        }

        return new Stage(identifier, isInfinite, params);
    }

}
