package de.fearnixx.jeak.tournament.stagepattern;

import de.fearnixx.jeak.tournament.encounter.AbstractStageCreator;
import de.fearnixx.jeak.tournament.encounter.StageCreatorFactory;

import java.util.List;

public class Stage {

    private String type;
    private AbstractStageCreator encounterFactory;
    private boolean infinite;
    private List<Integer> params;

    public Stage(String type, boolean infinite, List<Integer> params) {
        this.type = type;
        this.infinite = infinite;
        this.params = params;
        encounterFactory = StageCreatorFactory.getEncounterFactory(type, params);
    }

    public AbstractStageCreator getEncounterFactory() {
        return encounterFactory;
    }

    public boolean isInfinite() {
        return infinite;
    }

    public String getType() {
        return type;
    }

    public List<Integer> getParams() {
        return params;
    }
}
