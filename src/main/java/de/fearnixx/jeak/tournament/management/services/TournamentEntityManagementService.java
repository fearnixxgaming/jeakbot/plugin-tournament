package de.fearnixx.jeak.tournament.management.services;

import de.fearnixx.jeak.tournament.entities.Season;
import de.fearnixx.jeak.tournament.entities.encounter.Encounter;
import de.fearnixx.jeak.tournament.entities.team.Player;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.Tournament;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentParticipation;
import de.fearnixx.jeak.tournament.enums.ParticipationState;
import de.fearnixx.jeak.tournament.enums.TournamentState;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TournamentEntityManagementService {

    private static final String SELECT_TEAM = "SELECT t FROM Team t ";
    private static final String SELECT_TOURNAMENT = "SELECT t FROM Tournament t ";
    private static final String WHERE_NAME = "WHERE name = :name";

    private EntityManager entityManager;

    private DeleteLeaderTokenService deleteLeaderTokenService;
    private TypedQuery<Team> teamByNameQuery;
    private TypedQuery<Team> teamByTagQuery;
    private TypedQuery<Tournament> runningTournamentQuery;
    private TypedQuery<Tournament> tournamentByNameQuery;
    private TypedQuery<Season> seasonByNameQuery;
    private TypedQuery<Team> createdTeamsQuery;
    private TypedQuery<Encounter> encounterByIdQuery;
    private TypedQuery<Player> playerByIdQuery;

    public TournamentEntityManagementService(EntityManager entityManager) {
        this.entityManager = entityManager;
        deleteLeaderTokenService = new DeleteLeaderTokenService(this.getEntityManager());

        teamByNameQuery = entityManager.createQuery(SELECT_TEAM + WHERE_NAME, Team.class);
        teamByTagQuery = entityManager.createQuery(SELECT_TEAM + "WHERE tag = :tag", Team.class);
        runningTournamentQuery = entityManager.createQuery(SELECT_TOURNAMENT + "WHERE state = :state", Tournament.class);
        tournamentByNameQuery = entityManager.createQuery(SELECT_TOURNAMENT + WHERE_NAME, Tournament.class);
        seasonByNameQuery = entityManager.createQuery("SELECT s FROM Season s " + WHERE_NAME, Season.class);
        createdTeamsQuery = entityManager.createQuery(SELECT_TEAM + "WHERE creatorUuid = :creatorUuid", Team.class);
        encounterByIdQuery = entityManager.createQuery("SELECT e FROM Encounter e WHERE id = :id", Encounter.class);
        playerByIdQuery = entityManager.createQuery("SELECT p FROM Player p WHERE id = :id", Player.class);
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public boolean beginTransaction() {
        boolean transactionAlreadyActive = isTransactionAlreadyActive();

        if (!transactionAlreadyActive) {
            entityManager.getTransaction().begin();
        }

        return transactionAlreadyActive;
    }

    public void rollbackTransaction() {
        if (entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().rollback();
        }
    }

    private boolean isTransactionAlreadyActive() {
        return entityManager.getTransaction().isActive();
    }

    public Optional<Team> getTeamByName(String name) {
        teamByNameQuery.setParameter("name", name);
        return getUniqueResult(teamByNameQuery);
    }

    public Optional<Team> getTeamByTag(String tag) {
        teamByTagQuery.setParameter("tag", tag);
        return getUniqueResult(teamByTagQuery);
    }

    public List<Tournament> getRunningTournaments() {
        runningTournamentQuery.setParameter("state", TournamentState.STARTED);
        return runningTournamentQuery.getResultList();
    }

    public Optional<Tournament> getTournamentByName(String tournamentName) {
        tournamentByNameQuery.setParameter("name", tournamentName);
        return getUniqueResult(tournamentByNameQuery);
    }

    public Optional<Season> getSeasonByName(String seasonName) {
        seasonByNameQuery.setParameter("name", seasonName);
        return getUniqueResult(seasonByNameQuery);
    }

    public Optional<Encounter> getEncounterById(int id) {
        encounterByIdQuery.setParameter("id", id);
        return encounterByIdQuery.getResultList().stream().findFirst();
    }

    public Optional<Player> getPlayerById(int id) {
        playerByIdQuery.setParameter("id", id);
        return playerByIdQuery.getResultList().stream().findFirst();
    }

    public DeleteLeaderTokenService getDeleteLeaderTokenService() {
        return deleteLeaderTokenService;
    }

    private <T> Optional<T> getUniqueResult(TypedQuery<T> query) {
        List<T> results = query.getResultList();
        return results.size() == 1 ? Optional.of(results.get(0)) : Optional.empty();
    }

    public int getCreatedTeamsByUser(String uuid) {
        createdTeamsQuery.setParameter("creatorUuid", uuid);
        return createdTeamsQuery.getResultList().size();
    }

    public boolean checkActiveTournaments(Team team) {
        final List<Tournament> activeTournaments = getRunningTournaments().stream()
                .filter(t -> t.getTeams().contains(team))
                .collect(Collectors.toList());

        if (activeTournaments.isEmpty()) {
            return false;
        }

        return activeTournaments.stream()
                .map(t -> t.optTournamentParticipation(team))
                .map(Optional::orElseThrow)
                .map(TournamentParticipation::getState)
                .anyMatch(state -> state == ParticipationState.ACTIVE);
    }
}
