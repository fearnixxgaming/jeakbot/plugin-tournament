package de.fearnixx.jeak.tournament.management.services;

import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.permission.base.IPermission;
import de.fearnixx.jeak.service.permission.base.IPermissionService;
import de.fearnixx.jeak.service.permission.teamspeak.ITS3PermissionProvider;
import de.fearnixx.jeak.service.teamspeak.IUserService;
import de.fearnixx.jeak.teamspeak.IServer;
import de.fearnixx.jeak.teamspeak.PermissionSID;
import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.QueryCommands;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IChannel;
import de.fearnixx.jeak.teamspeak.data.IDataHolder;
import de.fearnixx.jeak.teamspeak.data.IUser;
import de.fearnixx.jeak.teamspeak.query.IQueryRequest;
import de.fearnixx.jeak.teamspeak.query.QueryBuilder;
import de.fearnixx.jeak.tournament.config.TournamentConfigService;
import de.fearnixx.jeak.tournament.entities.team.Player;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.Tournament;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class TournamentChannelManagementService {

    private static final String TOURNAMENT_CH_TOPIC_PREFIX = "[TOURNAMENT_ID:";
    private static final Logger logger = LoggerFactory.getLogger(TournamentChannelManagementService.class);

    @Inject
    private IServer server;

    @Inject
    private IDataCache cache;

    @Inject
    private TournamentConfigService configContainer;

    @Inject
    private IPermissionService permService;

    @Inject
    private TournamentEntityManagementService entityManagement;

    @Inject
    private IUserService userService;

    private static final String CHANNEL_CREATE_CMD = "channelcreate";
    private static final String CHANNEL_NAME_PROP = "channel_name";

    private Logger log = LoggerFactory.getLogger(TournamentChannelManagementService.class);

    private Map<Integer, TournamentChannelInfo> tournamentChannelInfoMap = new HashMap<>();

    public void createTournamentChannel(Tournament tournament, int parentChannelId) {
        QueryBuilder queryBuilder = IQueryRequest.builder()
                .command(CHANNEL_CREATE_CMD)
                .addKey(CHANNEL_NAME_PROP, "[ - " + tournament.getName() + " - ]")
                .addKey("cpid", parentChannelId)
                .addKey(PropertyKeys.Channel.TOPIC, TOURNAMENT_CH_TOPIC_PREFIX + tournament.getId() + "]")
                .addKey(PropertyKeys.Channel.MAX_CLIENTS, 0)
                .addKey(PropertyKeys.Channel.FLAG_PERMANENT, 1);

        server.getConnection().sendRequest(
                queryBuilder
                        .onSuccess(answer -> {

                                    int channelId = getChannelIdFromAnswer(answer);

                                    TournamentChannelInfo tournamentChannelInfo = new TournamentChannelInfo(channelId);

                                    updateChannelPermission(channelId);
                                    createLobbyChannel(tournamentChannelInfo);

                                    setChannelGroupForTournamentLeaders(tournament);

                                    tournamentChannelInfoMap.put(tournament.getId(), tournamentChannelInfo);
                                }
                        )
                        .onError(error -> log.error("Couldn't create tournament-channel! {}", error.getError()))
                        .build()
        );
    }

    public void createWaitingAndRegisteringChannel(Tournament tournament, int parentChannelId) {
        QueryBuilder queryBuilder = IQueryRequest.builder()
                .command(CHANNEL_CREATE_CMD)
                .addKey(CHANNEL_NAME_PROP, "[Waiting Registration: " + tournament.getName() + "]")
                .addKey("cpid", parentChannelId)
                .addKey(PropertyKeys.Channel.TOPIC, TOURNAMENT_CH_TOPIC_PREFIX + tournament.getId() + ",W]")
                .addKey(PropertyKeys.Channel.MAX_CLIENTS, 0)
                .addKey(PropertyKeys.Channel.TALK_POWER, 50)
                .addKey(PropertyKeys.Channel.FLAG_PERMANENT, 1);

        server.getConnection().sendRequest(
                queryBuilder.onError(
                        answer -> log.error("Failed to create waiting channel! {}, {}",
                                answer.getErrorCode(),
                                answer.getErrorMessage())).build()
        );

        createRegistrationChannel(tournament, parentChannelId, "[Registration: ");
        createRegistrationChannel(tournament, parentChannelId, "[Waiting Start: ");
    }

    private void createRegistrationChannel(Tournament tournament, int parentChannelId, String prefix) {
        QueryBuilder queryBuilder;
        queryBuilder = IQueryRequest.builder()
                .command(CHANNEL_CREATE_CMD)
                .addKey(CHANNEL_NAME_PROP, prefix + tournament.getName() + "]")
                .addKey("cpid", parentChannelId)
                .addKey(PropertyKeys.Channel.TOPIC, TOURNAMENT_CH_TOPIC_PREFIX + tournament.getId() + ",R]")
                .addKey(PropertyKeys.Channel.MAX_CLIENTS, 0)
                .addKey(PropertyKeys.Channel.FLAG_PERMANENT, 1);

        server.getConnection().sendRequest(
                queryBuilder.onError(
                        answer -> log.error("Failed to create registration channel! {}, {}",
                                answer.getErrorCode(), answer.getErrorMessage())
                )
                        .onSuccess(answer -> {
                                    final String id = answer.getDataChain().get(0).getProperty(PropertyKeys.Channel.ID)
                                            .orElseThrow(
                                                    () -> new IllegalStateException("Answer from channel creation contained no ID!")
                                            );

                                    ITS3PermissionProvider ts3Provider = permService.getTS3Provider();

                                    final IQueryRequest permRequest = IQueryRequest.builder()
                                            .command(
                                                    QueryCommands.PERMISSION.CHANNEL_PERMISSION_ADD
                                            )
                                            .addKey(PropertyKeys.Channel.ID, id)
                                            .addKey("permid", ts3Provider.translateSID("i_channel_needed_join_power"))
                                            .addKey("permvalue", 50)
                                            .onSuccess(s -> log.debug("Channel permission updated."))
                                            .onError(e -> log.error("Couldn't update channel permissions")).build();

                                    server.getConnection().sendRequest(permRequest);
                                }
                        )
                        .build()
        );
    }

    public void deleteTournamentChannel(Tournament tournament) {
        getTournamentChannelInfo(tournament).ifPresent(
                t -> cache.getChannels().stream()
                        .filter(c -> c.getID().equals(t.getTournamentChannelId()))
                        .findFirst()
                        .ifPresent(c -> server.getConnection().sendRequest(c.delete(true)))
        );

        tournamentChannelInfoMap.remove(tournament.getId());
    }

    public void setChannelGroupForTournamentLeaders(Tournament tournament) {
        int channelGroupId = configContainer.getChannelGroupOfTournamentParticipants();

        tournament.getLeaders()
                .stream()
                .map(this::optUserByUuid)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(
                        client -> {
                            final Optional<TournamentChannelInfo> tournamentChannelInfo = getTournamentChannelInfo(tournament);
                            tournamentChannelInfo.ifPresent(channelInfo -> {
                                setChannelGroup(client, channelInfo.getTournamentChannelId(), channelGroupId);
                                setChannelGroup(client, channelInfo.getLobbyChannelId(), channelGroupId);
                            });
                        }
                );
    }


    public void setChannelGroupForTeamOfTournament(Tournament tournament, Team team) {
        int channelGroupId = configContainer.getChannelGroupOfTournamentParticipants();

        team.getMembersUid().stream()
                .map(this::optUserByUuid)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(
                        client -> {
                            final Optional<TournamentChannelInfo> tournamentChannelInfo = getTournamentChannelInfo(tournament);
                            tournamentChannelInfo.ifPresent(channelInfo -> {
                                setChannelGroup(client, channelInfo.getTournamentChannelId(), channelGroupId);
                                setChannelGroup(client, channelInfo.getLobbyChannelId(), channelGroupId);
                            });
                        }
                );
    }

    private String getTeamName(Team team) {
        String teamName = team.getName();
        if (team.isSinglePlayer()) {
            final Player player = team.getMembers().get(0);
            teamName = player.optName().orElse(
                    player.optTsUid()
                            .map(
                                    tsUid -> userService.findUserByUniqueID(tsUid).stream().findFirst()
                                            .map(IUser::getNickName).orElse(tsUid)
                            ).orElse("Unknown player")
            );
        }

        return teamName;
    }

    public void createTeamChannel(Tournament tournament, Team team) {
        Optional<TournamentChannelInfo> tournamentChannelInfo = getTournamentChannelInfo(tournament);

        if (tournamentChannelInfo.isEmpty()) {
            return;
        }

        QueryBuilder queryBuilder = IQueryRequest.builder()
                .command(CHANNEL_CREATE_CMD)
                .addKey(CHANNEL_NAME_PROP, "[" + getTeamName(team) + "]")
                .addKey("cpid", tournamentChannelInfo.get().getTournamentChannelId())
                .addKey(PropertyKeys.Channel.TOPIC, "[TEAM_ID:" + team.getId() + "]")
                .addKey(PropertyKeys.Channel.FLAG_PERMANENT, 1);

        server.getConnection().sendRequest(
                queryBuilder
                        .onSuccess(answer -> {

                                    int channelId = getChannelIdFromAnswer(answer);
                                    updateChannelPermission(channelId);

                                    int channelGroupId = configContainer.getChannelGroupOfTournamentParticipants();

                                    team.getMembersUid()
                                            .stream()
                                            .map(this::optUserByUuid)
                                            .filter(Optional::isPresent)
                                            .map(Optional::get)
                                            .forEach(
                                                    client -> setChannelGroup(client, channelId, channelGroupId)
                                            );

                                    tournament.getLeaders()
                                            .stream()
                                            .map(this::optUserByUuid)
                                            .filter(Optional::isPresent)
                                            .map(Optional::get)
                                            .forEach(
                                                    leader -> setChannelGroup(leader, channelId, channelGroupId)
                                            );

                                    getTournamentChannelInfo(tournament).ifPresent(
                                            info -> info.addTeamChannel(team, channelId)
                                    );
                                }
                        )
                        .onError(
                                error -> log.error("Couldn't create team-channel! {}", error.getError())
                        )
                        .build()
        );
    }

    private void createLobbyChannel(TournamentChannelInfo tournamentChannelInfo) {
        QueryBuilder queryBuilder = IQueryRequest.builder()
                .command(CHANNEL_CREATE_CMD)
                .addKey(CHANNEL_NAME_PROP, "Lobby")
                .addKey("cpid", tournamentChannelInfo.getTournamentChannelId())
                .addKey(PropertyKeys.Channel.FLAG_PERMANENT, 1);

        queryBuilder.onSuccess(answer -> {
            final int channelIdFromAnswer = getChannelIdFromAnswer(answer);

            tournamentChannelInfo.setLobbyChannelId(channelIdFromAnswer);
            updateChannelPermission(channelIdFromAnswer);
            log.debug("Lobby-Channel was created successfully");
        });
        queryBuilder.onError(answer -> log.error("Lobby-Channel couldn't be created"));

        server.getConnection().sendRequest(queryBuilder.build());
    }

    private void updateChannelPermission(int channelId) {
        ITS3PermissionProvider ts3Provider = permService.getTS3Provider();


        int participantChannelGroup = configContainer.getChannelGroupOfTournamentParticipants();
        if (participantChannelGroup < 0) {
            logger.debug("No channel permission management required: No channel group defined.");
            return;
        }

        var optPermValue = ts3Provider
                .getChannelGroupPermission(participantChannelGroup, PermissionSID.CHANNEL_JOIN_POWER.getPermSID())
                .map(IPermission::getValue);

        if (optPermValue.isEmpty()) {
            logger.info("{} is not set. Not setting needed permission.", PermissionSID.CHANNEL_JOIN_POWER.getPermSID());
            return;
        }

        server.getConnection().sendRequest(
                IQueryRequest.builder().command("channeladdperm")
                        .addKey(PropertyKeys.Channel.ID, channelId)
                        .addKey("permid", ts3Provider.translateSID(PermissionSID.CHANNEL_NEEDED_JOIN_POWER.getPermSID()))
                        .addKey("permvalue", optPermValue.get())
                        .onSuccess(s -> log.debug("Channel permission updated."))
                        .onError(e -> log.error("Couldn't update channel permissions")).build()
        );
    }

    private int getChannelIdFromAnswer(IQueryEvent.IAnswer answer) {
        List<IDataHolder> dataChain = answer.getDataChain();
        if (dataChain.isEmpty()) {
            throw new IllegalStateException("Datachain cannot be empty!");
        }

        return Integer.parseInt(
                dataChain.get(0)
                        .getProperty(PropertyKeys.Channel.ID)
                        .orElseThrow(() ->
                                new IllegalStateException(
                                        "Channel-ID-Property not contained in answer!"))
        );
    }

    private void setChannelGroup(IUser user, int channelId, int channelGroupId) {
        server.getConnection()
                .sendRequest(
                        user.setChannelGroup(channelId, channelGroupId)
                );
    }

    private Optional<TournamentChannelInfo> getTournamentChannelInfo(Tournament tournament) {
        if (tournamentChannelInfoMap.containsKey(tournament.getId())) {
            return Optional.of(tournamentChannelInfoMap.get(tournament.getId()));
        }

        List<IChannel> tournamentChannels =
                cache.getChannels()
                        .stream()
                        .filter(c -> c.getTopic().equals(TOURNAMENT_CH_TOPIC_PREFIX + tournament.getId().toString() + "]"))
                        .collect(Collectors.toList());

        if (tournamentChannels.size() == 1) {
            log.debug("Found channel for tournament with id {}", tournament.getId());

            final IChannel tournamentChannel = tournamentChannels.get(0);

            TournamentChannelInfo tournamentChannelInfo = new TournamentChannelInfo(tournamentChannel.getID());

            tournamentChannelInfoMap.put(tournament.getId(), tournamentChannelInfo);
            return Optional.of(tournamentChannelInfo);
        }

        log.warn("Couldn't find channel for tournament with id {}", tournament.getId());

        return Optional.empty();
    }

    public Optional<IChannel> getTeamChannel(Tournament tournament, Team team) {

        if (tournamentChannelInfoMap.containsKey(tournament.getId())) {
            Optional<Integer> teamChannelId = tournamentChannelInfoMap.get(tournament.getId()).getTeamChannel(team);
            if (teamChannelId.isPresent()) {
                return cache.getChannels().stream().filter(c -> c.getID().equals(teamChannelId.get())).findFirst();
            }
        }

        List<IChannel> teamChannels =
                cache.getChannels()
                        .stream()
                        .filter(c -> c.getTopic().equals("[TEAM_ID:" + team.getId().toString() + "]"))
                        .filter(c ->
                                cache.getChannels().stream()
                                        .filter(ch -> ch.getID().equals(c.getParent()))
                                        .anyMatch(ch -> ch.getTopic().equals(TOURNAMENT_CH_TOPIC_PREFIX + tournament.getId().toString() + "]"))
                        )
                        .collect(Collectors.toList());

        if (teamChannels.size() == 1) {
            log.debug("Found channel for team with id {} in tournament with id {}", team.getId(), tournament.getId());
            IChannel teamChannel = teamChannels.get(0);

            getTournamentChannelInfo(tournament)
                    .orElseThrow(() -> new IllegalStateException("No tournament channel info!"))
                    .addTeamChannel(team, teamChannel.getID());

            return Optional.of(teamChannel);
        }


        log.warn("Couldn't find channel for team with id {} in tournament with id {}", team.getId(), tournament.getId());

        return Optional.empty();
    }

    private Optional<IUser> optUserByUuid(String uuid) {
        return userService.findUserByUniqueID(uuid).stream().findFirst();
    }
}
