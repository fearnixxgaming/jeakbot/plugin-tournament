package de.fearnixx.jeak.tournament.management;

import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.data.IUser;
import de.fearnixx.jeak.tournament.entities.encounter.Encounter;
import de.fearnixx.jeak.tournament.entities.encounter.EncounterParticipation;
import de.fearnixx.jeak.tournament.entities.encounter.Match;
import de.fearnixx.jeak.tournament.entities.encounter.MatchScore;
import de.fearnixx.jeak.tournament.entities.team.Player;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.Tournament;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentParameter;
import de.fearnixx.jeak.tournament.enums.EncounterState;
import de.fearnixx.jeak.tournament.enums.StageState;

import javax.persistence.EntityManager;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EncounterManagement extends AbstractManagement {

    private static final String NOT_ALLOWED = "Only tournament or team leaders are allowed to use this command!";
    private static final String TOURNAMENT_MSG_PREFIX = "[Tournament - ";
    private static final String ENCOUNTER_KEY = "encounter";
    private static final String STAGE_STATE_KEY = "stage-state";
    private static final String READY_STATE_KEY = "ready-state";
    private static final String POINTS_KEY = "field:points";
    private static final String MATCH_ID_KEY = "field:match-id";

    private static final String WON_MESSAGE = "You won the encounter!";
    private static final String LOST_MESSAGE = "You lost the encounter!";
    private static final String DRAWN_MESSAGE = "You drew the encounter!";

    public Map<String, List<String>> encounterInfo(String uuid, Encounter encounter) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        Set<String> allowedUsers = encounter.getTeams().stream().flatMap(
                team -> Stream.concat(team.getLeaders().stream(), team.getMembersUid().stream())
        ).collect(Collectors.toSet());

        if (!allowedUsers.contains(uuid)) {
            return getNotAllowedResponse();
        }

        List<EncounterParticipation> participations = encounter.getEncounterParticipations();
        participations.sort(Comparator.comparing(p -> p.getTeam().getName()));

        String hoster = encounter.getHoster();
        String hosterString = "";

        if (hoster != null) {
            hosterString = "with the hoster '" + hoster + "'";
        }

        StringBuilder stringBuilder = new StringBuilder(
                "The encounter is '" + encounter.getState() + "' " + hosterString + " and these teams are participating:"
        );

        participations.forEach(p -> {
            stringBuilder.append("\n");
            stringBuilder.append(p.getTeam().getName());
            stringBuilder.append(": ");
            stringBuilder.append("Matches won: ").append(p.getMatchesWon()).append(" - Rounds won: ").append(p.getRoundsWon());
        });

        messagingService.sendTextMessageToClient(
                uuid,
                stringBuilder.toString()
        );

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> addParticipant(String uuid, Encounter encounter, Team team) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!encounter.getStage().getTournament().isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (encounter.getTeams().contains(team)) {
            return createSingleResult(TEAM_KEY, "The team already participates in this encounter!");
        }

        if (encounter.getStage().getTournament().optParameter(TournamentParameter.SUPPRESS_PART_GEN).isEmpty()) {
            return createSingleResult(TOURNAMENT_KEY, "This tournament does not permit manual adding of participants");
        }

        if (encounter.getEncounterParticipations().size() >= 2) {
            return createSingleResult(ENCOUNTER_KEY, "The encounter contains at least two teams!");
        }

        if (encounter.getState() != EncounterState.CREATED) {
            return createSingleResult(ENCOUNTER_KEY, "The encounter is not in the CREATED state!");
        }

        encounter.createEncounterParticipation(team);
        entityManagement.getEntityManager().persist(encounter);

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> messageEncounter(String uuid, Encounter encounter, String message) {
        boolean transactionWasActive = entityManagement.beginTransaction();
        String msg = TOURNAMENT_MSG_PREFIX + encounter.getStage().getTournament().getName() + ": Encounter - "
                + encounter.getId() + "] " + message;

        Set<String> teamLeaders = encounter.getTeamLeaderUuids();

        if (!encounter.getStage().getTournament().isLeader(uuid) && !teamLeaders.contains(uuid)) {
            return getNotAllowedResponse();
        }

        encounter.getTeams().forEach(
                team -> messagingService.messageTeam(team, msg)
        );

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> setHoster(String uuid, Encounter encounter, String hoster) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!encounter.getStage().getTournament().isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        encounter.setHoster(hoster);

        final String hosterDisplay;

        final List<IClient> matchingClient = userService.findClientByUniqueID(hoster);
        if (!matchingClient.isEmpty()) {
            hosterDisplay = matchingClient.get(0).getNickName();
        } else {
            hosterDisplay = hoster;
        }

        encounter.getTeams().forEach(
                team -> messagingService.messageTeam(
                        team,
                        "The hoster '" + hosterDisplay + "' was assigned to the encounter with id " + encounter.getId()
                )
        );

        entityManagement.getEntityManager().flush();

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> startEncounter(String uuid, Encounter encounter) {
        boolean transactionWasActive = entityManagement.beginTransaction();
        List<Team> teams = encounter.getTeams();
        Set<String> teamLeaders = encounter.getTeamLeaderUuids();

        final Tournament tournament = encounter.getStage().getTournament();

        if (!tournament.isLeader(uuid) && !teamLeaders.contains(uuid)) {
            return getNotAllowedResponse();
        }

        if (!encounter.getStage().getState().equals(StageState.STARTED)) {
            return createSingleResult(STAGE_STATE_KEY, "The stage of the encounter was not started!");
        }

        final EncounterState encounterState = encounter.getState();

        if (!encounterState.equals(EncounterState.READY)) {

            if (encounterState.equals(EncounterState.CREATED)) {
                return createSingleResult(ENCOUNTER_KEY, "The encounter must be prepared and then readied up!");
            } else if (encounterState.equals(EncounterState.PREPARING)) {
                return createSingleResult(READY_STATE_KEY, "The participating teams are not ready!");
            }

            return createSingleResult(ENCOUNTER_KEY, "The encounter was already started!");
        }

        if (encounter.getStage().getEncounters().stream()
                .filter(e -> !e.getId().equals(encounter.getId()))
                .filter(e -> e.getTeams().stream().anyMatch(teams::contains))
                .anyMatch(e -> e.getState() == EncounterState.STARTED
                        || e.getState() == EncounterState.PREPARING
                        || e.getState() == EncounterState.READY)) {

            return createSingleResult(
                    STAGE_STATE_KEY,
                    "At least one team is already participating in an active encounter of this stage!"
            );
        }

        encounter.start();

        String hoster = encounter.getHoster();

        if (hoster != null) {
            final List<IClient> matchingClient = userService.findClientByUniqueID(hoster);
            if (!matchingClient.isEmpty()) {
                hoster = matchingClient.get(0).getNickName();
            }
        }

        String hosterString = (hoster == null) ? "" : "The hoster is '" + hoster + "'";
        String message = TOURNAMENT_MSG_PREFIX + tournament.getName() + "] Encounter has started. " + hosterString;

        teams.forEach(team -> messagingService.messageTeam(team, message));

        teams.forEach(team -> channelManagement.getTeamChannel(tournament, team).ifPresent(
                channel -> {
                    Map<String, String> propsToEdit = new HashMap<>();

                    propsToEdit.put(PropertyKeys.Channel.NAME, "[" + getTeamName(team) + "]" + " [INGAME]");

                    String opponents = teams.stream()
                            .filter(t -> !t.getId().equals(team.getId()))
                            .map(Team::getName)
                            .collect(Collectors.joining(","));

                    propsToEdit.put(
                            PropertyKeys.Channel.DESCRIPTION, "Ingame vs. " + opponents
                                    + System.lineSeparator() + hosterString
                    );

                    server.getConnection().sendRequest(channel.edit(propsToEdit));
                }
        ));

        entityManagement.getEntityManager().flush();

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    private String getTeamName(Team team) {
        String teamName = team.getName();
        if (team.isSinglePlayer()) {
            final Player player = team.getMembers().get(0);
            teamName = player.optName().orElse(
                    player.optTsUid()
                            .map(
                                    tsUid -> userService.findUserByUniqueID(tsUid).stream().findFirst()
                                            .map(IUser::getNickName).orElse(tsUid)
                            ).orElse("Unknown player")
            );
        }

        return teamName;
    }

    public Map<String, List<String>> addMatch(String uuid, Encounter encounter, String scoreString) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        List<EncounterParticipation> encounterParticipations = encounter.getEncounterParticipations();
        List<Team> teams = encounter.getTeams();

        Tournament tournament = encounter.getStage().getTournament();

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        Map<String, List<String>> errors = new HashMap<>();

        if (!encounter.getState().equals(EncounterState.STARTED)) {
            errors.put(ENCOUNTER_KEY, Collections.singletonList("The encounter is not started!"));
        }

        List<Integer> scores;
        try {
            scores = Arrays.stream(scoreString.split(":")).map(Integer::valueOf).collect(Collectors.toList());
        } catch (NumberFormatException e) {
            errors.put(POINTS_KEY, Collections.singletonList("The entered scores are not numerical!"));
            return errors;
        }

        if (teams.size() != scores.size()) {
            errors.put(POINTS_KEY, Collections.singletonList("There must be a score for every participating team!"));
            return errors;
        }

        if (tournament.optParameter(TournamentParameter.GAME_TIME_TRACKING).isPresent() && scores.stream().filter(s -> s > 0).count() != 1) {
            errors.put(POINTS_KEY, Collections.singletonList("There must be exactly one non-zero score in game time tracking mode!"));
            return errors;
        }

        Match match = encounter.createMatch();
        List<MatchScore> matchScores = new ArrayList<>();

        for (int i = 0; i < scores.size(); i++) {
            matchScores.add(match.createMatchScore(encounterParticipations.get(i).getTeam(), scores.get(i)));
        }

        match.setResult(matchScores.toArray(new MatchScore[0]));
        entityManagement.getEntityManager().persist(encounter);

        entityManagement.getEntityManager().flush();

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> removeMatch(String uuid, Encounter encounter, int matchId) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        Tournament tournament = encounter.getStage().getTournament();

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (!encounter.getState().equals(EncounterState.STARTED)) {
            return createSingleResult(ENCOUNTER_KEY, "The encounter is not started!");
        }

        final Optional<Match> optMatch = encounter.getMatches().stream().filter(m -> m.getId() == matchId).findFirst();

        if (optMatch.isEmpty()) {
            return createSingleResult(MATCH_ID_KEY, "The encounter does not contain a match with the given index!");
        }

        final EntityManager entityManager = entityManagement.getEntityManager();

        final Match match = optMatch.get();

        match.clearResult();
        encounter.removeMatch(match);
        entityManager.remove(match);

        entityManager.persist(encounter);

        entityManager.flush();

        if (!transactionWasActive) {
            entityManager.getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> finishEncounter(String uuid, Encounter encounter) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        List<Team> teams = encounter.getTeams();

        Tournament tournament = encounter.getStage().getTournament();

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (!encounter.getState().equals(EncounterState.STARTED)) {
            return createSingleResult(ENCOUNTER_KEY, "The encounter is not started!");
        }

        if (encounter.getMatches().isEmpty()) {
            return createSingleResult(ENCOUNTER_KEY, "The encounter must contain at least one match!");
        }

        String message = TOURNAMENT_MSG_PREFIX + tournament.getName() + "] Encounter was finished.";
        encounter.getTeams().forEach(
                team -> messagingService.messageTeam(team, message)
        );

        for (Team team : encounter.getTeams()) {
            final EncounterParticipation ep = encounter.getEncounterParticipation(team);

            if (ep.isEncounterWon()) {
                messagingService.messageTeam(team, WON_MESSAGE);
            } else if (ep.isEncounterLost()) {
                messagingService.messageTeam(team, LOST_MESSAGE);
            } else if (ep.isEncounterDrawn()) {
                messagingService.messageTeam(team, DRAWN_MESSAGE);
            }
        }

        encounter.finish();

        final EntityManager entityManager = entityManagement.getEntityManager();

        teams.forEach(
                team -> channelManagement.getTeamChannel(tournament, team).ifPresent(
                        channel -> {
                            Map<String, String> propsToEdit = new HashMap<>();
                            propsToEdit.put(PropertyKeys.Channel.NAME, "[" + team.getName() + "]");
                            propsToEdit.put(PropertyKeys.Channel.DESCRIPTION, "");
                            server.getConnection().sendRequest(channel.edit(propsToEdit));
                        })
        );

        encounterGenerationService.processStageState(tournament);

        entityManager.flush();
        if (!transactionWasActive) {
            entityManager.getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> prepareEncounter(String uuid, Encounter encounter) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!encounter.getStage().getTournament().isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (!encounter.getStage().getState().equals(StageState.STARTED)) {
            return createSingleResult(STAGE_STATE_KEY, "The stage of the encounter was not started!");
        }

        if (!encounter.getState().equals(EncounterState.CREATED)) {
            return createSingleResult(ENCOUNTER_KEY, "The encounter must be in the CREATED state to prepare!");
        }

        if (encounter.getEncounterParticipations().isEmpty()) {
            return createSingleResult(ENCOUNTER_KEY, "The encounter does not contain any participants!");
        }

        final boolean encounterBlocked = encounter.getStage().getEncounters().stream()
                .filter(e -> !e.equals(encounter))
                .filter(e -> e.getTeams().stream().anyMatch(encounter.getTeams()::contains))
                .anyMatch(e -> e.getState() == EncounterState.STARTED
                        || e.getState() == EncounterState.READY
                        || e.getState() == EncounterState.PREPARING);

        if (encounterBlocked) {
            return createSingleResult(ENCOUNTER_KEY, "The encounter is blocked due to another encounter of this team!");
        }

        encounter.setState(EncounterState.PREPARING);

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> readyEncounter(String uuid, Encounter encounter, Team team) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!team.isLeader(uuid) && !encounter.getStage().getTournament().isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        final Optional<EncounterParticipation> encounterParticipation = encounter.optEncounterParticipation(team);

        if (encounterParticipation.isEmpty()) {
            return createSingleResult(ENCOUNTER_KEY, "The team is not participating in this encounter!");
        }

        if (!encounter.getStage().getState().equals(StageState.STARTED)) {
            return createSingleResult(STAGE_STATE_KEY, "The stage of the encounter was not started!");
        }

        if (!encounter.getState().equals(EncounterState.PREPARING)) {
            return createSingleResult(ENCOUNTER_KEY, "The encounter is not in a valid state to ready up!");
        }

        final EncounterParticipation ep = encounterParticipation.get();
        ep.toggleReady();

        String message = "The team '" + team.getName() + "' is " + (!ep.isReady() ? "not " : "") + "ready!";
        messagingService.sendTextMessageToClient(uuid, message);

        if (encounter.getEncounterParticipations().stream().allMatch(EncounterParticipation::isReady)) {
            encounter.setState(EncounterState.READY);
        } else {
            encounter.setState(EncounterState.PREPARING);
        }

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> forfeitEncounter(String uuid, Encounter encounter, Team team) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!encounter.getStage().getTournament().isLeader(uuid)) {
            return getNotAllowedResponse();
        }


        final Optional<EncounterParticipation> optEncounterParticipation = encounter.optEncounterParticipation(team);

        if (optEncounterParticipation.isEmpty()) {
            return createSingleResult(TEAM_KEY, "The team '" + team.getName() + "' does not participate in the encounter!");
        }

        if (encounter.getStage().getState() != StageState.STARTED) {
            return createSingleResult(STAGE_STATE_KEY, "An encounter can only be forfeited if the current stage is started!");
        }

        if (encounter.getState() == EncounterState.FINISHED || encounter.getState() == EncounterState.FORFEITED) {
            return createSingleResult(ENCOUNTER_KEY, "The encounter cannot be forfeited since it is already finished or forfeited!");
        }

        entityManagement.getEntityManager().refresh(encounter);
        encounter.forfeit(team);

        encounterGenerationService.processStageState(encounter.getStage().getTournament());

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    private Map<String, List<String>> createSingleResult(String key, String errorMessage) {
        return Map.of(key, Collections.singletonList(errorMessage));
    }

    private Map<String, List<String>> getNotAllowedResponse() {
        return createSingleResult("user", NOT_ALLOWED);
    }
}
