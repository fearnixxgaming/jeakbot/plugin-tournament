package de.fearnixx.jeak.tournament.management.services;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.teamspeak.IUserService;
import de.fearnixx.jeak.teamspeak.IServer;
import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.TargetType;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.data.IUser;
import de.fearnixx.jeak.teamspeak.query.IQueryRequest;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.requestqueue.ClientRequestQueue;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class TournamentMessagingService {

    @Inject
    private IServer server;

    @Inject
    private IUserService userService;

    @Inject
    private TournamentEntityManagementService entityManagement;

    private ClientRequestQueue requestQueue;

    public TournamentMessagingService(ClientRequestQueue requestQueue) {
        this.requestQueue = requestQueue;
    }

    public void sendTextMessageToClient(IUser target, String message) {
        sendTextMessageToClient(target.getClientUniqueID(), message);
    }

    public void sendTextMessageToClient(IUser target, String message, boolean storeRequestIfOffline) {
        sendTextMessageToClient(target.getClientUniqueID(), message, storeRequestIfOffline);
    }

    public void sendTextMessageToClient(String targetUID, String message) {
        sendTextMessageToClient(targetUID, message, false);
    }

    public void sendTextMessageToClient(String targetUID, String message, boolean storeRequestIfOffline) {
        Optional<IClient> client = userService.findClientByUniqueID(targetUID).stream().findFirst();
        boolean present = client.isPresent();

        if (present || storeRequestIfOffline) {

            int targetId = present ? client.get().getClientID() : -1;

            IQueryRequest request = IQueryRequest.builder()
                    .command("sendtextmessage")
                    .addKey(PropertyKeys.TextMessage.TARGET_TYPE, TargetType.CLIENT.getQueryNum())
                    .addKey(PropertyKeys.TextMessage.TARGET_ID, targetId)
                    .addKey(PropertyKeys.TextMessage.MESSAGE, message)
                    .build();

            if (present) {
                server.getConnection().sendRequest(request);
            } else {
                requestQueue.submitRequestForClient(targetUID, request);
            }
        }
    }

    public void messageTeam(Team team, String message) {
        Set<String> messageUids = new HashSet<>(team.getMembersUid());

        messageUids.addAll(team.getLeaders());
        messageUids.forEach(
                uuid -> sendTextMessageToClient(uuid, message, true)
        );
    }
}
