package de.fearnixx.jeak.tournament.management.services;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.tournament.entities.encounter.Encounter;
import de.fearnixx.jeak.tournament.entities.encounter.EncounterParticipation;
import de.fearnixx.jeak.tournament.entities.stage.GroupMembership;
import de.fearnixx.jeak.tournament.entities.stage.Stage;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.Tournament;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentParameter;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentParticipation;
import de.fearnixx.jeak.tournament.enums.EncounterState;
import de.fearnixx.jeak.tournament.enums.ParticipationState;
import de.fearnixx.jeak.tournament.enums.StageState;
import de.fearnixx.jeak.tournament.enums.StageType;
import de.fearnixx.jeak.tournament.stagepattern.TournamentStageManagementService;
import org.antlr.v4.codegen.model.chunk.TokenRef;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TournamentStageGenerationService {

    @Inject
    private TournamentEntityManagementService entityManagement;

    @Inject
    private TournamentStageManagementService stageManagement;

    @Inject
    private TournamentMessagingService messagingService;

    public boolean generateStage(Stage stage) {
        return generateStage(stage, true);
    }

    public boolean generateStage(Stage stage, boolean nextStage) {
        List<Team> participatingTeams = stage.getTournament().getTournamentParticipations()
                .stream()
                .filter(TournamentParticipation::isEligibleToPlay)
                .map(TournamentParticipation::getTeam)
                .collect(Collectors.toList());

        Optional<de.fearnixx.jeak.tournament.stagepattern.Stage> currentTournamentStage = stageManagement
                .getTournamentStage(stage.getTournament(), nextStage);

        if (currentTournamentStage.isEmpty()) {
            return false;
        }

        final String type = currentTournamentStage.get().getType();

        if (Arrays.stream(StageType.values()).map(StageType::toString).noneMatch(type::equals)) {
            return false;
        }

        stage.setType(StageType.valueOf(type));

        currentTournamentStage.get()
                .getEncounterFactory()
                .createStage(participatingTeams, stage);

        final EntityManager entityManager = entityManagement.getEntityManager();

        final List<Encounter> encounters = stage.getEncounters();

        final List<Team> teamsWithEncounter = encounters.stream()
                .flatMap(encounter -> encounter.getEncounterParticipations(true).stream().map(EncounterParticipation::getTeam))
                .collect(Collectors.toList());

        if (stage.getTournament().optParameter(TournamentParameter.SUPPRESS_PART_GEN).isEmpty()) {
            participatingTeams.stream().filter(team -> !teamsWithEncounter.contains(team)).forEach(stage::createFreePass);
        }

        entityManager.persist(stage);

        return !encounters.isEmpty();
    }

    public void processStageState(Tournament tournament) {
        final Stage stage = tournament.getCurrentStage();

        if (stage.getEncounters().stream()
                .allMatch(e -> e.getState() == EncounterState.FINISHED || e.getState() == EncounterState.FORFEITED)) {

            stage.setState(StageState.FINISHED);

            new StageProgressionDecider(stage).processStageProgression();

            Stage nextStage = tournament.createStage(StageType.UNDEFINED);

            boolean encountersGenerated = generateStage(nextStage);

            if (!encountersGenerated) {
                tournament.getLeaders().forEach(
                        leader -> messagingService.sendTextMessageToClient(
                                leader,
                                "The last encounter of the tournament was finished!",
                                true
                        )
                );

                if (stage.getType().isGroup()) {
                    stage.getGroups().forEach(
                            g -> {
                                final List<GroupMembership> groupResults = g.getMembershipsSorted();

                                for (int i = 1; i < groupResults.size(); i++) {
                                    final TournamentParticipation tournamentParticipation = groupResults.get(i).getTournamentParticipation();
                                    tournamentParticipation.setState(ParticipationState.KNOCKED_OUT);
                                    tournamentParticipation.setLostStage(stage);

                                }
                            }
                    );
                }

                tournament.getTournamentParticipations().stream()
                        .filter(tp -> tp.getState() == ParticipationState.ACTIVE)
                        .forEach(tp -> tp.setState(ParticipationState.WON));

                entityManagement.getEntityManager().remove(nextStage);
            } else {
                tournament.setAndAddNewStage(nextStage);
            }

            entityManagement.getEntityManager().persist(tournament);
        }
    }
}
