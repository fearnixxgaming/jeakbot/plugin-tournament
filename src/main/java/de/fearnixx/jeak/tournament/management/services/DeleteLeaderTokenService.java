package de.fearnixx.jeak.tournament.management.services;

import de.fearnixx.jeak.tournament.entities.team.TeamLeadershipToken;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentLeadershipToken;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class DeleteLeaderTokenService {

    private final EntityManager entityManager;
    private final TypedQuery<TeamLeadershipToken> teamLeadershipTokenQuery;
    private final TypedQuery<TournamentLeadershipToken> tournamentLeadershipTokenQuery;

    private final TypedQuery<TeamLeadershipToken> allTeamLeadershipTokenQuery;
    private final TypedQuery<TournamentLeadershipToken> allTournamentLeadershipTokenQuery;


    public DeleteLeaderTokenService(EntityManager entityManager) {
        this.entityManager = entityManager;

        teamLeadershipTokenQuery = entityManager.createQuery(
                "SELECT t FROM TeamLeadershipToken t WHERE token = :token",
                TeamLeadershipToken.class
        );

        tournamentLeadershipTokenQuery = entityManager.createQuery(
                "SELECT t FROM TournamentLeadershipToken t WHERE token = :token",
                TournamentLeadershipToken.class
        );

        allTeamLeadershipTokenQuery = entityManager.createQuery(
                "SELECT t FROM TeamLeadershipToken t",
                TeamLeadershipToken.class
        );

        allTournamentLeadershipTokenQuery = entityManager.createQuery(
                "SELECT t FROM TournamentLeadershipToken t",
                TournamentLeadershipToken.class
        );
    }

    public void deleteTeamLeaderToken(String token) {
        removeToken(teamLeadershipTokenQuery, token);
    }

    public void deleteTournamentLeaderToken(String token) {
        removeToken(tournamentLeadershipTokenQuery, token);
    }

    public void clearAllTokens() {
        synchronized (entityManager) {
            entityManager.getTransaction().begin();

            try {
                allTeamLeadershipTokenQuery.getResultList().forEach(entityManager::remove);
                allTournamentLeadershipTokenQuery.getResultList().forEach(entityManager::remove);

                entityManager.getTransaction().commit();
            } catch (Exception e) {
                entityManager.getTransaction().rollback();

                throw new IllegalStateException("An exception occurred while trying to remove the token!", e);
            }
        }
    }

    private void removeToken(TypedQuery<?> query, String token) {
        synchronized (entityManager) {
            entityManager.getTransaction().begin();

            try {
                query.setParameter("token", token);
                query.getResultList().forEach(entityManager::remove);

                entityManager.getTransaction().commit();
            } catch (Exception e) {
                entityManager.getTransaction().rollback();

                throw new IllegalStateException("An exception occurred while trying to remove the token!", e);
            }
        }
    }
}
