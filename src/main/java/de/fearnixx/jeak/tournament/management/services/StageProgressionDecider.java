package de.fearnixx.jeak.tournament.management.services;

import de.fearnixx.jeak.tournament.entities.encounter.EncounterParticipation;
import de.fearnixx.jeak.tournament.entities.stage.GroupMembership;
import de.fearnixx.jeak.tournament.entities.stage.Stage;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentParticipation;
import de.fearnixx.jeak.tournament.enums.ParticipationState;
import de.fearnixx.jeak.tournament.enums.StageState;

import java.util.List;
import java.util.stream.Collectors;

public class StageProgressionDecider {

    private Stage stage;

    public StageProgressionDecider(Stage stage) {
        this.stage = stage;
    }

    public void processStageProgression() {
        if (stage.getState() != StageState.FINISHED) {
            throw new IllegalStateException("Can't process a state progression of an unfinished stage!");
        }

        List<TournamentParticipation> knockedOutTeams;

        switch (stage.getType()) {
            case KO:
                knockedOutTeams = stage.getEncounters().stream()
                        .flatMap(e -> e.getEncounterParticipations().stream())
                        .filter(EncounterParticipation::isEncounterLost)
                        .map(EncounterParticipation::getTournamentParticipation)
                        .collect(Collectors.toList());

                break;
            case GR:
            case TFT:
                knockedOutTeams = stage.getGroups().stream()
                        .flatMap(group -> group.getMemberships().stream())
                        .filter(GroupMembership::isGroupLost)
                        .map(GroupMembership::getTournamentParticipation).collect(Collectors.toList());

                break;
            default:
                throw new IllegalStateException("Invalid stage type!");
        }

        knockedOutTeams.stream()
                .filter(ep -> ep.getState() != ParticipationState.FORFEITED &&
                        ep.getState() != ParticipationState.DISQUALIFIED &&
                        ep.getState() != ParticipationState.KNOCKED_OUT)
                .forEach(
                        tp -> {
                            tp.setLostStage(stage);
                            tp.setState(ParticipationState.KNOCKED_OUT);
                        }
                );
    }
}
