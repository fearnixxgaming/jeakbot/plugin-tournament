package de.fearnixx.jeak.tournament.management;

import de.fearnixx.jeak.service.permission.base.IPermission;
import de.fearnixx.jeak.service.task.ITask;
import de.fearnixx.jeak.teamspeak.data.IUser;
import de.fearnixx.jeak.tournament.commands.CommandPermissions;
import de.fearnixx.jeak.tournament.entities.team.*;

import javax.persistence.TypedQuery;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class TeamManagement extends AbstractManagement {

    private static final String LEADER_TOKEN_PREFIX = "The leader token for team '";

    private static final String TEAM_NAME_KEY = "field:teamName";
    private static final String ONLY_LEADER_MSG = "Only the leader of a team is able to use this command!";
    private static final String TEAM_PERMISSION_KEY = "team:permission";
    private static final String PLAYER_KEY = "field:player";
    private static final String FIELD_TAG = "field:tag";
    private static final String TEAM_ACTIVE_TOURNAMENT_KEY = "team:activeTournament";

    private static final String TEAM_ACTIVE_TOURNAMENT_MSG =
            "The team is actively participating in a tournament and no changes are allowed!";
    private static final String SINGLE_PLAYER_TEAM_NOT_EDITABLE_MSG = "A single-player-team can not be edited!";

    private static final String[] ALLOWED_LOGO_TYPES = {"jpg", "png"};

    private TypedQuery<Player> playerByNameQuery;
    private TypedQuery<Player> playerByUUIDQuery;

    public void init() {
        playerByUUIDQuery = entityManagement.getEntityManager().createQuery("SELECT p FROM Player p WHERE tsUid = :uuid", Player.class);
        playerByNameQuery = entityManagement.getEntityManager().createQuery("SELECT p FROM Player p WHERE name = :name", Player.class);
    }

    public Map<String, List<String>> createTeam(String uuid, String teamName) {
        boolean transactionWasActive = entityManagement.beginTransaction();
        final Optional<String> optTeamName = checkTeamName(teamName);

        final IUser user = optUserByUuid(uuid).orElseThrow(() -> new IllegalStateException("User for uuid: " + uuid + " not found!"));
        final Optional<IPermission> maxTeamPermission = user.getPermission(ManagementPermissions.TOURNAMENT_CREATE_MAX_TEAMS);

        if (maxTeamPermission.isEmpty()) {
            return createSingleResult(TEAM_PERMISSION_KEY, "You do not have the permission to create a team!");
        }

        int createdTeamsOfUser = entityManagement.getCreatedTeamsByUser(uuid);
        if (createdTeamsOfUser >= maxTeamPermission.get().getValue()) {
            return createSingleResult(TEAM_PERMISSION_KEY, "You have already created the maximum amount of teams!");
        }

        if (optTeamName.isEmpty()) {
            return createSingleResult(TEAM_NAME_KEY, "The team name is invalid!");
        }

        teamName = optTeamName.get();

        if (entityManagement.getTeamByName(teamName).isPresent()) {
            return createSingleResult(TEAM_NAME_KEY, TEAM_PREFIX + teamName + "' already exists!");
        }

        Team team = new Team(teamName, uuid);

        team.createLeadership(uuid);
        final List<Player> players = playerByUUIDQuery.setParameter("uuid", uuid).getResultList();
        if (players.isEmpty()) {
            final Player player = Player.createByTsUid(uuid);
            entityManagement.getEntityManager().persist(player);
            team.createMembership(player, true);
        } else {
            team.createMembership(players.get(0), true);
        }

        entityManagement.getEntityManager().persist(team);

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        messagingService.sendTextMessageToClient(uuid, TEAM_PREFIX + teamName + "' has been created successfully.");

        return new HashMap<>();
    }

    public Map<String, List<String>> renameTeam(String uuid, Team team, String newTeamName) {
        boolean transactionWasActive = entityManagement.beginTransaction();
        final Optional<String> optTeamName = checkTeamName(newTeamName);

        if (!team.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (optTeamName.isEmpty()) {
            return createSingleResult(TEAM_NAME_KEY, "The team name is invalid!");
        }

        if (team.isSinglePlayer()) {
            return createSingleResult(TEAM_NAME_KEY, SINGLE_PLAYER_TEAM_NOT_EDITABLE_MSG);
        }

        newTeamName = optTeamName.get();

        if (entityManagement.getTeamByName(newTeamName).isPresent()) {
            return createSingleResult(TEAM_NAME_KEY, TEAM_PREFIX + newTeamName + "' already exists!");
        }

        if (checkActiveTournaments(team)) {
            return createSingleResult(TEAM_ACTIVE_TOURNAMENT_KEY, TEAM_ACTIVE_TOURNAMENT_MSG);
        }

        String oldTeamName = team.getName();

        team.setName(newTeamName);

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        messagingService.sendTextMessageToClient(
                uuid,
                TEAM_PREFIX + oldTeamName + "' was renamed to '" + newTeamName + "'!"
        );

        return new HashMap<>();
    }

    public Map<String, List<String>> deleteTeam(String uuid, Team team) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!team.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (!team.getTournamentParticipations().isEmpty()) {
            return createSingleResult("team", "The team is or was part of at least one tournament!");
        }

        entityManagement.getEntityManager().remove(team);

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        messagingService.sendTextMessageToClient(uuid, TEAM_PREFIX + team.getName() + "' was deleted successfully");

        return new HashMap<>();
    }

    public Map<String, List<String>> joinTeam(String uuid, Team team, String joinToken) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        final String teamName = team.getName();

        if (team.optTeamMembershipByTsUid(uuid).isPresent()) {
            return createSingleResult("user", "You are already member of the team '" + teamName + "'");
        }

        if (!team.getJoinToken().equals(joinToken)) {
            return createSingleResult(TOKEN_KEY, "Invalid token for the team '" + teamName + "'");
        }

        if (team.isSinglePlayer()) {
            return createSingleResult(TEAM_NAME_KEY, SINGLE_PLAYER_TEAM_NOT_EDITABLE_MSG);
        }

        if (checkActiveTournaments(team)) {
            return createSingleResult(TEAM_ACTIVE_TOURNAMENT_KEY, TEAM_ACTIVE_TOURNAMENT_MSG);
        }

        final List<Player> players = playerByUUIDQuery.setParameter("uuid", uuid).getResultList();
        if (players.isEmpty()) {
            final Player player = Player.createByTsUid(uuid);
            entityManagement.getEntityManager().persist(player);
            team.createMembership(player, true);
        } else {
            team.createMembership(players.get(0), true);
        }

        entityManagement.getEntityManager().persist(team);

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        messagingService.sendTextMessageToClient(uuid, "You successfully joined the team '" + teamName + "'");

        return new HashMap<>();
    }

    public Map<String, List<String>> leaveTeam(String uuid, Team team) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        Optional<TeamMembership> optTeamMembership = team.optTeamMembershipByTsUid(uuid);

        if (optTeamMembership.isEmpty()) {
            return createSingleResult("user", "You are not a member of the team '" + team.getName() + "'");
        }

        if (team.isSinglePlayer()) {
            return createSingleResult(TEAM_NAME_KEY, SINGLE_PLAYER_TEAM_NOT_EDITABLE_MSG);
        }

        TeamMembership teamMembership = optTeamMembership.get();

        if (teamMembership.isActive() && checkActiveTournaments(team)) {
            return createSingleResult(TEAM_ACTIVE_TOURNAMENT_KEY, TEAM_ACTIVE_TOURNAMENT_MSG);
        }

        team.removeMembership(teamMembership);
        entityManagement.getEntityManager().remove(teamMembership);

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        messagingService.sendTextMessageToClient(uuid, "You successfully left the team '" + team.getName() + "'");

        return new HashMap<>();
    }

    public Map<String, List<String>> token(String uuid, Team team, String newJoinToken) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!team.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (team.isSinglePlayer()) {
            return createSingleResult(TEAM_NAME_KEY, SINGLE_PLAYER_TEAM_NOT_EDITABLE_MSG);
        }

        if (newJoinToken == null) {
            messagingService.sendTextMessageToClient(
                    uuid, "The token for team '" + team.getName() + "': " + team.getJoinToken()
            );
        } else {
            newJoinToken = newJoinToken.trim();

            if (!isTokenValid(newJoinToken)) {
                return createSingleResult(TOKEN_KEY, "The token must be not empty and without any whitespaces");
            }

            team.setJoinToken(newJoinToken);

            messagingService.sendTextMessageToClient(uuid, "The token for team '" + team.getName() + "' was changed");
        }

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> leaderToken(String uuid, Team team, String leaderToken) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (team.isSinglePlayer()) {
            return createSingleResult(TEAM_NAME_KEY, SINGLE_PLAYER_TEAM_NOT_EDITABLE_MSG);
        }

        if (leaderToken == null) {
            if (!team.isLeader(uuid)) {
                return getNotAllowedResponse();
            }
            final String token = UUID.randomUUID().toString();

            team.createTeamLeadershipToken();
            entityManagement.getEntityManager().persist(team);

            messagingService.sendTextMessageToClient(
                    uuid, LEADER_TOKEN_PREFIX + team.getName() + "': " + token
            );

            taskService.scheduleTask(
                    ITask.builder()
                            .delay(configService.getLeaderTokenTimeout(), TimeUnit.MINUTES)
                            .name("deleteTeamLeaderToken:" + token)
                            .runnable(
                                    () -> entityManagement.getDeleteLeaderTokenService().deleteTeamLeaderToken(token)
                            )
                            .build()
            );
        } else {
            final String searchLeaderToken = leaderToken.trim();

            if (!isTokenValid(searchLeaderToken)) {
                return createSingleResult(TOKEN_KEY, "The token must be not empty without any whitespaces");
            }

            if (team.isLeader(uuid)) {
                return createSingleResult("user", "You are already leader of team '" + team.getName() + "'");
            }

            Optional<TeamLeadershipToken> leadershipToken = optLeadershipToken(team, searchLeaderToken);

            if (leadershipToken.isEmpty()) {
                return createSingleResult(TOKEN_KEY,
                        LEADER_TOKEN_PREFIX + team.getName() + "' is not valid (anymore)."
                );
            }

            team.createLeadership(uuid);
            entityManagement.getEntityManager().persist(team);

            final TeamLeadershipToken teamLeadershipToken = leadershipToken.get();
            team.removeTeamLeadershipToken(teamLeadershipToken);
            entityManagement.getEntityManager().remove(teamLeadershipToken);

            messagingService.sendTextMessageToClient(
                    uuid,
                    "You are now a leader of team '" + team.getName() + "'"
            );
        }

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> leaderRemove(String uuid, Team team, String leaderUuid) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!team.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (team.isSinglePlayer()) {
            return createSingleResult(TEAM_NAME_KEY, SINGLE_PLAYER_TEAM_NOT_EDITABLE_MSG);
        }

        String uuidToRemove;

        if (leaderUuid == null) {
            uuidToRemove = uuid;
        } else {
            uuidToRemove = leaderUuid;
        }

        Optional<TeamLeadership> teamLeadership = team.getLeaderships().stream()
                .filter(tl -> tl.getUuid().equals(leaderUuid))
                .findFirst();

        final String teamName = team.getName();

        if (teamLeadership.isEmpty()) {
            return createSingleResult("leader", "This user is no leader of the team '" + teamName + "'");
        }

        if (team.getLeaders().size() == 1) {
            return createSingleResult("leader", "The last leader of a team cannot be removed!");
        }

        final TeamLeadership leadership = teamLeadership.get();

        team.removeLeadership(leadership);
        entityManagement.getEntityManager().remove(leadership);

        entityManagement.getEntityManager().flush();

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        messagingService.sendTextMessageToClient(
                uuidToRemove,
                "You are no longer leader of the team '" + teamName + "'",
                true
        );

        if (!uuidToRemove.equals(uuid)) {
            Optional<IUser> optUser = optUserByUuid(uuidToRemove);
            String nickName = optUser.isPresent() ? optUser.get().getNickName() : uuidToRemove;

            messagingService.sendTextMessageToClient(
                    uuid,
                    "The user '" + nickName + "' is no longer leader of the team '" + teamName + "'"
            );
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> infoTeam(String uuid, Team team) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        List<String> memberUids = team.getMembersUid();

        if (!team.isLeader(uuid) || !memberUids.contains(uuid)) {
            return createSingleResult("user", "Only members or the leader of a team are able to use this command!");
        }

        List<String> playerInfo = new ArrayList<>();
        for (Player player : team.getMembers()) {
            String tsDisplay = player.optTsUid().map(
                    tsUid -> optUserByUuid(tsUid).map(IUser::getNickName).orElse(tsUid)
            ).orElse("-");

            String playerName = player.optName().orElse("-");

            playerInfo.add(tsDisplay + " / " + playerName + " (" + player.getId() + ")");
        }

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        messagingService.sendTextMessageToClient(
                uuid,
                TEAM_PREFIX + team.getName() + "' consists of these members:\n" + String.join("\n", playerInfo)
        );

        return new HashMap<>();
    }

    public Map<String, List<String>> messageTeam(String uuid, Team team, String message) {
        boolean transactionWasActive = entityManagement.beginTransaction();
        if (!team.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        final Optional<String> optMessage = checkMessage(message);

        if (optMessage.isEmpty()) {
            return createSingleResult("message", "The message must be not empty!");
        }

        String msg = "[Team - " + team.getName() + "] " + optMessage.get();


        messagingService.messageTeam(team, msg);
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> addTeamMember(String uuid, Team team, String playerUuid, String playerName) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        final IUser user = optUserByUuid(uuid).orElseThrow(
                () -> new IllegalStateException("User for uuid: " + uuid + " not found!")
        );

        if (!user.hasPermission(CommandPermissions.TEAM_MEMBER_ADD)) {
            return createSingleResult(TEAM_PERMISSION_KEY, "You do not have the permission to add team-members!");
        }

        if (!team.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (team.isSinglePlayer()) {
            return createSingleResult(TEAM_NAME_KEY, SINGLE_PLAYER_TEAM_NOT_EDITABLE_MSG);
        }

        if (checkActiveTournaments(team)) {
            return createSingleResult(TEAM_ACTIVE_TOURNAMENT_KEY, TEAM_ACTIVE_TOURNAMENT_MSG);
        }

        final String teamName = team.getName();
        if (playerUuid != null && team.getMembersUid().contains(playerUuid)) {
            return createSingleResult(PLAYER_KEY, "The player is already member of the team '" + teamName + "'");
        }

        if (playerName != null && team.getMembersPlayerName().contains(playerName)) {
            return createSingleResult(PLAYER_KEY, "The player is already member of the team '" + teamName + "'");
        }

        if (playerName == null && playerUuid == null) {
            return createSingleResult(PLAYER_KEY, "The player name or the ts-uid have to be supplied to add a team-member!");
        }

        final Player playerToAdd;
        List<Player> playerSearchResult = new ArrayList<>();

        if (playerUuid != null) {
            playerSearchResult = playerByUUIDQuery.setParameter("uuid", playerUuid).getResultList();
        }

        if (playerName != null && playerSearchResult.isEmpty()) {
            playerSearchResult = playerByNameQuery.setParameter("name", playerName).getResultList();
        }

        if (playerSearchResult.isEmpty()) {
            Player player = null;

            if (playerUuid != null) {
                player = Player.createByTsUid(playerUuid);
            }

            if (playerName != null) {
                if (player == null) {
                    player = Player.createByName(playerName);
                } else {
                    player.setName(playerName);
                }
            }

            entityManagement.getEntityManager().persist(player);
            playerToAdd = player;
        } else {
            playerToAdd = playerSearchResult.get(0);

            playerToAdd.setName(playerName);
            playerToAdd.setTsUid(playerUuid);
        }

        team.createMembership(playerToAdd, true);

        entityManagement.getEntityManager().persist(team);

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        messagingService.sendTextMessageToClient(
                playerUuid, "You were added to the team '" + teamName + "'"
        );

        if (playerUuid != null && !playerUuid.equals(uuid)) {
            messagingService.sendTextMessageToClient(
                    uuid,
                    "The client has been added to the team."
            );
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> removeTeamMember(String uuid, Team team, Player player) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!team.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (team.isSinglePlayer()) {
            return createSingleResult(TEAM_NAME_KEY, SINGLE_PLAYER_TEAM_NOT_EDITABLE_MSG);
        }

        if (checkActiveTournaments(team)) {
            return createSingleResult(TEAM_ACTIVE_TOURNAMENT_KEY, TEAM_ACTIVE_TOURNAMENT_MSG);
        }

        Optional<TeamMembership> optTeamMembership = team.optTeamMembership(player);

        final String teamName = team.getName();

        if (optTeamMembership.isEmpty()) {
            return createSingleResult("client", "The client is not a member of the team '" + teamName + "'");
        }

        TeamMembership teamMembership = optTeamMembership.get();
        team.removeMembership(teamMembership);
        entityManagement.getEntityManager().remove(teamMembership);

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        player.optTsUid().ifPresent(
                tsUid ->
                        messagingService.sendTextMessageToClient(
                                tsUid,
                                "You were removed from the team '" + teamName + "'"
                        )
        );

        messagingService.sendTextMessageToClient(
                uuid,
                "The client has been removed from the team."
        );

        return new HashMap<>();
    }

    public Map<String, List<String>> claimTeam(String uuid, Team team) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        final IUser user = optUserByUuid(uuid).orElseThrow(
                () -> new IllegalStateException("User for uuid: " + uuid + " not found!")
        );

        if (!user.hasPermission(CommandPermissions.TEAM_CLAIM)) {
            return createSingleResult(TEAM_PERMISSION_KEY, "You do not have the permission to claim a team!");
        }

        if (team.isLeader(uuid)) {
            return createSingleResult("user", "You are already leader of team '" + team.getName() + "'");
        }

        team.createLeadership(uuid);
        entityManagement.getEntityManager().persist(team);

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> setPlayerInfo(String uuid, Team team, Player player, String playerUuid, String playerName, boolean active) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!team.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (checkActiveTournaments(team)) {
            return createSingleResult(TEAM_ACTIVE_TOURNAMENT_KEY, TEAM_ACTIVE_TOURNAMENT_MSG);
        }

        if (!team.getMembers().contains(player)) {
            return createSingleResult(PLAYER_KEY, "The given player is not a member of this team!");
        }

        if (playerUuid == null && playerName == null) {
            return createSingleResult(PLAYER_KEY, "The player must have a ts-uid or player name!");
        }

        if (team.isSinglePlayer() && !active) {
            return createSingleResult(PLAYER_KEY, "A single player can only be active!");
        }

        team.optTeamMembership(player).ifPresent(p -> p.setActive(active));

        String newName = playerName;
        if (newName != null && newName.isEmpty()) {
            newName = null;
        }
        player.setName(newName);

        String newUuid = playerUuid;
        if (newUuid != null && newUuid.isEmpty()) {
            newUuid = null;
        }
        player.setTsUid(newUuid);

        entityManagement.getEntityManager().persist(player);

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> setTeamDescription(String uuid, Team team, String description) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!team.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (description.length() > 999) {
            return createSingleResult("field:description", "The description must contain less than 1000 characters!");
        }

        team.setDescription(description);

        entityManagement.getEntityManager().persist(team);

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> setTeamLogoUrl(String uuid, Team team, String logoUrl) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!team.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (logoUrl.length() > 999) {
            return createSingleResult("field:logoUrl", "The logo url must contain less than 1000 characters!");
        }

        if (Arrays.stream(ALLOWED_LOGO_TYPES).noneMatch(logoUrl.toLowerCase()::endsWith)) {
            return createSingleResult("field:logoUrl", "The logo URL is not specifying a valid media type!");
        }

        team.setLogoUrl(logoUrl);

        entityManagement.getEntityManager().persist(team);

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> setTag(String uuid, Team team, String tag) {
        boolean transactionWasActive = entityManagement.beginTransaction();
        tag = tag.toUpperCase();

        if (!team.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (team.isSinglePlayer()) {
            return createSingleResult(FIELD_TAG, "A single player team can't have a tag!");
        }

        if (tag.length() > 8) {
            return createSingleResult(FIELD_TAG, "The tag must contain less than 9 characters!");
        }

        final Optional<Team> alreadyUsingTeam = entityManagement.getTeamByTag(tag);
        if (alreadyUsingTeam.isPresent()) {
            return createSingleResult(
                    FIELD_TAG, "The team '" + alreadyUsingTeam.get().getName() + "' is already using this tag!"
            );
        }

        team.setTag(tag);
        entityManagement.getEntityManager().persist(team);

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    private Map<String, List<String>> createSingleResult(String key, String errorMessage) {
        return Map.of(key, Collections.singletonList(errorMessage));
    }

    private Map<String, List<String>> getNotAllowedResponse() {
        return createSingleResult("user", ONLY_LEADER_MSG);
    }

    private Optional<String> checkTeamName(String teamName) {
        if (teamName.isBlank() || teamName.startsWith(Team.SINGLE_PLAYER_TEAM_PREFIX)) {
            return Optional.empty();
        }

        return Optional.of(teamName.trim());
    }

    private boolean checkActiveTournaments(Team team) {
        return entityManagement.checkActiveTournaments(team);
    }

    private Optional<TeamLeadershipToken> optLeadershipToken(Team team, String leadershipToken) {
        return team.getTeamLeadershipTokens().stream()
                .filter(teamLeadershipToken -> teamLeadershipToken.getToken().equals(leadershipToken))
                .filter(teamLeadershipToken -> teamLeadershipToken.getCreatedTsp().isAfter(
                        ZonedDateTime.now().minusMinutes(configService.getLeaderTokenTimeout()))
                )
                .findFirst();
    }

    private Optional<String> checkMessage(String message) {
        if (message.trim().isBlank()) {
            return Optional.empty();
        }

        return Optional.of(message.trim());
    }

    private boolean isTokenValid(String token) {
        return !token.isBlank() &&
                !token.contains(" ");
    }

    private Optional<IUser> optUserByUuid(String uuid) {
        return userService.findUserByUniqueID(uuid).stream().findFirst();
    }
}

