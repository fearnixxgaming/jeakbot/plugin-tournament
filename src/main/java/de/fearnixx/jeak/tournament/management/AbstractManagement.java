package de.fearnixx.jeak.tournament.management;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.task.ITaskService;
import de.fearnixx.jeak.service.teamspeak.IUserService;
import de.fearnixx.jeak.teamspeak.IServer;
import de.fearnixx.jeak.tournament.config.TournamentConfigService;
import de.fearnixx.jeak.tournament.management.services.TournamentChannelManagementService;
import de.fearnixx.jeak.tournament.management.services.TournamentEntityManagementService;
import de.fearnixx.jeak.tournament.management.services.TournamentMessagingService;
import de.fearnixx.jeak.tournament.management.services.TournamentStageGenerationService;
import de.fearnixx.jeak.tournament.stagepattern.TournamentStageManagementService;

public class AbstractManagement {

    protected static final String TEAM_PREFIX = "The team '";
    protected static final String TOURNAMENT_PREFIX = "The tournament '";

    protected static final String TOKEN_KEY = "token";
    protected static final String TOURNAMENT_KEY = "tournament";
    protected static final String PARTICIPATION_KEY = "participation";
    protected static final String TEAM_KEY = "team";
    protected static final String CLIENT_KEY = "client";
    
    @Inject
    protected IServer server;

    @Inject
    protected TournamentEntityManagementService entityManagement;

    @Inject
    protected TournamentMessagingService messagingService;

    @Inject
    protected TournamentChannelManagementService channelManagement;

    @Inject
    protected TournamentStageManagementService stageManagement;

    @Inject
    protected TournamentStageGenerationService encounterGenerationService;
    
    @Inject
    protected TournamentConfigService configService;

    @Inject
    protected IUserService userService;

    @Inject
    protected ITaskService taskService;
}
