package de.fearnixx.jeak.tournament.management;

public class ManagementPermissions {

    private ManagementPermissions() {
    }

    public static final String TOURNAMENT_CREATE_MAX_TEAMS = "tournaments.command.tournament.create.max_teams";
}
