package de.fearnixx.jeak.tournament.management;

import de.fearnixx.jeak.service.task.ITask;
import de.fearnixx.jeak.teamspeak.data.IUser;
import de.fearnixx.jeak.tournament.commands.CommandPermissions;
import de.fearnixx.jeak.tournament.entities.Season;
import de.fearnixx.jeak.tournament.entities.encounter.Encounter;
import de.fearnixx.jeak.tournament.entities.encounter.EncounterParticipation;
import de.fearnixx.jeak.tournament.entities.stage.Group;
import de.fearnixx.jeak.tournament.entities.stage.Stage;
import de.fearnixx.jeak.tournament.entities.stage.StageFreePass;
import de.fearnixx.jeak.tournament.entities.team.Player;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.*;
import de.fearnixx.jeak.tournament.enums.*;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class TournamentManagement extends AbstractManagement {

    private static final String DEFAULT_STAGE_PATTERN = "(KO:2)*";
    private static final String LEADER_TOKEN_PREFIX = "The leader token for tournament '";
    private static final String TOURNAMENT_NAME_KEY = "field:tournamentName";
    private static final String ONLY_TOURNAMENT_LEADERS = "Only tournament leaders are allowed to use this command!";

    private static final String TEST_TOURNAMENT_MARKER = "test";
    private static final String TEST_TOURNAMENT_KEY = "##TEST_TOURNAMENT##";
    private static final String PARAMETER_MSG_PREFIX = "The parameter '";

    private static final String ALREADY_FINISHED_SUFFIX = "' is already started or finished!";
    private static final String NOT_PARTICIPATING_MESSAGE = "' is not participating in this tournament!";

    private static final String TOURNAMENT_PERMISSION_KEY = "tournament:permission";
    private static final String TEAM_MSG_PREFIX = "The team '";
    private static final String CLASSIFIER_KEY = "field:classifier";
    private static final String SEASON_KEY = "field:season";

    private TypedQuery<TournamentClassifier> classifierQuery;
    private TypedQuery<Season> seasonQuery;
    private TypedQuery<Player> playerByUidQuery;

    public void init() {
        classifierQuery = entityManagement.getEntityManager()
                .createQuery("SELECT tc FROM TournamentClassifier tc WHERE classifier = :classifier", TournamentClassifier.class);

        seasonQuery = entityManagement.getEntityManager()
                .createQuery("SELECT s FROM Season s WHERE name = :name", Season.class);

        playerByUidQuery = entityManagement.getEntityManager()
                .createQuery("SELECT p FROM Player p WHERE tsUid = :uuid", Player.class);
    }

    public Map<String, List<String>> createTournament(String uuid, String tournamentName) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        final Optional<String> optTournamentName = checkTournamentName(tournamentName);

        if (optTournamentName.isEmpty()) {
            return createSingleResult(TOURNAMENT_NAME_KEY, "The tournament name is invalid!");
        }

        tournamentName = optTournamentName.get();

        if (entityManagement.getTournamentByName(tournamentName).isPresent()) {
            return createSingleResult(TOURNAMENT_NAME_KEY, TOURNAMENT_PREFIX + tournamentName + "' already exists!");
        }

        Tournament tournament = new Tournament(tournamentName, uuid);
        tournament.createTournamentParameter(TournamentParameter.STAGE_PATTERN, DEFAULT_STAGE_PATTERN);
        tournament.createTournamentLeadership(uuid);

        if (tournamentName.toLowerCase().contains(TEST_TOURNAMENT_MARKER)) {
            tournament.createTournamentParameter(TEST_TOURNAMENT_KEY, "-");
        }

        final EntityManager entityManager = entityManagement.getEntityManager();
        entityManager.persist(tournament);

        entityManager.flush();

        if (!transactionWasActive) {
            entityManager.getTransaction().commit();
        }

        channelManagement.createTournamentChannel(tournament, configService.getTournamentContainerChannel());
        channelManagement.createWaitingAndRegisteringChannel(tournament, configService.getWaitingChannel());

        messagingService.sendTextMessageToClient(
                uuid,
                TOURNAMENT_PREFIX + tournamentName + "' has been created successfully."
        );

        return new HashMap<>();
    }

    private Optional<String> checkTournamentName(String tournamentName) {
        tournamentName = tournamentName.trim();

        if (tournamentName.isBlank()) {
            return Optional.empty();
        }

        return Optional.of(tournamentName);
    }

    public Map<String, List<String>> deleteTournament(String uuid, Tournament tournament) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (!tournament.getParameters().containsKey(TEST_TOURNAMENT_KEY)) {
            return createSingleResult(TOURNAMENT_KEY, "The tournament is not a test-tournament and cannot be deleted!");
        }

        final EntityManager entityManager = entityManagement.getEntityManager();

        entityManager.remove(tournament);

        stageManagement.removeTournament(tournament);

        entityManager.flush();
        if (!transactionWasActive) {
            entityManager.getTransaction().commit();
        }

        messagingService.sendTextMessageToClient(
                uuid,
                TOURNAMENT_PREFIX + tournament.getName() + "' has been deleted successfully."
        );

        channelManagement.deleteTournamentChannel(tournament);

        return new HashMap<>();
    }

    public Map<String, List<String>> joinTournament(String uuid, Tournament tournament, Team team) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!team.isLeader(uuid)) {
            return createSingleResult("user", "Only team leaders are able to join a tournament");
        }

        if (tournament.getStartedTsp() != null) {
            return createSingleResult(TOURNAMENT_KEY, TOURNAMENT_PREFIX + tournament.getName() + "' is already started!");
        }

        if (tournament.optTournamentParticipation(team).isPresent()) {
            return createSingleResult(PARTICIPATION_KEY, TEAM_PREFIX + team.getName() + "' already participates in this tournament!");
        }

        if (team.getMembers().stream().anyMatch(member -> tournament.getParticipants().contains(member))) {
            return createSingleResult(TOURNAMENT_KEY, "At least one member of '" + team.getName() + "' is already participating in the tournament!");
        }

        final List<TournamentParticipation> tournamentParticipations = team.getTournamentParticipations();
        if (tournamentParticipations.stream().anyMatch(tp -> !tp.getTournament().getState().equals(TournamentState.FINISHED))) {
            return createSingleResult(PARTICIPATION_KEY, TEAM_PREFIX + team.getName() + "' already participates in another tournament that is not finished!");
        }

        List<String> teamSizeErrors = checkTeamSize(tournament, team);
        if (!teamSizeErrors.isEmpty()) {
            return Map.of(TEAM_KEY, teamSizeErrors);
        }

        final boolean isSinglePlayerTournament = tournament.optParameter(TournamentParameter.SINGLE_PLAYER).isPresent();
        if (team.isSinglePlayer() != isSinglePlayerTournament) {
            return createSingleResult(TOURNAMENT_KEY, "The tournament" + (isSinglePlayerTournament ? " is " : " is not ") + " a single-player-tournament and the team is not eligible!");
        }

        if (tournament.optParameter(TournamentParameter.REQUIRE_INVITATION).isPresent()) {
            return createSingleResult(TOURNAMENT_KEY, "This tournament requires an invitation!");
        }

        tournament.createTournamentParticipation(team);

        entityManagement.getEntityManager().persist(tournament);
        entityManagement.getEntityManager().flush();

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        channelManagement.setChannelGroupForTeamOfTournament(tournament, team);

        messagingService.sendTextMessageToClient(uuid,
                TEAM_PREFIX + team.getName() + "' successfully joined the tournament '" +
                        tournament.getName() + "'"
        );

        return new HashMap<>();
    }

    private List<String> checkTeamSize(Tournament tournament, Team team) {
        List<String> errors = new ArrayList<>();
        tournament.optParameterValue(TournamentParameter.MIN_TEAM_SIZE).ifPresent(
                minTeamSizeStr -> {
                    int minTeamSize = Integer.parseInt(minTeamSizeStr);
                    int actualTeamSize = team.getMembers().size();

                    if (actualTeamSize < minTeamSize) {
                        errors.add(
                                TEAM_PREFIX + team.getName() + "' does not have enough members! Actual: " + actualTeamSize
                                        + " " + "Required: " + minTeamSize
                        );
                    }
                }
        );

        tournament.optParameterValue(TournamentParameter.MAX_TEAM_SIZE).ifPresent(
                maxTeamSizeStr -> {
                    int maxTeamSize = Integer.parseInt(maxTeamSizeStr);
                    int actualTeamSize = team.getMembers().size();

                    if (actualTeamSize > maxTeamSize) {
                        errors.add(
                                TEAM_PREFIX + team.getName() + "' has too many members! Actual: " + actualTeamSize
                                        + " " + "Permitted: " + maxTeamSize
                        );
                    }
                }
        );

        return errors;
    }

    public Map<String, List<String>> readyTournament(String uuid, Tournament tournament, Team team) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!team.isLeader(uuid) && !tournament.isLeader(uuid)) {
            return createSingleResult("user", "Only team- or tournament-leaders are able to ready up for a tournament!");
        }

        if (tournament.getStartedTsp() != null) {
            return createSingleResult(TOURNAMENT_KEY, TOURNAMENT_PREFIX + tournament.getName() + "' is already started!");
        }

        final Optional<TournamentParticipation> optParticipation = tournament.optTournamentParticipation(team);

        if (optParticipation.isEmpty()) {
            return createSingleResult(PARTICIPATION_KEY, TEAM_PREFIX + team.getName() + NOT_PARTICIPATING_MESSAGE);
        }

        TournamentParticipation tournamentParticipation = optParticipation.get();

        if (tournamentParticipation.getState() == ParticipationState.DISQUALIFIED) {
            return createSingleResult(PARTICIPATION_KEY, TEAM_PREFIX + team.getName() + " has been disqualified.");
        }

        if (tournamentParticipation.getState() == ParticipationState.READY) {
            return createSingleResult(PARTICIPATION_KEY, TEAM_PREFIX + team.getName() + " has already readied up for the tournament.");
        }

        tournamentParticipation.setState(ParticipationState.READY);

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        messagingService.sendTextMessageToClient(uuid,
                TEAM_PREFIX + team.getName() + "' readied up for the tournament '" +
                        tournament.getName() + "'"
        );

        return new HashMap<>();
    }

    public Map<String, List<String>> addSinglePlayerTeam(String uuid, Tournament tournament, String playerUuid) {
        entityManagement.beginTransaction();

        List<Player> playerSearchResult = playerByUidQuery
                .setParameter("uuid", playerUuid)
                .getResultList();

        Player player;
        Team team;
        boolean isNewTeam = true;

        if (playerSearchResult.isEmpty()) {
            player = Player.createByTsUid(playerUuid);
            entityManagement.getEntityManager().persist(player);

            entityManagement.getEntityManager().getTransaction().commit();
            entityManagement.beginTransaction();

            player = playerByUidQuery.setParameter("uuid", playerUuid).getResultList().get(0);
        } else {
            player = playerSearchResult.get(0);
        }

        List<Team> singlePlayerTeamSearchResult = entityManagement.getEntityManager()
                .createQuery("SELECT t FROM Team t WHERE name = :name", Team.class)
                .setParameter("name", Team.SINGLE_PLAYER_TEAM_PREFIX + player.getId())
                .getResultList();

        if (singlePlayerTeamSearchResult.isEmpty()) {
            team = new Team(player);
        } else {
            team = singlePlayerTeamSearchResult.get(0);
            isNewTeam = false;
        }


        if (isNewTeam) {
            team = new Team(player);
            team.createMembership(player, true);
            team.createLeadership(playerUuid);
            entityManagement.getEntityManager().persist(team);
        }

        return addTeam(uuid, tournament, team);
    }

    public Map<String, List<String>> addTeam(String uuid, Tournament tournament, Team team) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (tournament.getStartedTsp() != null || tournament.getFinishedTsp() != null) {
            return createSingleResult(TOURNAMENT_KEY, TOURNAMENT_PREFIX + tournament.getName() + "' is already started or finished");
        }

        if (tournament.optTournamentParticipation(team).isPresent()) {
            return createSingleResult(PARTICIPATION_KEY, TEAM_PREFIX + team.getName() + "' already participates in this tournament!");
        }

        if (team.getActiveMembers().stream().anyMatch(member -> tournament.getActiveParticipants().contains(member))) {
            return createSingleResult(TOURNAMENT_KEY, "At least one member of '" + team.getName() + "' is already actively participating in the tournament!");
        }

        final boolean isSinglePlayerTournament = tournament.optParameter(TournamentParameter.SINGLE_PLAYER).isPresent();
        if (team.isSinglePlayer() != isSinglePlayerTournament) {
            return createSingleResult(TOURNAMENT_KEY, "The tournament" + (isSinglePlayerTournament ? " is " : " is not ") + " a single-player-tournament and the team is not eligible!");
        }

        tournament.createTournamentParticipation(team);

        entityManagement.getEntityManager().persist(tournament);

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        channelManagement.setChannelGroupForTeamOfTournament(tournament, team);

        String message = TEAM_PREFIX + team.getName() + "' has been added to the tournament '" +
                tournament.getName() + "'";

        messagingService.sendTextMessageToClient(uuid, message);

        team.getLeaders().forEach(
                leader -> messagingService.sendTextMessageToClient(leader, message, true)
        );

        return new HashMap<>();
    }

    public Map<String, List<String>> leaveTournament(String uuid, Tournament tournament, Team team) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!team.isLeader(uuid)) {
            return createSingleResult("user", "Only team leaders are allowed to leave a tournament!");
        }

        final Optional<TournamentParticipation> tournamentParticipation = tournament.optTournamentParticipation(team);

        if (tournamentParticipation.isEmpty()) {
            return createSingleResult(PARTICIPATION_KEY, TEAM_PREFIX + team.getName() + NOT_PARTICIPATING_MESSAGE);
        }

        if (tournament.getState() != TournamentState.CREATED) {
            return createSingleResult(
                    TOURNAMENT_KEY,
                    TOURNAMENT_PREFIX + tournament.getName() + ALREADY_FINISHED_SUFFIX
            );
        }

        final TournamentParticipation participation = tournamentParticipation.get();
        tournament.removeTournamentParticipation(participation);
        team.removeTournamentParticipation(participation);
        entityManagement.getEntityManager().remove(participation);

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        messagingService.sendTextMessageToClient(uuid,
                TEAM_PREFIX + team.getName() + "' successfully left the tournament '" +
                        tournament.getName() + "'"
        );

        return new HashMap<>();
    }

    public Map<String, List<String>> removeTeam(String uuid, Tournament tournament, Team team) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        Optional<TournamentParticipation> tournamentParticipation =
                tournament.optTournamentParticipation(team);

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (tournamentParticipation.isEmpty()) {
            return createSingleResult(PARTICIPATION_KEY, TEAM_PREFIX + team.getName() + NOT_PARTICIPATING_MESSAGE);
        }

        if (tournament.getState() != TournamentState.CREATED) {
            return createSingleResult(TOURNAMENT_KEY, TOURNAMENT_PREFIX + tournament.getName() + ALREADY_FINISHED_SUFFIX);
        }

        final TournamentParticipation participation = tournamentParticipation.get();
        tournament.removeTournamentParticipation(participation);
        team.removeTournamentParticipation(participation);
        entityManagement.getEntityManager().remove(participation);

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        String message = TEAM_PREFIX + team.getName() + "' has been removed from the tournament '" +
                tournament.getName() + "'";
        messagingService.sendTextMessageToClient(
                uuid, message
        );

        team.getLeaders().forEach(
                leader -> messagingService.sendTextMessageToClient(leader, message, true)
        );

        return new HashMap<>();
    }

    public Map<String, List<String>> startTournament(String uuid, Tournament tournament) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        TournamentState status = tournament.getState();
        if (status.equals(TournamentState.STARTED) || status.equals(TournamentState.FINISHED)) {
            return createSingleResult(TOURNAMENT_KEY, TOURNAMENT_PREFIX + tournament.getName() + ALREADY_FINISHED_SUFFIX);
        }

        entityManagement.getEntityManager().refresh(tournament);
        if (tournament.getTeams().isEmpty()) {
            return createSingleResult(TOURNAMENT_KEY, TOURNAMENT_PREFIX + tournament.getName() + "' has no participating teams!");
        }

        List<TournamentParticipation> participations = tournament.getTournamentParticipations()
                .stream()
                .filter(p -> p.getState().equals(ParticipationState.JOINED))
                .collect(Collectors.toList());

        if (!participations.isEmpty()) {
            return createSingleResult(PARTICIPATION_KEY,
                    "Could not start the tournament, because the following teams have not readied up yet: "
                            + participations.stream()
                            .map(p -> p.getTeam().getName())
                            .collect(Collectors.joining(System.lineSeparator()))
            );
        }

        tournament.start();
        stageManagement.registerTournament(tournament);

        final Stage firstStage = tournament.createStage(StageType.UNDEFINED);

        final boolean encountersCreated = encounterGenerationService.generateStage(firstStage);

        if (!encountersCreated) {
            entityManagement.getEntityManager().remove(firstStage);
            return createSingleResult(TOURNAMENT_KEY, "There are no encounters to be generated!");
        } else {
            tournament.setAndAddNewStage(firstStage);
            entityManagement.getEntityManager().persist(tournament);
        }

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        tournament.getTeams().forEach(team -> channelManagement.createTeamChannel(tournament, team));

        messagingService.sendTextMessageToClient(uuid,
                TOURNAMENT_PREFIX + tournament.getName() + "' has been started successfully."
        );

        return new HashMap<>();
    }

    public Map<String, List<String>> startCurrentStage(String uuid, Tournament tournament) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        Stage stage = tournament.getCurrentStage();

        if (stage == null) {
            return createSingleResult(TOURNAMENT_KEY, "The tournament must have been started!");
        }

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (!stage.getState().equals(StageState.CREATED)) {
            return createSingleResult("stage", "A stage can only be started when it is in the CREATED state!");
        }

        stage.getEncounters().forEach(encounter -> {
                    final List<String> teamsInEncounter = encounter.getTeams()
                            .stream()
                            .map(Team::getName)
                            .sorted(Comparator.naturalOrder())
                            .collect(Collectors.toList());

                    encounter.getTeams().forEach(
                            team ->
                                    messagingService.messageTeam(
                                            team,
                                            "Your team now participates in the encounter with id '" + encounter.getId() + "'\n" +
                                                    "Participants: " + String.join(":", teamsInEncounter) + "\n"
                                    )
                    );
                }
        );

        stage.setState(StageState.STARTED);
        entityManagement.getEntityManager().persist(stage);

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        messagingService.sendTextMessageToClient(uuid,
                TOURNAMENT_PREFIX + tournament.getName() + "' had its current stage started!"
        );

        return new HashMap<>();
    }

    public Map<String, List<String>> reRollCurrentStage(String uuid, Tournament tournament) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        Stage currentStage = tournament.getCurrentStage();

        if (currentStage == null) {
            return createSingleResult(TOURNAMENT_KEY, "The tournament must have been started!");
        }

        if (!currentStage.getState().equals(StageState.CREATED)) {
            return createSingleResult("stage", "A stage can only be re-rolled when it is in the CREATED state!");
        }

        final List<Encounter> currentEncounters = new ArrayList<>(currentStage.getEncounters());

        currentEncounters.forEach(e -> e.getEncounterParticipations().forEach(
                ep -> {
                    e.removeEncounterParticipation(ep);
                    ep.getTeam().removeEncounterParticipation(ep);
                    entityManagement.getEntityManager().remove(ep);
                })
        );

        currentEncounters.forEach(e -> {
                    currentStage.removeEncounter(e);
                    entityManagement.getEntityManager().remove(e);
                }
        );

        if (currentStage.getType().isGroup()) {
            List<Group> groups = new ArrayList<>(currentStage.getGroups());

            groups.forEach(
                    group -> {
                        currentStage.removeGroup(group);
                        entityManagement.getEntityManager().remove(group);
                    }
            );
        }

        final List<StageFreePass> freePasses = new ArrayList<>(currentStage.getFreePasses());

        freePasses.forEach(
                freePass -> {
                    currentStage.removeFreePass(freePass);
                    entityManagement.getEntityManager().remove(freePass);
                }
        );

        final boolean encountersCreated = encounterGenerationService.generateStage(currentStage, false);

        if (!encountersCreated) {
            return createSingleResult(TOURNAMENT_KEY, "There are no encounters to be created!");
        }

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        messagingService.sendTextMessageToClient(uuid,
                TOURNAMENT_PREFIX + tournament.getName() + "' had its current stage re-rolled!"
        );

        return new HashMap<>();
    }

    public Map<String, List<String>> finishTournament(String uuid, Tournament tournament) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (tournament.getState().equals(TournamentState.FINISHED)) {
            return createSingleResult(TOURNAMENT_KEY, TOURNAMENT_PREFIX + tournament.getName() + "' is already finished!");
        }

        if (!tournament.getState().equals(TournamentState.STARTED)) {
            return createSingleResult(TOURNAMENT_KEY, TOURNAMENT_PREFIX + tournament.getName() + "' was not started!");
        }

        tournament.finish();

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        messagingService.sendTextMessageToClient(uuid, TOURNAMENT_PREFIX +
                tournament.getName() + "' has been finished successfully.");

        channelManagement.deleteTournamentChannel(tournament);

        return new HashMap<>();
    }

    public Map<String, List<String>> infoTournament(String uuid, Tournament tournament) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        List<Team> teams = tournament.getTournamentParticipations().stream()
                .map(TournamentParticipation::getTeam)
                .collect(Collectors.toList());

        if (!tournament.isLeader(uuid) && !tournament.getParticipantsUid().contains(uuid)) {
            return createSingleResult(
                    "user",
                    "Only the participants or the leader of this tournament are able to use this command!"
            );
        }

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        String teamList = teams.stream().map(Team::getName).collect(Collectors.joining("\n"));
        messagingService.sendTextMessageToClient(uuid,
                TOURNAMENT_PREFIX + tournament.getName() + "' consists of these teams:\n" + teamList
        );

        return new HashMap<>();
    }

    public Map<String, List<String>> messageTournament(String uuid, Tournament tournament, String message) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        String msg = "[Tournament - " + tournament.getName() + "] " + message;

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (message.isEmpty()) {
            return createSingleResult("message", "Message must be not empty!");
        }

        new HashSet<>(tournament.getParticipants()).stream()
                .map(Player::optTsUid)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(pUid -> messagingService.sendTextMessageToClient(pUid, msg, true));

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> paramTournament(String uuid, Tournament tournament, String key, String value) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (key.startsWith("##")) {
            return createSingleResult("param:key", "User added keys must not start with '##'!");
        }

        final Optional<TournamentParameter> tournamentParameter = tournament.optParameter(key);

        String message = PARAMETER_MSG_PREFIX;

        if (tournamentParameter.isEmpty()) {
            if (value == null) {
                return createSingleResult("param:key", "The tournament contains no parameter with the given key!");
            } else {
                tournament.createTournamentParameter(key, value);
                entityManagement.getEntityManager().persist(tournament);

                if (key.equals(TournamentParameter.SINGLE_PLAYER) && tournament.getTeams().stream().anyMatch(t -> !t.isSinglePlayer())) {
                    return createSingleResult("param:key", "The single-player-parameter can not be set, since non-single-player teams have already been added!");
                }

                message += key + "' was added and set to: " + value;
            }
        } else {
            if (value == null) {
                message += key + "' is set to: " + tournamentParameter.get().getValue();
            } else {
                tournamentParameter.get().setValue(value);

                message += key + "' was set to: " + value;
            }
        }

        messagingService.sendTextMessageToClient(uuid, message);

        entityManagement.getEntityManager().flush();

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> leaderTournament(String uuid, Tournament tournament, String leaderToken) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (leaderToken == null) {
            if (!tournament.isLeader(uuid)) {
                return getNotAllowedResponse();
            }
            String token = UUID.randomUUID().toString();

            tournament.createTournamentLeadershipToken();
            entityManagement.getEntityManager().persist(tournament);

            messagingService.sendTextMessageToClient(uuid, LEADER_TOKEN_PREFIX + tournament.getName()
                    + "': " + token);

            taskService.scheduleTask(
                    ITask.builder()
                            .delay(configService.getLeaderTokenTimeout(), TimeUnit.MINUTES)
                            .name("deleteTournamentLeaderToken:" + token)
                            .runnable(
                                    () -> entityManagement.getDeleteLeaderTokenService().deleteTournamentLeaderToken(token)
                            )
                            .build()
            );
        } else {

            if (tournament.isLeader(uuid)) {
                return createSingleResult("user", "You are already leader of tournament '" + tournament.getName() + "'");
            }

            if (leaderToken.isEmpty()) {
                return createSingleResult(TOKEN_KEY, "The leader token can not be empty!");
            }

            List<TournamentLeadershipToken> leadershipTokens = tournament.getTournamentLeadershipTokens().stream()
                    .filter(tournamentLeadershipToken -> tournamentLeadershipToken.getToken().equals(leaderToken))
                    .collect(Collectors.toList());

            if (leadershipTokens.isEmpty()) {
                return createSingleResult(TOKEN_KEY, LEADER_TOKEN_PREFIX + tournament.getName() + "' is invalid.");
            }

            if (leadershipTokens.get(0).getCreatedTsp().isBefore(ZonedDateTime.now().minusMinutes(configService.getLeaderTokenTimeout()))) {
                return createSingleResult(TOKEN_KEY, LEADER_TOKEN_PREFIX + tournament.getName() + "' is not valid anymore.");
            }

            tournament.createTournamentLeadership(uuid);
            entityManagement.getEntityManager().persist(tournament);

            leadershipTokens.forEach(entityManagement.getEntityManager()::remove);
            messagingService.sendTextMessageToClient(uuid, "You are now a leader of tournament '"
                    + tournament.getName() + "'");
        }

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        channelManagement.setChannelGroupForTournamentLeaders(tournament);

        return new HashMap<>();
    }

    public Map<String, List<String>> leaderRemove(String uuid, Tournament tournament, String leaderToRemoveUuid) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        String uuidToRemove;
        if (leaderToRemoveUuid == null) {
            uuidToRemove = uuid;
        } else {
            uuidToRemove = leaderToRemoveUuid;
        }

        Optional<TournamentLeadership> tournamentLeadership = tournament.optTournamentLeadership(uuidToRemove);

        if (tournamentLeadership.isEmpty()) {
            return createSingleResult(CLIENT_KEY, "This user is no leader of the tournament '" + tournament.getName() + "'");
        }

        if (tournament.getLeaders().size() == 1) {
            return createSingleResult("leader", "The last leader of a tournament cannot be removed!");
        }


        final TournamentLeadership leadership = tournamentLeadership.get();
        tournament.removeTournamentLeadership(leadership);
        entityManagement.getEntityManager().remove(leadership);

        entityManagement.getEntityManager().flush();
        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        messagingService.sendTextMessageToClient(
                uuidToRemove,
                "You are no longer leader of the tournament '" + tournament.getName() + "'",
                true
        );

        if (!uuidToRemove.equals(uuid)) {
            Optional<IUser> optUser = optUserByUuid(uuidToRemove);
            String nickName = optUser.isPresent() ? optUser.get().getNickName() : uuidToRemove;

            messagingService.sendTextMessageToClient(
                    uuid,
                    "The user '" + nickName + "' is no longer leader of the tournament '"
                            + tournament.getName() + "'"
            );
        }

        return new HashMap<>();
    }

    private Optional<IUser> optUserByUuid(String uuid) {
        return userService.findUserByUniqueID(uuid).stream().findFirst();
    }

    public Map<String, List<String>> listEncounters(String uuid, Tournament tournament) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        List<Encounter> encounters = tournament.getStages().stream().flatMap(stage -> stage.getEncounters().stream())
                .filter(encounter ->
                        encounter.getState().equals(EncounterState.STARTED)
                                || encounter.getState().equals(EncounterState.READY)
                                || encounter.getState().equals(EncounterState.PREPARING)
                                || encounter.getState().equals(EncounterState.CREATED)
                ).collect(Collectors.toList());

        String encounterList = encounters
                .stream().map(encounter -> {
                            String teamList = encounter.getEncounterParticipations()
                                    .stream()
                                    .map(EncounterParticipation::getTeam)
                                    .map(Team::getName)
                                    .collect(Collectors.joining(" - ")
                                    );

                            return "(" + encounter.getId() + " - " + encounter.getState() + ") " + teamList;
                        }
                )
                .collect(Collectors.joining("\n"));

        messagingService.sendTextMessageToClient(uuid,
                TOURNAMENT_PREFIX + tournament.getName() +
                        "' has these running encounters:\n" + encounterList
        );

        return new HashMap<>();
    }

    public Map<String, List<String>> claimTournament(String uuid, Tournament tournament) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        final IUser user = optUserByUuid(uuid).orElseThrow(
                () -> new IllegalStateException("User for uuid: " + uuid + " not found!")
        );

        if (!user.hasPermission(CommandPermissions.TOURNAMENT_CLAIM)) {
            return createSingleResult(TOURNAMENT_PERMISSION_KEY, "You do not have the permission to claim a tournament!");
        }

        if (tournament.isLeader(uuid)) {
            return createSingleResult("user", "You are already leader of tournament '"
                    + tournament.getName() + "'");
        }

        tournament.createTournamentLeadership(uuid);
        entityManagement.getEntityManager().persist(tournament);

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> forfeitForTeam(String uuid, Tournament tournament, Team team) {
        if (tournament.getState() == TournamentState.CREATED) {
            createSingleResult(TOURNAMENT_KEY, "The tournament has not started and can't be forfeited. Just leave the tournament instead.");
        }

        final Stage currentStage = tournament.getCurrentStage();
        if (currentStage == null || currentStage.getState() != StageState.STARTED) {
            createSingleResult(TOURNAMENT_KEY, "The current stage of the tournament has to be started to forfeit the tournament!");
        }

        return endTournamentForTeam(uuid, tournament, team, ParticipationState.FORFEITED);
    }

    public Map<String, List<String>> disqualifyTeam(String uuid, Tournament tournament, Team team) {
        final Stage currentStage = tournament.getCurrentStage();
        if ((currentStage == null || currentStage.getState() != StageState.STARTED) || tournament.getState() == TournamentState.FINISHED) {
            createSingleResult(TOURNAMENT_KEY, "The current stage of the tournament has to be started or the tournament must be finished to disqualify a team !");
        }

        return endTournamentForTeam(uuid, tournament, team, ParticipationState.DISQUALIFIED);
    }

    private Map<String, List<String>> endTournamentForTeam(String uuid, Tournament tournament, Team team, ParticipationState state) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        final Optional<TournamentParticipation> optTournamentParticipation = tournament.optTournamentParticipation(team);

        if (optTournamentParticipation.isEmpty()) {
            return createSingleResult(TEAM_KEY, TEAM_MSG_PREFIX + team.getName() + "' does not participate in the tournament!");
        }

        final ParticipationState participationState = optTournamentParticipation.get().getState();
        if (participationState == ParticipationState.DISQUALIFIED) {
            return createSingleResult(TEAM_KEY, TEAM_MSG_PREFIX + team.getName() + "' was already disqualified!");
        }

        if (participationState == ParticipationState.FORFEITED && state == participationState) {
            return createSingleResult(TEAM_KEY, TEAM_MSG_PREFIX + team.getName() + "' has already forfeited!");
        }


        final Stage currentStage = tournament.getCurrentStage();
        if (currentStage != null && currentStage.getType() != StageType.UNDEFINED) {
            tournament.getTournamentParticipation(team).setLostStage(currentStage);

            final List<Encounter> encounters = currentStage.getEncounters();

            encounters.stream()
                    .filter(e -> e.getTeams().contains(team))
                    .filter(e -> e.getState() == EncounterState.CREATED
                            || e.getState() == EncounterState.PREPARING
                            || e.getState() == EncounterState.READY
                            || e.getState() == EncounterState.STARTED)
                    .forEach(
                            e -> {
                                entityManagement.getEntityManager().refresh(e);
                                e.forfeit(team);
                            }
                    );

            if (!encounters.isEmpty()) {
                encounterGenerationService.processStageState(tournament);
            }
        }

        final TournamentParticipation tournamentParticipation = optTournamentParticipation.get();
        tournamentParticipation.setState(state);

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> createClassifier(String classifier, String description) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        classifierQuery.setParameter("classifier", classifier);
        final List<TournamentClassifier> classifiers = classifierQuery.getResultList();

        if (!classifiers.isEmpty()) {
            return createSingleResult(CLASSIFIER_KEY, "The classifier already exists!");
        }

        if (classifier.isBlank()) {
            return createSingleResult(CLASSIFIER_KEY, "The classifier must be not empty!");
        }

        final TournamentClassifier tournamentClassifier = new TournamentClassifier(classifier, description);
        entityManagement.getEntityManager().persist(tournamentClassifier);

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> setClassifier(String uuid, Tournament tournament, String classifier) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        classifierQuery.setParameter("classifier", classifier);
        final List<TournamentClassifier> classifiers = classifierQuery.getResultList();

        if (classifiers.isEmpty()) {
            return createSingleResult(CLASSIFIER_KEY, "The classifier does not exists!");
        }

        tournament.setClassifier(classifiers.get(0));
        entityManagement.getEntityManager().persist(tournament);

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }


    public Map<String, List<String>> createSeason(String name, ZonedDateTime start, ZonedDateTime end) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        seasonQuery.setParameter("name", name);
        final List<Season> seasons = seasonQuery.getResultList();

        if (!seasons.isEmpty()) {
            return createSingleResult(SEASON_KEY, "The season already exists!");
        }

        if (name.isBlank()) {
            return createSingleResult(SEASON_KEY, "The season name must be not empty!");
        }

        if (start.isAfter(end)) {
            return createSingleResult(SEASON_KEY, "The season start must be before the end!");
        }

        final Season season = new Season(name, start, end);
        entityManagement.getEntityManager().persist(season);

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    public Map<String, List<String>> setSeason(String uuid, Tournament tournament, Season season) {
        boolean transactionWasActive = entityManagement.beginTransaction();

        if (!tournament.isLeader(uuid)) {
            return getNotAllowedResponse();
        }

        tournament.setSeason(season);
        entityManagement.getEntityManager().persist(tournament);

        if (!transactionWasActive) {
            entityManagement.getEntityManager().getTransaction().commit();
        }

        return new HashMap<>();
    }

    private Map<String, List<String>> createSingleResult(String key, String errorMessage) {
        return Map.of(key, Collections.singletonList(errorMessage));
    }

    private Map<String, List<String>> getNotAllowedResponse() {
        return createSingleResult("user", ONLY_TOURNAMENT_LEADERS);
    }
}
