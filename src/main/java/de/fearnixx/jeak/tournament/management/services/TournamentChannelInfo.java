package de.fearnixx.jeak.tournament.management.services;

import de.fearnixx.jeak.tournament.entities.team.Team;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class TournamentChannelInfo {

    private int tournamentChannelId;
    private int lobbyChannelId;

    private Map<String, Integer> teamChannels = new HashMap<>();

    public TournamentChannelInfo(int tournamentChannelId) {
        this.tournamentChannelId = tournamentChannelId;
    }

    public void addTeamChannel(Team team, Integer channelId) {
        teamChannels.put(team.getName(), channelId);
    }

    public Optional<Integer> getTeamChannel(Team team) {
        return Optional.ofNullable(teamChannels.get(team.getName()));
    }

    public void setLobbyChannelId(int lobbyChannelId) {
        this.lobbyChannelId = lobbyChannelId;
    }

    public int getTournamentChannelId() {
        return tournamentChannelId;
    }

    public int getLobbyChannelId() {
        return lobbyChannelId;
    }
}
