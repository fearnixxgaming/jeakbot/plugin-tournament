package de.fearnixx.jeak.tournament.requestqueue;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.teamspeak.IUserService;
import de.fearnixx.jeak.teamspeak.IServer;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.query.IQueryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.Queue;

public class QueuedRequestExecutor {

    private static final Logger log = LoggerFactory.getLogger(QueuedRequestExecutor.class);

    @Inject
    private IServer server;

    @Inject
    private IUserService userService;

    public synchronized void executeStoredRequests(String uuid, Queue<IQueryRequest> requests) {
        if (!requests.isEmpty()) {
            executeStoredRequest(
                    requests.remove(),
                    requests,
                    getClientId(uuid)
            );
        }
    }

    private void executeStoredRequest(
            IQueryRequest currentRequest,
            Queue<IQueryRequest> followUpRequests,
            int clientId) {

        if (clientId == -1) {
            if (!followUpRequests.isEmpty()) {
                executeStoredRequest(followUpRequests.remove(), followUpRequests, clientId);
            }

            return;
        }

        log.debug("Executing stored request");

        replacePropertyIfPresent(currentRequest, "target", clientId);
        replacePropertyIfPresent(currentRequest, "cid", clientId);

        currentRequest.onDone(answer -> {

            if (!followUpRequests.isEmpty()) {
                executeStoredRequest(followUpRequests.remove(), followUpRequests, clientId);
            }
        });

        server.getConnection().sendRequest(currentRequest);
    }

    private void replacePropertyIfPresent(IQueryRequest currentRequest, String key, Object value) {
        currentRequest.getDataChain().stream()
                .filter(chain -> chain.hasProperty(key))
                .forEach(chain -> chain.setProperty(key, value));
    }

    private int getClientId(String uuid) {
        Optional<IClient> targetClient = userService.findClientByUniqueID(uuid).stream().findFirst();

        return targetClient.map(IClient::getClientID).orElse(-1);
    }
}
