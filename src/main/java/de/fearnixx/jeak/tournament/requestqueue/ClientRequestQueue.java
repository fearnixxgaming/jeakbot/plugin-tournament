package de.fearnixx.jeak.tournament.requestqueue;

import de.fearnixx.jeak.reflect.IInjectionService;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.query.IQueryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class ClientRequestQueue {

    private final Map<String, Queue<IQueryRequest>> clientRequestQueueMap;
    private QueuedRequestExecutor executor;

    public static final Logger log = LoggerFactory.getLogger(ClientRequestQueue.class);

    @Inject
    private IInjectionService injectionService;

    public ClientRequestQueue() {
        this.clientRequestQueueMap = new HashMap<>();
    }

    public void initialize() {
        this.executor = injectionService.injectInto(new QueuedRequestExecutor());
    }

    public void submitRequestForClient(String uuid, IQueryRequest request) {
        synchronized (clientRequestQueueMap) {

            if (!clientRequestQueueMap.containsKey(uuid)) {
                clientRequestQueueMap.put(uuid, new LinkedList<>());
                log.debug("Initialized request queue for client {}", uuid);
            }

            clientRequestQueueMap.get(uuid).add(request);
            log.debug("Submitted request for client {}", uuid);
        }
    }

    public void submitRequestForClient(IClient client, IQueryRequest request) {
        submitRequestForClient(client.getClientUniqueID(), request);
    }

    public boolean hasStoredRequests(String uuid) {
        synchronized (clientRequestQueueMap) {
            return clientRequestQueueMap.containsKey(uuid);
        }
    }

    public boolean hasStoredRequests(IClient client) {
        return hasStoredRequests(client.getClientUniqueID());
    }

    public void executeClientRequests(String uuid) {
        synchronized (clientRequestQueueMap) {
            if (!hasStoredRequests(uuid)) {
                log.debug("The client has no stored requests {}", uuid);
            }

            log.debug("Executing stored requests for client {}", uuid);

            executor.executeStoredRequests(uuid, clientRequestQueueMap.get(uuid));

            log.debug("Executed all stored requests for client {}", uuid);
            removeClientRequests(uuid);
        }
    }

    public void executeClientRequests(IClient client) {
        executeClientRequests(client.getClientUniqueID());
    }

    public void removeClientRequests(String uuid) {
        synchronized (clientRequestQueueMap) {
            clientRequestQueueMap.remove(uuid);
            log.debug("Removed request queue for client {}", uuid);
        }
    }

    public void removeClientRequests(IClient client) {
        removeClientRequests(client.getClientUniqueID());
    }
}
