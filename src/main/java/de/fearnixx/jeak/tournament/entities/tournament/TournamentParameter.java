package de.fearnixx.jeak.tournament.entities.tournament;

import javax.persistence.*;

@Entity(name = "TournamentParameter")
@Table(name = "TOURNAMENT_PARAMETER")
public class TournamentParameter {

    public static final String STAGE_PATTERN = "stage_pattern";
    public static final String GAME_TIME_TRACKING = "game_time_tracking";
    public static final String SINGLE_PLAYER = "single_player";
    public static final String MIN_TEAM_SIZE = "min_team_size";
    public static final String MAX_TEAM_SIZE = "max_team_size";
    public static final String REQUIRE_INVITATION = "require_invitation";
    public static final String FORFEIT_SCORE = "forfeit_score";
    public static final String SUPPRESS_PART_GEN = "suppress_part_gen";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TOURNAMENT_PARAMETER_ID")
    private Integer id;

    @JoinColumn(name = "TOURNAMENT_ID")
    @ManyToOne
    private Tournament tournament;

    @Column(name = "PARAMETER_KEY")
    private String key;

    @Column(name = "PARAMETER_VALUE")
    private String value;

    private TournamentParameter() {
    }

    TournamentParameter(Tournament tournament, String key) {
        this.tournament = tournament;
        this.key = key;
    }

    public Integer getId() {
        return id;
    }

    public Tournament getTournament() {
        return tournament;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
