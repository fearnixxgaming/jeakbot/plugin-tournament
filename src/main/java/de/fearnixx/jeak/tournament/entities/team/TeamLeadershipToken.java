package de.fearnixx.jeak.tournament.entities.team;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity(name = "TeamLeadershipToken")
@Table(name = "TEAM_LEADERSHIP_TOKEN")
public class TeamLeadershipToken implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TEAM_LEADERSHIP_TOKEN_ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "TEAM_ID")
    private Team team;

    @Column(name = "TOKEN")
    private String token = UUID.randomUUID().toString();

    @Column(name = "CREATED_TSP")
    private ZonedDateTime createdTsp = ZonedDateTime.now();

    private TeamLeadershipToken() {
    }

    TeamLeadershipToken(Team team) {
        this.team = team;
    }

    public Integer getId() {
        return id;
    }

    public Team getTeam() {
        return team;
    }

    public String getToken() {
        return token;
    }

    public ZonedDateTime getCreatedTsp() {
        return createdTsp;
    }

}
