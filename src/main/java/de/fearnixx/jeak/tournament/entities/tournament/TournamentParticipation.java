package de.fearnixx.jeak.tournament.entities.tournament;

import de.fearnixx.jeak.tournament.entities.stage.Stage;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.enums.ParticipationState;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Optional;

@Entity(name = "TournamentParticipation")
@Table(name = "TOURNAMENT_PARTICIPATION")
public class TournamentParticipation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TOURNAMENT_PARTICIPATION_ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "TEAM_ID")
    private Team team;

    @ManyToOne
    @JoinColumn(name = "TOURNAMENT_ID")
    private Tournament tournament;

    @Column(name = "JOIN_TSP")
    private ZonedDateTime joinTsp = ZonedDateTime.now();

    @Column(name = "STATE")
    @Enumerated(EnumType.STRING)
    private ParticipationState state = ParticipationState.JOINED;

    @Column(name = "ELIGIBLE_TO_PLAY")
    private Boolean eligibleToPlay = true;

    @ManyToOne
    @JoinColumn(name = "LOST_STAGE_ID")
    private Stage lostStage;

    private TournamentParticipation() {
    }

    TournamentParticipation(Team team, Tournament tournament) {
        this.team = team;
        this.tournament = tournament;
    }

    public Integer getId() {
        return id;
    }

    public Team getTeam() {
        return team;
    }

    public Tournament getTournament() {
        return tournament;
    }

    public ZonedDateTime getJoinTsp() {
        return joinTsp;
    }

    public ParticipationState getState() {
        return state;
    }

    public void setState(ParticipationState state) {
        this.eligibleToPlay =
                state != ParticipationState.DISQUALIFIED
                        && state != ParticipationState.FORFEITED
                        && state != ParticipationState.KNOCKED_OUT;

        this.state = state;
    }

    public boolean isEligibleToPlay() {
        return eligibleToPlay;
    }

    public void setLostStage(Stage lostStage) {
        this.lostStage = lostStage;
    }

    public Optional<Stage> optLostStage() {
        return Optional.ofNullable(lostStage);
    }
}
