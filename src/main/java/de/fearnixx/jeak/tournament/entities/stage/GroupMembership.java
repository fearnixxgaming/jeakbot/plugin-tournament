package de.fearnixx.jeak.tournament.entities.stage;

import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentParticipation;

import javax.persistence.*;

@Entity(name = "GroupMembership")
@Table(name = "GROUP_MEMBERSHIP")
public class GroupMembership {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "GROUP_MEMBERSHIP_ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "GROUP_ID")
    private Group group;

    @ManyToOne
    @JoinColumn(name = "TEAM_ID")
    private Team team;

    @Column(name = "GROUP_WON")
    private boolean groupWon;

    @Column(name = "POINTS")
    private int points;

    @Column(name = "ENCOUNTERS_WON")
    private int encountersWon;

    @Column(name = "MATCHES_WON")
    private int matchesWon;

    @Column(name = "ROUNDS_WON")
    private int roundsWon;

    private GroupMembership() {
    }

    GroupMembership(Group group, Team team) {
        this.group = group;
        this.team = team;
    }

    public Integer getId() {
        return id;
    }

    public Group getGroup() {
        return group;
    }

    public Team getTeam() {
        return team;
    }

    public TournamentParticipation getTournamentParticipation() {
        return group.getStage().getTournament().getTournamentParticipation(team);
    }

    void setGroupWon(boolean groupWon) {
        this.groupWon = groupWon;
    }

    void setPoints(int points) {
        this.points = points;
    }

    void setEncountersWon(int encountersWon) {
        this.encountersWon = encountersWon;
    }

    void setMatchesWon(int matchesWon) {
        this.matchesWon = matchesWon;
    }

    void setRoundsWon(int roundsWon) {
        this.roundsWon = roundsWon;
    }

    public boolean isGroupWon() {
        return groupWon;
    }

    public boolean isGroupLost() {
        return !groupWon;
    }

    public int getPoints() {
        return points;
    }

    public int getEncountersWon() {
        return encountersWon;
    }

    public int getMatchesWon() {
        return matchesWon;
    }

    public int getRoundsWon() {
        return roundsWon;
    }
}

