package de.fearnixx.jeak.tournament.entities.team;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

@Entity(name = "TeamLeadership")
@Table(name = "TEAM_LEADERSHIP")
public class TeamLeadership implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TEAM_LEADERSHIP_ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "TEAM_ID")
    private Team team;

    @Column(name = "UUID")
    private String uuid;

    @Column(name = "BEGIN_TSP")
    private ZonedDateTime beginTsp = ZonedDateTime.now();

    private TeamLeadership() {
    }

    TeamLeadership(Team team, String uuid) {
        this.team = team;
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public Team getTeam() {
        return team;
    }

    public String getUuid() {
        return uuid;
    }

    public ZonedDateTime getBeginTsp() {
        return beginTsp;
    }
}
