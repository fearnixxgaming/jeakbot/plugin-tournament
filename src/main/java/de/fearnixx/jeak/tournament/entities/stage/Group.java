package de.fearnixx.jeak.tournament.entities.stage;

import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentParameter;
import de.fearnixx.jeak.tournament.enums.StageType;

import javax.persistence.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Entity(name = "Group")
@Table(name = "GROUPS")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "GROUP_ID")
    private int id;

    @ManyToOne
    @JoinColumn(name = "STAGE_ID")
    private Stage stage;

    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "group", cascade = CascadeType.PERSIST)
    private List<GroupMembership> memberships = new ArrayList<>();

    @Column(name = "WINNING_TEAM_COUNT")
    private int winningTeamCount;

    private Group() {
    }

    Group(Stage stage, String name, int winningTeamCount) {
        this.stage = stage;
        this.name = name;
        this.winningTeamCount = winningTeamCount;
    }

    public int getId() {
        return id;
    }

    public Stage getStage() {
        return stage;
    }

    public String getName() {
        return name;
    }

    public List<GroupMembership> getMemberships() {
        return Collections.unmodifiableList(memberships);
    }

    public List<GroupMembership> getMembershipsSorted() {
        final ArrayList<GroupMembership> groupMemberships = new ArrayList<>(memberships);
        sortMemberships(groupMemberships);
        return groupMemberships;
    }

    public void refreshResult() {
        for (Team team : getTeams()) {
            AtomicInteger points = new AtomicInteger();
            AtomicInteger encountersWon = new AtomicInteger();
            AtomicInteger matchesWon = new AtomicInteger();
            AtomicInteger roundsWon = new AtomicInteger();

            getStage().getEncounters().stream()
                    .map(e -> e.optEncounterParticipation(team))
                    .filter(Optional::isPresent)
                    .map(Optional::get).forEach(
                    ep -> {
                        matchesWon.addAndGet(ep.getMatchesWon());
                        roundsWon.addAndGet(ep.getRoundsWon());

                        if (ep.isEncounterWon()) {
                            encountersWon.incrementAndGet();
                            points.addAndGet(3);
                        } else if (ep.isEncounterDrawn()) {
                            points.addAndGet(1);
                        }
                    }
            );

            final GroupMembership groupMembership = optMembership(team).orElseThrow();
            groupMembership.setRoundsWon(roundsWon.get());
            groupMembership.setMatchesWon(matchesWon.get());
            groupMembership.setEncountersWon(encountersWon.get());
            groupMembership.setPoints(points.get());
        }

        final ArrayList<GroupMembership> groupMembers = new ArrayList<>(getMemberships());
        sortMemberships(groupMembers);

        groupMembers.subList(0, winningTeamCount).forEach(gm -> gm.setGroupWon(true));
        groupMembers.subList(winningTeamCount, groupMembers.size()).forEach(gm -> gm.setGroupWon(false));
    }

    private void sortMemberships(ArrayList<GroupMembership> groupMembers) {
        //TFT-Group-ordering is different than all other stage-types. Only rounds won are counted.
        if (getStage().getType().equals(StageType.TFT)) {
            groupMembers.sort(Comparator.comparingInt(GroupMembership::getRoundsWon).reversed());
            return;
        }

        groupMembers.sort(
                (a, b) -> {
                    int compA = a.getPoints();
                    int compB = b.getPoints();
                    if (compA != compB) {
                        return compB - compA;
                    }

                    compA = a.getEncountersWon();
                    compB = b.getEncountersWon();
                    if (compA != compB) {
                        return compB - compA;
                    }

                    compA = a.getMatchesWon();
                    compB = b.getMatchesWon();
                    if (compA != compB) {
                        return compB - compA;
                    }

                    if (stage.getTournament().optParameter(TournamentParameter.GAME_TIME_TRACKING).isPresent()) {
                        //Game-Time-Tracking-Mode: Lower Avg. Round-Score of won games is better.
                        //Precondition: Only won matches contain a score (the game time)

                        if (compA == 0) {
                            //Nobody won any match => no sort
                            return 0;
                        }

                        float compTimeA = (float) a.getRoundsWon() / a.getMatchesWon();
                        float compTimeB = (float) b.getRoundsWon() / b.getMatchesWon();

                        return Float.compare(compTimeA, compTimeB);
                    }

                    return b.getRoundsWon() - a.getRoundsWon();
                }
        );
    }

    public List<Team> getTeams() {
        return memberships.stream().map(GroupMembership::getTeam).collect(Collectors.toUnmodifiableList());
    }

    public Optional<GroupMembership> optMembership(Team team) {
        return memberships.stream().filter(m -> m.getTeam().equals(team)).findFirst();
    }

    public void removeMembership(GroupMembership membership) {
        memberships.remove(membership);
    }

    public GroupMembership createMembership(Team team) {
        final GroupMembership groupMembership = new GroupMembership(this, team);
        memberships.add(groupMembership);
        return groupMembership;
    }

    public int getWinningTeamCount() {
        return winningTeamCount;
    }
}
