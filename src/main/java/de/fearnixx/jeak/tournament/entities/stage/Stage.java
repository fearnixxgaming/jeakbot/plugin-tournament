package de.fearnixx.jeak.tournament.entities.stage;

import de.fearnixx.jeak.tournament.entities.encounter.Encounter;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.Tournament;
import de.fearnixx.jeak.tournament.enums.StageState;
import de.fearnixx.jeak.tournament.enums.StageType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Entity(name = "Stage")
@Table(name = "STAGE")
public class Stage {

    private static final String GROUP_PREFIX = "Group ";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STAGE_ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "TOURNAMENT_ID")
    private Tournament tournament;

    @Column(name = "STATE")
    @Enumerated(EnumType.STRING)
    private StageState state = StageState.CREATED;

    @Column(name = "TYPE")
    private StageType type;

    @Column(name = "WINNING_TEAMS_PER_ENCOUNTER")
    private int winningTeamsPerEncounter;

    public Integer getId() {
        return id;
    }

    @OneToMany(mappedBy = "stage", cascade = CascadeType.PERSIST)
    private List<StageFreePass> freePasses = new ArrayList<>();

    @OneToMany(mappedBy = "stage", cascade = CascadeType.PERSIST)
    private List<Encounter> encounters = new ArrayList<>();

    @OneToMany(mappedBy = "stage", cascade = CascadeType.PERSIST)
    private List<Group> groups = new ArrayList<>();

    private Stage() {
    }

    public Stage(Tournament tournament, StageType type) {
        this.tournament = tournament;
        this.type = type;
    }

    public Tournament getTournament() {
        return tournament;
    }

    public StageState getState() {
        return state;
    }

    public void setState(StageState state) {
        this.state = state;
    }

    public StageType getType() {
        return type;
    }

    public void setType(StageType type) {
        this.type = type;
    }

    public List<StageFreePass> getFreePasses() {
        return Collections.unmodifiableList(freePasses);
    }

    public StageFreePass createFreePass(Team team) {
        final StageFreePass freePass = new StageFreePass(this, team);
        freePasses.add(freePass);
        return freePass;
    }

    public void removeFreePass(StageFreePass stageFreePass) {
        freePasses.remove(stageFreePass);
    }

    public List<Encounter> getEncounters() {
        return Collections.unmodifiableList(encounters);
    }

    public Encounter createEncounter() {
        final Encounter encounter = new Encounter(this);
        encounters.add(encounter);
        return encounter;
    }

    public void removeEncounter(Encounter encounter) {
        encounters.remove(encounter);
    }

    public Optional<Encounter> optEncounter(int encounterIndex) {
        if (encounters.size() > encounterIndex) {
            return Optional.of(encounters.get(encounterIndex));
        } else {
            return Optional.empty();
        }
    }

    public List<Group> getGroups() {
        if (!this.getType().isGroup()) {
            throw new IllegalStateException("Tried to access a group in a stage which is not a group stage!");
        }

        return Collections.unmodifiableList(groups);
    }

    public Optional<Group> optGroup(Team team) {
        if (!this.getType().isGroup()) {
            throw new IllegalStateException("Tried to access a group in a stage which is not a group stage!");
        }

        return groups.stream()
                .filter(
                        group -> group.getMemberships().stream()
                                .map(GroupMembership::getTeam)
                                .anyMatch(t -> t.equals(team))
                )
                .findFirst();
    }

    public Group createGroup(int winningTeamCount) {
        final Group group = new Group(this, GROUP_PREFIX + (this.groups.size() + 1), winningTeamCount);
        groups.add(group);
        return group;
    }

    public void removeGroup(Group group) {
        groups.remove(group);
    }

    public int getWinningTeamsPerEncounter() {
        return winningTeamsPerEncounter;
    }

    public void setWinningTeamsPerEncounter(int winningTeamsPerEncounter) {
        if (winningTeamsPerEncounter != 1) {
            throw new UnsupportedOperationException("Winning teams per encounter must be equal to 1! Any other value might lead to unforeseen results!");
        }

        this.winningTeamsPerEncounter = winningTeamsPerEncounter;
    }
}
