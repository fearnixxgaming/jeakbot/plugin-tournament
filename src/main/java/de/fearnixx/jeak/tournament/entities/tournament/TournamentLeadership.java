package de.fearnixx.jeak.tournament.entities.tournament;


import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity(name = "TournamentLeadership")
@Table(name = "TOURNAMENT_LEADERSHIP")
public class TournamentLeadership {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TOURNAMENT_LEADERSHIP_ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "TOURNAMENT_ID")
    private Tournament tournament;

    @Column(name = "UUID")
    private String uuid;

    @Column(name = "BEGIN_TSP")
    private ZonedDateTime beginTsp = ZonedDateTime.now();

    private TournamentLeadership() {
    }

    TournamentLeadership(Tournament tournament, String uuid) {
        this.tournament = tournament;
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public Tournament getTournament() {
        return tournament;
    }

    public String getUuid() {
        return uuid;
    }

    public ZonedDateTime getBeginTsp() {
        return beginTsp;
    }
}
