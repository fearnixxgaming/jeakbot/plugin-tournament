package de.fearnixx.jeak.tournament.entities.team;

import de.fearnixx.jeak.tournament.entities.AdditionalSeasonScore;
import de.fearnixx.jeak.tournament.entities.encounter.EncounterParticipation;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentParticipation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Entity(name = "Team")
@Table(name = "TEAM")
public class Team implements Serializable {

    public static final String SINGLE_PLAYER_TEAM_PREFIX = "#SPT#";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TEAM_ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TAG")
    private String tag;

    @Column(name = "CREATOR_UUID")
    private String creatorUuid;

    @Column(name = "JOIN_TOKEN")
    private String joinToken = UUID.randomUUID().toString();

    @Column(name = "SINGLE_PLAYER")
    private boolean singlePlayer;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "LOGO_URL")
    private String logoUrl;

    @OneToMany(mappedBy = "team", cascade = CascadeType.REFRESH)
    private List<TournamentParticipation> tournamentParticipations = new ArrayList<>();

    @OneToMany(mappedBy = "team", cascade = CascadeType.REFRESH)
    private List<EncounterParticipation> encounterParticipations = new ArrayList<>();

    @OneToMany(mappedBy = "team", cascade = CascadeType.PERSIST)
    private List<TeamLeadership> leaderships = new ArrayList<>();

    @OneToMany(mappedBy = "team", cascade = CascadeType.PERSIST)
    private List<TeamMembership> memberships = new ArrayList<>();

    @OneToMany(mappedBy = "team", cascade = CascadeType.PERSIST)
    private List<TeamLeadershipToken> teamLeadershipTokens = new ArrayList<>();

    @OneToMany(mappedBy = "team")
    private List<AdditionalSeasonScore> additionalSeasonScores = new ArrayList<>();

    private Team() {
    }

    public Team(String name, String creatorUuid) {
        this(name, creatorUuid, false);
    }

    public Team(Player player) {
        this(SINGLE_PLAYER_TEAM_PREFIX + player.getId() + "", player.optTsUid().orElse(""), true);
    }

    private Team(String name, String creatorUuid, boolean singlePlayer) {
        this.name = name;
        this.creatorUuid = creatorUuid;
        this.singlePlayer = singlePlayer;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag.toUpperCase();
    }

    public String getCreatorUuid() {
        return creatorUuid;
    }

    public String getJoinToken() {
        return joinToken;
    }

    public void setJoinToken(String joinToken) {
        this.joinToken = joinToken;
    }

    public boolean isSinglePlayer() {
        return singlePlayer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public List<TournamentParticipation> getTournamentParticipations() {
        return Collections.unmodifiableList(tournamentParticipations);
    }

    public void removeTournamentParticipation(TournamentParticipation tournamentParticipation) {
        tournamentParticipations.remove(tournamentParticipation);
    }

    public List<EncounterParticipation> getEncounterParticipations() {
        return Collections.unmodifiableList(encounterParticipations);
    }

    public void removeEncounterParticipation(EncounterParticipation encounterParticipation) {
        encounterParticipations.remove(encounterParticipation);
    }

    public List<TeamLeadership> getLeaderships() {
        return Collections.unmodifiableList(leaderships);
    }

    public TeamLeadership createLeadership(String uuid) {
        final TeamLeadership teamLeadership = new TeamLeadership(this, uuid);
        leaderships.add(teamLeadership);
        return teamLeadership;
    }

    public void removeLeadership(TeamLeadership teamLeadership) {
        leaderships.remove(teamLeadership);
    }

    public List<TeamMembership> getMemberships() {
        return Collections.unmodifiableList(memberships);
    }

    public TeamMembership createMembership(Player player, boolean active) {
        if (singlePlayer && !memberships.isEmpty()) {
            throw new IllegalStateException("A player can not be added to a single player team!");
        }

        final TeamMembership teamMembership = new TeamMembership(this, player, active);
        memberships.add(teamMembership);
        return teamMembership;
    }

    public void removeMembership(TeamMembership teamMembership) {
        memberships.remove(teamMembership);
    }

    public List<Player> getMembers() {
        return memberships.stream().map(TeamMembership::getPlayer).collect(Collectors.toList());
    }

    public List<Player> getActiveMembers() {
        return memberships.stream().filter(TeamMembership::isActive).map(TeamMembership::getPlayer).collect(Collectors.toList());
    }

    public List<String> getMembersUid() {
        return memberships.stream().map(TeamMembership::getPlayer)
                .map(Player::optTsUid)
                .filter(Optional::isPresent).map(Optional::get)
                .collect(Collectors.toList());
    }

    public List<String> getMembersPlayerName() {
        return memberships.stream().map(TeamMembership::getPlayer)
                .map(Player::optName)
                .filter(Optional::isPresent).map(Optional::get)
                .collect(Collectors.toList());
    }

    public Optional<TeamMembership> optTeamMembership(Player player) {
        return memberships.stream().filter(tm -> tm.getPlayer().equals(player)).findFirst();
    }

    public Optional<TeamMembership> optTeamMembershipByTsUid(String tsUid) {
        return memberships.stream().filter(tm -> tsUid.equals(tm.getPlayer().optTsUid().orElse(null))).findFirst();
    }

    public List<String> getLeaders() {
        return leaderships.stream().map(TeamLeadership::getUuid).collect(Collectors.toList());
    }

    public boolean isLeader(String uuid) {
        return leaderships.stream().map(TeamLeadership::getUuid).anyMatch(uuid::equals);
    }

    public Optional<TeamLeadership> optTeamLeadership(String uuid) {
        return leaderships.stream().filter(tl -> tl.getUuid().equals(uuid)).findFirst();
    }

    public List<TeamLeadershipToken> getTeamLeadershipTokens() {
        return Collections.unmodifiableList(teamLeadershipTokens);
    }

    public TeamLeadershipToken createTeamLeadershipToken() {
        final TeamLeadershipToken leadershipToken = new TeamLeadershipToken(this);
        teamLeadershipTokens.add(leadershipToken);
        return leadershipToken;
    }

    public void removeTeamLeadershipToken(TeamLeadershipToken teamLeadershipToken) {
        teamLeadershipTokens.remove(teamLeadershipToken);
    }

    public int getAdditionalSeasonScore() {
        return additionalSeasonScores.stream().mapToInt(AdditionalSeasonScore::getAmount).sum();
    }
}
