package de.fearnixx.jeak.tournament.entities.encounter;

import de.fearnixx.jeak.tournament.entities.stage.Group;
import de.fearnixx.jeak.tournament.entities.stage.Stage;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentParticipation;
import de.fearnixx.jeak.tournament.enums.StageType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Entity(name = "EncounterParticipation")
@Table(name = "ENCOUNTER_PARTICIPATION")
public class EncounterParticipation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ENCOUNTER_PARTICIPATION_ID")
    private Integer id;

    @JoinColumn(name = "ENCOUNTER_ID")
    @ManyToOne
    private Encounter encounter;

    @JoinColumn(name = "TEAM_ID")
    @ManyToOne
    private Team team;

    @Column(name = "READY")
    private Boolean ready = false;

    @Column(name = "ENCOUNTER_RESULT")
    private int encounterResult;

    @Column(name = "MATCHES_WON")
    private int matchesWon;

    @Column(name = "ROUNDS_WON")
    private int roundsWon;

    private EncounterParticipation() {
    }

    EncounterParticipation(Encounter encounter, Team team) {
        this.encounter = encounter;
        this.team = team;
    }

    public Integer getId() {
        return id;
    }

    public Encounter getEncounter() {
        return encounter;
    }

    public Team getTeam() {
        return team;
    }

    public TournamentParticipation getTournamentParticipation() {
        return encounter.getStage().getTournament().getTournamentParticipation(team);
    }

    public Boolean isReady() {
        return ready;
    }

    public void toggleReady() {
        this.ready = !this.ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public boolean isEncounterLost() {
        return encounterResult == 0;
    }

    public boolean isEncounterWon() {
        return encounterResult == 1;
    }

    public boolean isEncounterDrawn() {
        return encounterResult == 2;
    }

    void incrementMatchesWon() {
        this.matchesWon++;
        refreshEncounterResult();
    }

    void decrementMatchesWon() {
        this.matchesWon--;
        refreshEncounterResult();
    }

    private void refreshEncounterResult() {
        final List<EncounterParticipation> encounterParticipations = new ArrayList<>(encounter.getEncounterParticipations());
        encounterParticipations.sort(Comparator.comparingInt(EncounterParticipation::getMatchesWon).reversed());

        final EncounterParticipation firstEp = encounterParticipations.get(0);
        final EncounterParticipation secondEp = encounterParticipations.get(1);

        if (firstEp.getMatchesWon() > secondEp.getMatchesWon()) {
            firstEp.encounterResult = 1;

            for (int i = 1; i < encounterParticipations.size(); i++) {
                encounterParticipations.get(i).encounterResult = 0;
            }
        } else {
            encounterParticipations.forEach(encounterParticipation -> encounterParticipation.encounterResult = 2);
        }
    }

    public int getMatchesWon() {
        return matchesWon;
    }

    void addRoundsWon(int roundsWon) {
        this.roundsWon += roundsWon;
    }

    public int getRoundsWon() {
        return roundsWon;
    }
}
