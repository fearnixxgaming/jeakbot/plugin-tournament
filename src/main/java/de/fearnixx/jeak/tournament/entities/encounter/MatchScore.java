package de.fearnixx.jeak.tournament.entities.encounter;

import javax.persistence.*;

@Entity(name = "MatchScore")
@Table(name = "MATCH_SCORE")
public class MatchScore {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MATCH_SCORE_ID")
    private Integer id;

    @JoinColumn(name = "MATCH_ID")
    @ManyToOne
    private Match match;

    @JoinColumn(name = "ENCOUNTER_PARTICIPATION_ID")
    @ManyToOne
    private EncounterParticipation encounterParticipation;

    @Column(name = "ROUNDS_WON")
    private Integer roundsWon;

    private MatchScore() {
    }

    MatchScore(Match match, EncounterParticipation encounterParticipation, Integer roundsWon) {
        this.match = match;
        this.encounterParticipation = encounterParticipation;
        this.roundsWon = roundsWon;
    }

    public Integer getId() {
        return id;
    }

    public Match getMatch() {
        return match;
    }

    public EncounterParticipation getEncounterParticipation() {
        return encounterParticipation;
    }

    public Integer getRoundsWon() {
        return roundsWon;
    }
}
