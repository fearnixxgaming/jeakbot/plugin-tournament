package de.fearnixx.jeak.tournament.entities.encounter;

import de.fearnixx.jeak.tournament.entities.stage.GroupMembership;
import de.fearnixx.jeak.tournament.entities.stage.Stage;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.Tournament;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentParameter;
import de.fearnixx.jeak.tournament.enums.EncounterState;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Entity(name = "Encounter")
@Table(name = "ENCOUNTER")
public class Encounter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ENCOUNTER_ID")
    private Integer id;

    @JoinColumn(name = "STAGE_ID")
    @ManyToOne
    private Stage stage;

    @Column(name = "STATE")
    @Enumerated(EnumType.STRING)
    private EncounterState state = EncounterState.CREATED;

    @Column(name = "HOSTER")
    private String hoster;

    @Column(name = "CREATED_TSP")
    private ZonedDateTime createdTsp = ZonedDateTime.now();

    @Column(name = "STARTED_TSP")
    private ZonedDateTime startedTsp;

    @Column(name = "FINISHED_TSP")
    private ZonedDateTime finishedTsp;

    @OneToMany(mappedBy = "encounter", cascade = CascadeType.PERSIST)
    private List<EncounterParticipation> encounterParticipations = new ArrayList<>();

    @OneToMany(mappedBy = "encounter", cascade = CascadeType.PERSIST)
    private List<Match> matches = new ArrayList<>();

    private Encounter() {
    }

    public Encounter(Stage stage) {
        this.stage = stage;
    }

    public Integer getId() {
        return id;
    }

    public Stage getStage() {
        return stage;
    }

    public EncounterState getState() {
        return state;
    }

    public void setState(EncounterState state) {
        this.state = state;
    }

    public String getHoster() {
        return hoster;
    }

    public void setHoster(String hoster) {
        this.hoster = hoster;
    }

    public ZonedDateTime getCreatedTsp() {
        return createdTsp;
    }

    public ZonedDateTime getStartedTsp() {
        return startedTsp;
    }

    public ZonedDateTime getFinishedTsp() {
        return finishedTsp;
    }

    public void start() {
        state = EncounterState.STARTED;
        this.startedTsp = ZonedDateTime.now();
    }

    public void finish() {
        state = EncounterState.FINISHED;
        this.finishedTsp = ZonedDateTime.now();
    }

    public List<EncounterParticipation> getEncounterParticipations() {
        return getEncounterParticipations(false);
    }

    public List<EncounterParticipation> getEncounterParticipations(boolean ignoreSort) {
        final ArrayList<EncounterParticipation> encounterParticipationList = new ArrayList<>(this.encounterParticipations);

        if (!ignoreSort) {
            encounterParticipationList.sort(Comparator.comparingInt(EncounterParticipation::getId));
        }

        return Collections.unmodifiableList(encounterParticipationList);
    }

    public EncounterParticipation createEncounterParticipation(Team team) {
        final EncounterParticipation encounterParticipation = new EncounterParticipation(this, team);
        encounterParticipations.add(encounterParticipation);
        return encounterParticipation;
    }

    public void createEncounterParticipations(Team... teams) {
        Arrays.stream(teams).forEach(this::createEncounterParticipation);
    }

    public void createEncounterParticipations(List<Team> teams) {
        teams.forEach(this::createEncounterParticipation);
    }

    public void removeEncounterParticipation(EncounterParticipation encounterParticipation) {
        encounterParticipations.remove(encounterParticipation);
    }

    public List<Match> getMatches() {
        return Collections.unmodifiableList(this.matches);
    }

    public Match createMatch() {
        final Match match = new Match(this);
        matches.add(match);
        return match;
    }

    public void removeMatch(Match match) {
        matches.remove(match);
    }

    public List<Team> getTeams() {
        return getEncounterParticipations().stream()
                .map(EncounterParticipation::getTeam)
                .collect(Collectors.toUnmodifiableList());
    }

    public Set<String> getTeamLeaderUuids() {
        return encounterParticipations.stream()
                .map(EncounterParticipation::getTeam)
                .flatMap(team -> team.getLeaders().stream())
                .collect(Collectors.toSet());
    }

    public Optional<EncounterParticipation> optEncounterParticipation(Team team) {
        return encounterParticipations.stream().filter(ep -> ep.getTeam().equals(team)).findFirst();
    }

    public EncounterParticipation getEncounterParticipation(Team team) {
        return optEncounterParticipation(team).orElseThrow(
                () -> new IllegalArgumentException(
                        "The encounter with ID '" + getId() + "' has no encounter participation for the team '" + team.getName() + "'!"
                )
        );
    }

    public void forfeit(Team team) {
        if (getTeams().size() > 2) {
            throw new IllegalStateException("Cannot forfeit an encounter with more than 2 participants. Enter a score of 0 instead!");
        }

        final Tournament tournament = stage.getTournament();
        final int roundsWon = tournament.optParameter(TournamentParameter.FORFEIT_SCORE)
                .map(s -> Integer.parseInt(s.getValue()))
                .orElse(7);

        final boolean gameTracking = tournament.optParameter(TournamentParameter.GAME_TIME_TRACKING).isPresent();

        final List<Team> otherTeams = getTeams().stream()
                .filter(otherTeam -> !otherTeam.equals(team))
                .collect(Collectors.toList());

        if (matches.isEmpty()) {
            final Match match = createMatch();
            List<MatchScore> matchScores = new ArrayList<>();
            otherTeams.forEach(t -> {
                        int fRoundsWon = roundsWon;

                        if (gameTracking) {
                            if (stage.getType().isGroup()) {
                                final GroupMembership gm = stage.optGroup(t).orElseThrow().optMembership(t).orElseThrow();
                                fRoundsWon = gm.getMatchesWon() == 0 ? 2400 : gm.getRoundsWon() / gm.getMatchesWon();
                            } else {
                                fRoundsWon = 2400;
                            }
                        }

                        matchScores.add(match.createMatchScore(t, fRoundsWon));
                    }
            );

            matchScores.add(match.createMatchScore(team, 0));
            match.setResult(matchScores.toArray(new MatchScore[0]));
        } else {
            for (Match match : matches) {
                List<MatchScore> matchScores = new ArrayList<>();
                otherTeams.forEach(otherTeam -> matchScores.add(match.optMatchScore(otherTeam).orElseThrow()));
                match.createMatchScore(team, 0);
                match.setResult(matchScores.toArray(new MatchScore[0]));
            }
        }

        state = EncounterState.FORFEITED;
    }
}
