package de.fearnixx.jeak.tournament.entities.tournament;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "TournamentClassifier")
@Table(name = "TOURNAMENT_CLASSIFIER")
public class TournamentClassifier implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TOURNAMENT_CLASSIFIER_ID")
    private int id;

    @Column(name = "CLASSIFIER")
    private String classifier;

    @Column(name = "DESCRIPTION")
    private String description;

    private TournamentClassifier() {
    }

    public TournamentClassifier(String classifier, String description) {
        this.classifier = classifier;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getClassifier() {
        return classifier;
    }

    public String getDescription() {
        return description;
    }
}
