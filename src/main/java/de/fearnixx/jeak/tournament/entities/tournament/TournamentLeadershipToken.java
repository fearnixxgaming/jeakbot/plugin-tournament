package de.fearnixx.jeak.tournament.entities.tournament;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity(name = "TournamentLeadershipToken")
@Table(name = "TOURNAMENT_LEADERSHIP_TOKEN")
public class TournamentLeadershipToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TOURNAMENT_LEADERSHIP_TOKEN_ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "TOURNAMENT_ID")
    private Tournament tournament;

    @Column(name = "TOKEN")
    private String token = UUID.randomUUID().toString();

    @Column(name = "CREATED_TSP")
    private ZonedDateTime createdTsp = ZonedDateTime.now();

    private TournamentLeadershipToken() {
    }

    TournamentLeadershipToken(Tournament tournament) {
        this.tournament = tournament;
    }

    public Integer getId() {
        return id;
    }

    public Tournament getTournament() {
        return tournament;
    }

    public String getToken() {
        return token;
    }

    public ZonedDateTime getCreatedTsp() {
        return createdTsp;
    }
}
