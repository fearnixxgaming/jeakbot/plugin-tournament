package de.fearnixx.jeak.tournament.entities.stage;

import de.fearnixx.jeak.tournament.entities.team.Team;

import javax.persistence.*;

@Entity(name = "StageFreePass")
@Table(name = "STAGE_FREE_PASS")
public class StageFreePass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STAGE_FREE_PASS_ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "STAGE_ID")
    private Stage stage;

    @ManyToOne
    @JoinColumn(name = "TEAM_ID")
    private Team team;

    private StageFreePass() {
    }

    StageFreePass(Stage stage, Team team) {
        this.stage = stage;
        this.team = team;
    }

    public Integer getId() {
        return id;
    }

    public Stage getStage() {
        return stage;
    }

    public Team getTeam() {
        return team;
    }
}
