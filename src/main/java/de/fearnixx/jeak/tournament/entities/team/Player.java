package de.fearnixx.jeak.tournament.entities.team;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Optional;

@Entity(name = "Player")
@Table(name = "PLAYER")
public class Player implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PLAYER_ID")
    private int id;

    @Column(name = "TS_UID")
    private String tsUid;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ALLOWED_TO_PLAY")
    private Boolean allowedToPlay = true;

    private Player() {
    }

    public static Player createByName(String name) {
        final Player player = new Player();
        player.name = name;
        return player;
    }

    public static Player createByTsUid(String tsUid) {
        final Player player = new Player();
        player.tsUid = tsUid;
        return player;
    }

    public int getId() {
        return id;
    }

    public void setTsUid(String tsUid) {
        this.tsUid = tsUid;
    }

    public Optional<String> optTsUid() {
        return Optional.ofNullable(tsUid);
    }

    public void setName(String name) {
        this.name = name;
    }

    public Optional<String> optName() {
        return Optional.ofNullable(name);
    }

    public Boolean isAllowedToPlay() {
        return allowedToPlay;
    }

    public void setAllowedToPlay(Boolean allowedToPlay) {
        this.allowedToPlay = allowedToPlay;
    }
}
