package de.fearnixx.jeak.tournament.entities;

import de.fearnixx.jeak.tournament.entities.team.Team;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "AdditionalSeasonScore")
@Table(name = "ADDITIONAL_SEASON_SCORE")
public class AdditionalSeasonScore implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ADDITIONAL_SEASON_SCORE_ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "TEAM_ID")
    private Team team;

    @Column(name = "REASON")
    private String reason;

    @Column(name = "AMOUNT")
    private Integer amount;

    private AdditionalSeasonScore() {
    }

    public Integer getId() {
        return id;
    }

    public Team getTeam() {
        return team;
    }

    public String getReason() {
        return reason;
    }

    public int getAmount() {
        return amount;
    }
}
