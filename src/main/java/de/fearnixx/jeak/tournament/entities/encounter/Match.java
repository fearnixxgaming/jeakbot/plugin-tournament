package de.fearnixx.jeak.tournament.entities.encounter;

import de.fearnixx.jeak.tournament.entities.stage.Group;
import de.fearnixx.jeak.tournament.entities.stage.Stage;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.enums.StageType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Entity(name = "Match")
@Table(name = "MATCHES")
public class Match implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MATCH_ID")
    private int id;

    @JoinColumn(name = "ENCOUNTER_ID")
    @ManyToOne
    private Encounter encounter;

    @OneToMany(mappedBy = "match", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<MatchScore> matchScores = new ArrayList<>();

    private Match() {
    }

    Match(Encounter encounter) {
        this.encounter = encounter;
    }

    public int getId() {
        return id;
    }

    public Encounter getEncounter() {
        return encounter;
    }

    public List<MatchScore> getMatchScores() {
        return Collections.unmodifiableList(matchScores);
    }

    public Map<String, Integer> getMatchScoresByTeamName() {
        Map<String, Integer> scores = new HashMap<>();
        matchScores.forEach(
                matchScore -> scores.put(
                        matchScore.getEncounterParticipation().getTeam().getName(),
                        matchScore.getRoundsWon())
        );
        return scores;
    }

    public Optional<MatchScore> optMatchScore(Team team) {
        return matchScores.stream()
                .filter(matchScore -> matchScore.getEncounterParticipation().getTeam().equals(team))
                .findFirst();
    }

    public MatchScore createMatchScore(Team team, int roundsWon) {
        return new MatchScore(this, encounter.getEncounterParticipation(team), roundsWon);
    }

    public void setResult(MatchScore... matchScores) {
        if (matchScores == null) {
            throw new IllegalStateException("The match scores of a result must be non-null!");
        }

        if (matchScores.length != encounter.getEncounterParticipations().size()) {
            throw new IllegalStateException(
                    "The match scores of a result must have the same size as the number of encounter participations!"
            );
        }

        final List<MatchScore> matchScoreList = Arrays.asList(matchScores);
        final List<EncounterParticipation> resultEps = matchScoreList.stream()
                .map(MatchScore::getEncounterParticipation)
                .collect(Collectors.toList());

        if (!resultEps.containsAll(encounter.getEncounterParticipations())) {
            throw new IllegalStateException(
                    "The match scores of a result must contain a score for every participation of an encounter!"
            );
        }

        this.matchScores.clear();

        //Add match scores and rounds won
        matchScoreList.forEach(
                matchScore -> {
                    matchScore.getEncounterParticipation().addRoundsWon(matchScore.getRoundsWon());
                    this.matchScores.add(matchScore);
                }
        );

        //Increment matches won, if the result is not a draw
        matchScoreList.sort(Comparator.comparingInt(MatchScore::getRoundsWon).reversed());

        final MatchScore firstScore = matchScoreList.get(0);
        final MatchScore secondScore = matchScoreList.get(1);

        if (firstScore.getRoundsWon() > secondScore.getRoundsWon()) {
            firstScore.getEncounterParticipation().incrementMatchesWon();
        }

        refreshGroup();
    }

    public void clearResult() {
        if (matchScores.isEmpty()) {
            return;
        }

        final List<MatchScore> matchScoreList = new ArrayList<>(matchScores);

        matchScoreList.forEach(mS -> mS.getEncounterParticipation().addRoundsWon(-1 * mS.getRoundsWon()));

        //Decrement matches won, if the result was not a draw
        matchScoreList.sort(Comparator.comparingInt(MatchScore::getRoundsWon).reversed());

        final MatchScore firstScore = matchScoreList.get(0);
        final MatchScore secondScore = matchScoreList.get(1);

        if (firstScore.getRoundsWon() > secondScore.getRoundsWon()) {
            firstScore.getEncounterParticipation().decrementMatchesWon();
        }

        refreshGroup();

        this.matchScores.clear();
    }

    private void refreshGroup() {
        final Stage stage = encounter.getStage();

        if (stage.getType().isGroup()) {
            final Team team = matchScores.get(0).getEncounterParticipation().getTeam();

            final Group group = stage.optGroup(team).orElseThrow(
                    () -> new IllegalStateException(
                            "No group found for team '" + team.getName() + "' in tournament '"
                                    + stage.getTournament().getName() + "'!")
            );

            group.refreshResult();
        }
    }
}
