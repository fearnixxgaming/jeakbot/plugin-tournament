package de.fearnixx.jeak.tournament.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

@Entity(name = "Season")
@Table(name = "SEASON")
public class Season implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SEASON_ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "START")
    private ZonedDateTime startDate;

    @Column(name = "END")
    private ZonedDateTime endDate;

    private Season() {
    }

    public Season(String name, ZonedDateTime start, ZonedDateTime end) {
        this.name = name;
        this.startDate = start;
        this.endDate = end;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ZonedDateTime getStart() {
        return startDate;
    }

    public ZonedDateTime getEnd() {
        return endDate;
    }
}
