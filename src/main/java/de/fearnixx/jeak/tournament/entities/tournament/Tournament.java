package de.fearnixx.jeak.tournament.entities.tournament;

import de.fearnixx.jeak.tournament.entities.Season;
import de.fearnixx.jeak.tournament.entities.encounter.Encounter;
import de.fearnixx.jeak.tournament.entities.stage.Stage;
import de.fearnixx.jeak.tournament.entities.team.Player;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.enums.ParticipationState;
import de.fearnixx.jeak.tournament.enums.StageType;
import de.fearnixx.jeak.tournament.enums.TournamentState;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Entity(name = "Tournament")
@Table(name = "TOURNAMENT")
public class Tournament implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TOURNAMENT_ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CREATOR_UUID")
    private String creatorUuid;

    @Column(name = "STATE")
    @Enumerated(EnumType.STRING)
    private TournamentState state = TournamentState.CREATED;

    @JoinColumn(name = "CURRENT_STAGE_ID")
    @OneToOne
    private Stage currentStage;

    @Column(name = "CREATED_TSP")
    private ZonedDateTime createdTsp = ZonedDateTime.now();

    @Column(name = "STARTED_TSP")
    private ZonedDateTime startedTsp;

    @Column(name = "FINISHED_TSP")
    private ZonedDateTime finishedTsp;

    @JoinColumn(name = "SEASON_ID")
    @OneToOne
    private Season season;

    @JoinColumn(name = "TOURNAMENT_CLASSIFIER_ID")
    @OneToOne
    private TournamentClassifier classifier;

    @OneToMany(mappedBy = "tournament", cascade = CascadeType.PERSIST)
    private List<TournamentParticipation> tournamentParticipations = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "TOURNAMENT_PARTICIPATION",
            joinColumns = @JoinColumn(name = "TOURNAMENT_ID"),
            inverseJoinColumns = @JoinColumn(name = "TEAM_ID")
    )
    private List<Team> teams = new ArrayList<>();

    @OneToMany(mappedBy = "tournament", cascade = CascadeType.PERSIST)
    private List<TournamentLeadership> tournamentLeaderships = new ArrayList<>();

    @OneToMany(mappedBy = "tournament", cascade = CascadeType.PERSIST)
    private List<TournamentLeadershipToken> tournamentLeadershipTokens = new ArrayList<>();

    @OneToMany(mappedBy = "tournament", cascade = CascadeType.PERSIST)
    private List<TournamentParameter> tournamentParameters = new ArrayList<>();

    @OneToMany(mappedBy = "tournament", cascade = CascadeType.PERSIST)
    private List<Stage> stages = new ArrayList<>();

    private Tournament() {
    }

    public Tournament(String name, String creatorUuid) {
        this.name = name;
        this.creatorUuid = creatorUuid;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TournamentState getState() {
        return state;
    }

    public void setState(TournamentState state) {
        this.state = state;
    }

    public Stage getCurrentStage() {
        return currentStage;
    }

    public Stage createStage(StageType stageType) {
        return new Stage(this, stageType);
    }

    public void setAndAddNewStage(Stage stage) {
        stages.add(stage);
        this.currentStage = stage;
    }

    public void removeStage(Stage stage) {
        stages.remove(stage);
    }

    public ZonedDateTime getCreatedTsp() {
        return createdTsp;
    }

    public ZonedDateTime getStartedTsp() {
        return startedTsp;
    }

    public void start() {
        state = TournamentState.STARTED;

        tournamentParticipations.stream()
                .filter(
                        tp -> tp.getState() != ParticipationState.FORFEITED
                                && tp.getState() != ParticipationState.DISQUALIFIED)
                .forEach(tp -> tp.setState(ParticipationState.ACTIVE));

        this.startedTsp = ZonedDateTime.now();
    }

    public String getCreatorUuid() {
        return creatorUuid;
    }

    public ZonedDateTime getFinishedTsp() {
        return finishedTsp;
    }

    public void finish() {
        state = TournamentState.FINISHED;
        this.finishedTsp = ZonedDateTime.now();
    }

    public List<TournamentParticipation> getTournamentParticipations() {
        return Collections.unmodifiableList(tournamentParticipations);
    }

    public TournamentParticipation createTournamentParticipation(Team team) {
        final TournamentParticipation tournamentParticipation = new TournamentParticipation(team, this);
        tournamentParticipations.add(tournamentParticipation);
        return tournamentParticipation;
    }

    public void removeTournamentParticipation(TournamentParticipation tournamentParticipation) {
        tournamentParticipations.remove(tournamentParticipation);
    }

    public List<TournamentLeadership> getTournamentLeaderships() {
        return tournamentLeaderships;
    }

    public TournamentLeadership createTournamentLeadership(String uuid) {
        final TournamentLeadership tournamentLeadership = new TournamentLeadership(this, uuid);
        tournamentLeaderships.add(tournamentLeadership);
        return tournamentLeadership;
    }

    public void removeTournamentLeadership(TournamentLeadership tournamentLeadership) {
        tournamentLeaderships.remove(tournamentLeadership);
    }

    public List<TournamentLeadershipToken> getTournamentLeadershipTokens() {
        return tournamentLeadershipTokens;
    }

    public TournamentLeadershipToken createTournamentLeadershipToken() {
        final TournamentLeadershipToken leadershipToken = new TournamentLeadershipToken(this);
        tournamentLeadershipTokens.add(leadershipToken);
        return leadershipToken;
    }

    public void removeTournamentLeadershipToken(TournamentLeadershipToken tournamentLeadershipToken) {
        tournamentLeadershipTokens.remove(tournamentLeadershipToken);
    }

    public Map<String, String> getParameters() {
        Map<String, String> out = new HashMap<>();

        tournamentParameters.forEach(
                tParam -> out.put(tParam.getKey(), tParam.getValue())
        );

        return out;
    }

    public TournamentParameter createTournamentParameter(String key) {
        final TournamentParameter tournamentParameter = new TournamentParameter(this, key);
        tournamentParameters.add(tournamentParameter);
        return tournamentParameter;
    }

    public TournamentParameter createTournamentParameter(String key, String value) {
        final TournamentParameter tournamentParameter = createTournamentParameter(key);
        tournamentParameter.setValue(value);
        return tournamentParameter;
    }

    public Optional<TournamentParameter> optParameter(String key) {
        return tournamentParameters.stream()
                .filter(tp -> tp.getKey().equals(key))
                .findFirst();
    }

    public Optional<String> optParameterValue(String key) {
        return tournamentParameters.stream()
                .filter(tp -> tp.getKey().equals(key))
                .findFirst().map(TournamentParameter::getValue);
    }

    public List<Stage> getStages() {
        List<Stage> stageList = new ArrayList<>(stages);
        stageList.sort(Comparator.comparingInt(Stage::getId));
        return Collections.unmodifiableList(stageList);
    }

    public Optional<Stage> optStage(int stageIndex) {
        if (stages.size() < stageIndex + 1) {
            return Optional.empty();
        }

        return Optional.of(stages.get(stageIndex));
    }

    public List<Team> getTeams() {
        List<Team> teamList = new ArrayList<>(teams);
        teamList.sort(Comparator.comparing(Team::getName));
        return Collections.unmodifiableList(teamList);
    }

    public Optional<TournamentParticipation> optTournamentParticipation(Team team) {
        return tournamentParticipations.stream().filter(tp -> tp.getTeam().equals(team)).findFirst();
    }

    public TournamentParticipation getTournamentParticipation(Team team) {
        return optTournamentParticipation(team).orElseThrow(
                () -> new IllegalStateException("The team '" + team.getName() + "' does not participate in the tournament '" + name + "'!")
        );
    }

    public Optional<TournamentLeadership> optTournamentLeadership(String uuid) {
        return tournamentLeaderships.stream().filter(tl -> tl.getUuid().equals(uuid)).findFirst();
    }

    public Set<String> getLeaders() {
        return tournamentLeaderships.stream().map(TournamentLeadership::getUuid).collect(Collectors.toSet());
    }

    public boolean isLeader(String uuid) {
        return tournamentLeaderships.stream().map(TournamentLeadership::getUuid).anyMatch(uuid::equals);
    }

    public Set<Player> getParticipants() {
        return teams.stream().flatMap(team -> team.getMembers().stream()).collect(Collectors.toSet());
    }

    public Set<Player> getActiveParticipants() {
        return teams.stream().flatMap(team -> team.getActiveMembers().stream()).collect(Collectors.toSet());
    }

    public Set<String> getParticipantsUid() {
        return teams.stream().flatMap(team -> team.getMembersUid().stream()).collect(Collectors.toSet());
    }

    public List<Encounter> getAllEncounters() {
        return getStages().stream().flatMap(stage -> stage.getEncounters().stream()).collect(Collectors.toList());
    }

    public Optional<Season> optSeason() {
        return Optional.ofNullable(season);
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public Optional<TournamentClassifier> optClassifier() {
        return Optional.ofNullable(classifier);
    }

    public void setClassifier(TournamentClassifier classifier) {
        this.classifier = classifier;
    }
}
