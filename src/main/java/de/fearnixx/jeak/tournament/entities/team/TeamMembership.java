package de.fearnixx.jeak.tournament.entities.team;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

@Entity(name = "TeamMembership")
@Table(name = "TEAM_MEMBERSHIP")
public class TeamMembership implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TEAM_MEMBERSHIP_ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "TEAM_ID")
    private Team team;

    @JoinColumn(name = "PLAYER_ID")
    @ManyToOne
    private Player player;

    @Column(name = "JOIN_TSP")
    private ZonedDateTime joinTsp = ZonedDateTime.now();

    @Column(name = "ACTIVE")
    private boolean active;

    private TeamMembership() {
    }

    TeamMembership(Team team, Player player, boolean active) {
        this.team = team;
        this.player = player;
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public Team getTeam() {
        return team;
    }

    public Player getPlayer() {
        return player;
    }

    public ZonedDateTime getJoinTsp() {
        return joinTsp;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }
}
