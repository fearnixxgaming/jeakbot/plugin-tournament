package de.fearnixx.jeak.tournament.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity(name = "TournamentApiToken")
@Table(name = "TOURNAMENT_API_TOKEN")
public class TournamentApiToken implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TOKEN_ID")
    private Integer id;

    @Column(name = "CLIENT_UUID")
    private String clientUuid;

    @Column(name = "TOKEN")
    private String token = UUID.randomUUID().toString();

    @Column(name = "CREATION_TSP")
    private ZonedDateTime creationTsp = ZonedDateTime.now();

    private TournamentApiToken() {
    }

    public TournamentApiToken(String clientUuid) {
        this.clientUuid = clientUuid;
    }

    public Integer getId() {
        return id;
    }

    public String getClientUuid() {
        return clientUuid;
    }

    public String getToken() {
        return token;
    }

    public ZonedDateTime getCreationTsp() {
        return creationTsp;
    }
}