package de.fearnixx.jeak.tournament.controller.serialization;

import de.fearnixx.jeak.tournament.entities.encounter.Match;

import java.util.HashMap;
import java.util.Map;

public class MatchSerialization extends AbstractEntitySerializer<Match> {

    @Override
    public Map<String, Object> serialize(Match entity, String uuid) {
        Map<String, Object> matchSerialization = new HashMap<>();

        matchSerialization.put("id", entity.getId());
        matchSerialization.put("scores", entity.getMatchScoresByTeamName());

        return matchSerialization;
    }
}
