package de.fearnixx.jeak.tournament.controller.serialization;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.teamspeak.IUserService;
import de.fearnixx.jeak.teamspeak.data.IUser;
import de.fearnixx.jeak.tournament.entities.team.Player;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.management.services.TournamentEntityManagementService;

import java.util.Map;

public abstract class AbstractEntitySerializer<T> {

    @Inject
    private IUserService userService;

    @Inject
    protected TournamentEntityManagementService entityManagement;

    public abstract Map<String, Object> serialize(T entity, String uuid);

    protected String clientDisplay(String uuid) {
        return userService.findUserByUniqueID(uuid).stream().findFirst().map(IUser::getNickName).orElse(uuid);
    }

    protected String getTeamName(Team team) {
        String teamName = team.getName();
        if (team.isSinglePlayer()) {
            final Player player = team.getMembers().get(0);
            teamName = player.optName().orElse(player.optTsUid().map(this::clientDisplay).orElse(""));
        }

        return teamName;
    }
}
