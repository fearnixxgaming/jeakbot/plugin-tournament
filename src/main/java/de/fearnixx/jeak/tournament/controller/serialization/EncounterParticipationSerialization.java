package de.fearnixx.jeak.tournament.controller.serialization;

import de.fearnixx.jeak.tournament.entities.encounter.EncounterParticipation;
import de.fearnixx.jeak.tournament.entities.team.Team;

import java.util.HashMap;
import java.util.Map;

public class EncounterParticipationSerialization extends AbstractEntitySerializer<EncounterParticipation> {
    @Override
    public Map<String, Object> serialize(EncounterParticipation encounterParticipation, String uuid) {
        Map<String, Object> participationSerialization = new HashMap<>();

        Team team = encounterParticipation.getTeam();

        participationSerialization.put("id", encounterParticipation.getId());
        participationSerialization.put("teamId", team.getId());
        participationSerialization.put("team", getTeamName(team));
        participationSerialization.put("teamLogoUrl", team.getLogoUrl());
        participationSerialization.put("teamTag", team.getTag());

        final boolean isTeamLeader = team.isLeader(uuid);
        final boolean isTournamentLeader = encounterParticipation.getEncounter().getStage().getTournament().isLeader(uuid);

        participationSerialization.put("is-leader", isTeamLeader);
        participationSerialization.put("can-ready", (isTeamLeader || isTournamentLeader) && !encounterParticipation.isReady());

        participationSerialization.put("matchesWon", encounterParticipation.getMatchesWon());
        participationSerialization.put("roundsWon", encounterParticipation.getRoundsWon());
        participationSerialization.put("isEncounterWon", encounterParticipation.isEncounterWon());
        participationSerialization.put("isEncounterDrawn", encounterParticipation.isEncounterDrawn());

        return participationSerialization;
    }
}
