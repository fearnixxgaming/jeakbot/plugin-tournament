package de.fearnixx.jeak.tournament.controller.serialization;

import de.fearnixx.jeak.tournament.entities.Season;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class SeasonSerialization extends AbstractEntitySerializer<Season> {

    @Override
    public Map<String, Object> serialize(Season season, String uuid) {
        Map<String, Object> seasonSerialization = new HashMap<>();
        seasonSerialization.put("id", season.getId());
        seasonSerialization.put("name", season.getName());

        final ZonedDateTime start = season.getStart();
        seasonSerialization.put("start", start.format(DateTimeFormatter.ISO_ZONED_DATE_TIME));

        final ZonedDateTime end = season.getEnd();
        seasonSerialization.put("end", end.format(DateTimeFormatter.ISO_ZONED_DATE_TIME));

        final ZonedDateTime now = ZonedDateTime.now();
        seasonSerialization.put("is-running", now.isAfter(start) && now.isBefore(end));

        return seasonSerialization;
    }
}
