package de.fearnixx.jeak.tournament.controller;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.RequestMapping;
import de.fearnixx.jeak.reflect.RequestParam;
import de.fearnixx.jeak.reflect.RestController;
import de.fearnixx.jeak.service.controller.RequestMethod;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.data.IUser;
import de.fearnixx.jeak.tournament.config.TournamentConfigService;
import de.fearnixx.jeak.tournament.controller.responses.Response;
import de.fearnixx.jeak.tournament.controller.serialization.EncounterSerialization;
import de.fearnixx.jeak.tournament.entities.TournamentApiToken;
import de.fearnixx.jeak.tournament.entities.encounter.Encounter;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.management.EncounterManagement;

import java.util.*;
import java.util.stream.Collectors;

@RestController(pluginId = "tournament", endpoint = "/encounters")
public class EncounterController extends AbstractController {

    private static final String INVALID_TOKEN_MESSAGE = "Invalid token";

    private EncounterManagement encounterManagement;

    @Inject
    private TournamentConfigService configService;

    @Inject
    private IDataCache cache;

    @Inject
    private EncounterSerialization encounterSerialization;

    public EncounterController(EncounterManagement encounterManagement) {
        this.encounterManagement = encounterManagement;
    }

    private Response getResponseWithEncounter(Encounter encounter, String clientUuid) {
        final Response response = new Response();
        response.addDataProperty("encounter", encounterSerialization.serialize(encounter, clientUuid));
        return response;
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/prepare", isSecured = false)
    public Response prepareEncounter(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "encounterId") Integer encounterId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MESSAGE);
        }

        return wrapInTransaction(() -> {

                    final String clientUuid = clientToken.get().getClientUuid();
                    final Encounter encounter = getEncounter(encounterId);

                    final Map<String, List<String>> result = encounterManagement.prepareEncounter(
                            clientUuid,
                            encounter
                    );
                    checkErrors(result);

                    return getResponseWithEncounter(encounter, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/ready", isSecured = false)
    public Response readyEncounter(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "encounterId") Integer encounterId,
            @RequestParam(name = "teamId") Integer teamId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MESSAGE);
        }

        return wrapInTransaction(() -> {

                    final Encounter encounter = getEncounter(encounterId);
                    final String clientUuid = clientToken.get().getClientUuid();

                    final Map<String, List<String>> result = encounterManagement.readyEncounter(
                            clientUuid,
                            encounter,
                            getTeam(teamId)
                    );
                    checkErrors(result);

                    return getResponseWithEncounter(encounter, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/set-hoster", isSecured = false)
    public Response setHoster(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "encounterId") Integer encounterId,
            @RequestParam(name = "hoster") String hoster
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MESSAGE);
        }

        return wrapInTransaction(() -> {

                    final Encounter encounter = getEncounter(encounterId);
                    final String clientUuid = clientToken.get().getClientUuid();

                    final Map<String, List<String>> result = encounterManagement.setHoster(
                            clientUuid,
                            encounter,
                            hoster
                    );

                    checkErrors(result);

                    return getResponseWithEncounter(encounter, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/start", isSecured = false)
    public Response startEncounter(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "encounterId") Integer encounterId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MESSAGE);
        }

        return wrapInTransaction(() -> {

                    final Encounter encounter = getEncounter(encounterId);
                    final String clientUuid = clientToken.get().getClientUuid();

                    final Map<String, List<String>> result = encounterManagement.startEncounter(
                            clientUuid,
                            encounter
                    );
                    checkErrors(result);

                    return getResponseWithEncounter(encounter, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/add-score", isSecured = false)
    public Response addRoundScore(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "encounterId") Integer encounterId,
            @RequestParam(name = "scores") String scores
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MESSAGE);
        }

        return wrapInTransaction(() -> {
                    final Encounter encounter = getEncounter(encounterId);
                    final String clientUuid = clientToken.get().getClientUuid();

                    final Map<String, List<String>> result = encounterManagement.addMatch(
                            clientUuid,
                            encounter,
                            scores
                    );
                    checkErrors(result);

                    return getResponseWithEncounter(encounter, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/remove-match", isSecured = false)
    public Response removeMatch(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "encounterId") Integer encounterId,
            @RequestParam(name = "matchId") int matchId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MESSAGE);
        }

        return wrapInTransaction(() -> {
                    final Encounter encounter = getEncounter(encounterId);
                    final String clientUuid = clientToken.get().getClientUuid();

                    final Map<String, List<String>> result = encounterManagement.removeMatch(
                            clientUuid,
                            encounter,
                            matchId
                    );
                    checkErrors(result);

                    return getResponseWithEncounter(encounter, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/forfeit", isSecured = false)
    public Response forfeitEncounter(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "encounterId") Integer encounterId,
            @RequestParam(name = "teamId") Integer teamId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MESSAGE);
        }

        return wrapInTransaction(() -> {

                    final Encounter encounter = getEncounter(encounterId);
                    final String clientUuid = clientToken.get().getClientUuid();

                    final Map<String, List<String>> result = encounterManagement.forfeitEncounter(
                            clientUuid,
                            encounter,
                            getTeam(teamId)
                    );
                    checkErrors(result);

                    return getResponseWithEncounter(encounter, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/finish", isSecured = false)
    public Response finishEncounter(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "encounterId") Integer encounterId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MESSAGE);
        }

        return wrapInTransaction(() -> {

                    final Encounter encounter = getEncounter(encounterId);
                    final String clientUuid = clientToken.get().getClientUuid();

                    final Map<String, List<String>> result = encounterManagement.finishEncounter(
                            clientUuid,
                            encounter
                    );
                    checkErrors(result);

                    return getResponseWithEncounter(encounter, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/add-participant", isSecured = false)
    public Response addParticipant(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "encounterId") Integer encounterId,
            @RequestParam(name = "teamId") Integer teamId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MESSAGE);
        }

        return wrapInTransaction(() -> {

                    final Encounter encounter = getEncounter(encounterId);
                    final Team team = getTeam(teamId);
                    final String clientUuid = clientToken.get().getClientUuid();

                    final Map<String, List<String>> result = encounterManagement.addParticipant(
                            clientUuid,
                            encounter,
                            team
                    );
                    checkErrors(result);

                    return getResponseWithEncounter(encounter, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.GET, endpoint = "/hoster", isSecured = false)
    public Response getAllHoster(@RequestParam(name = "token") String token) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MESSAGE);
        }

        return wrapInTransaction(() -> {
            List<IClient> hoster =
                    cache.getClients().stream()
                            .filter(
                                    c -> c.getGroupIDs().contains(configService.getHosterServerGroup())
                            )
                            .sorted(Comparator.comparing(IUser::getNickName))
                            .collect(Collectors.toList());

            final List<Map<String, Object>> hosterList =
                    hoster.stream()
                            .map(
                                    c -> {
                                        Map<String, Object> ro = new HashMap<>();
                                        ro.put("uuid", c.getClientUniqueID());
                                        ro.put("nickname", c.getNickName());

                                        return ro;
                                    }
                            )
                            .collect(Collectors.toList());

            final Response response = new Response();
            response.addDataProperty("hoster", hosterList);
            return response;
        });
    }
}
