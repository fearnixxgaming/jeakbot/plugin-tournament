package de.fearnixx.jeak.tournament.controller.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;

public class Response {

    @JsonProperty
    private List<Error> errors;

    @JsonProperty
    private Map<String, Object> data;

    public Response() {
        this.errors = new ArrayList<>();
        this.data = new HashMap<>();
    }

    public Response(String... errors) {
        this.errors = new ArrayList<>();
        Arrays.stream(errors).map(Error::new).forEach(this.errors::add);

        this.data = new HashMap<>();
    }

    public void addError(Error error) {
        errors.add(error);
    }

    public void addDataProperty(String key, Object value) {
        if (data.containsKey(key)) {
            throw new IllegalStateException("The key " + key + " was already part of the response!");
        }

        data.put(key, value);
    }
}
