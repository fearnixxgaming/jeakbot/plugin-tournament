package de.fearnixx.jeak.tournament.controller.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidationError extends Error {

    @JsonProperty
    private final Map<String, List<String>> validations;

    private static final String VALIDATION_ERROR = "VALIDATION_ERROR";

    public ValidationError() {
        super(VALIDATION_ERROR);
        validations = new HashMap<>();
    }

    public void addValidation(String field, List<String> errors) {
        validations.put(field, errors);
    }

    public Map<String, List<String>> getValidations() {
        return validations;
    }
}
