package de.fearnixx.jeak.tournament.controller;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.RequestMapping;
import de.fearnixx.jeak.reflect.RequestParam;
import de.fearnixx.jeak.reflect.RestController;
import de.fearnixx.jeak.service.controller.RequestMethod;
import de.fearnixx.jeak.service.teamspeak.IUserService;
import de.fearnixx.jeak.teamspeak.data.IUser;
import de.fearnixx.jeak.tournament.commands.CommandPermissions;
import de.fearnixx.jeak.tournament.controller.responses.RequestException;
import de.fearnixx.jeak.tournament.controller.responses.Response;
import de.fearnixx.jeak.tournament.controller.serialization.TournamentSerialization;
import de.fearnixx.jeak.tournament.entities.Season;
import de.fearnixx.jeak.tournament.entities.TournamentApiToken;
import de.fearnixx.jeak.tournament.entities.tournament.Tournament;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentClassifier;
import de.fearnixx.jeak.tournament.management.TournamentManagement;
import de.fearnixx.jeak.tournament.management.services.TournamentEntityManagementService;

import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController(pluginId = "tournament", endpoint = "/tournaments")
public class TournamentController extends AbstractController {

    @Inject
    private TournamentEntityManagementService entityManagement;

    private final TournamentManagement tournamentManagement;

    @Inject
    private IUserService userService;

    @Inject
    private TournamentSerialization tournamentSerialization;

    public TournamentController(TournamentManagement tournamentManagement) {
        this.tournamentManagement = tournamentManagement;
    }

    private Response getResponseWithTournament(Tournament tournament, String uuid) {
        final Response response = new Response();
        response.addDataProperty("tournament", tournamentSerialization.serialize(tournament, uuid));

        boolean canClaimTournaments = false;

        if (!uuid.isBlank()) {
            final IUser user = userService.findUserByUniqueID(uuid).stream().findFirst().orElseThrow(
                    () -> new IllegalStateException("User for uuid: " + uuid + " not found!")
            );
            canClaimTournaments = !tournament.isLeader(uuid) && user.hasPermission(CommandPermissions.TEAM_CLAIM);
        }
        response.addDataProperty("can-claim-tournaments", canClaimTournaments);

        return response;
    }

    @RequestMapping(method = RequestMethod.GET, endpoint = "/list", isSecured = false)
    public Response getTournamentListWithInfo(@RequestParam(name = "token") String token) {
        String clientUuid = getClientUuidFromToken(token);

        return wrapInTransaction(() -> {
            TypedQuery<Tournament> tournamentQuery =
                    entityManagement.getEntityManager().createQuery(
                            "SELECT t FROM Tournament t order by t.id desc", Tournament.class
                    );

            List<Object> tournamentInfoList = new ArrayList<>();

            tournamentQuery.getResultList().forEach(
                    t -> tournamentInfoList.add(tournamentSerialization.serialize(t, clientUuid))
            );

            final Response response = new Response();
            response.addDataProperty("tournaments", tournamentInfoList);
            return response;
        });
    }

    @RequestMapping(method = RequestMethod.GET, endpoint = "/info", isSecured = false)
    public Response getTournamentInfo(@RequestParam(name = "token") String token,
                                      @RequestParam(name = "tournamentId") Integer tournamentId
    ) {
        String clientUuid = getClientUuidFromToken(token);

        return wrapInTransaction(() -> getResponseWithTournament(getTournament(tournamentId), clientUuid));
    }

    @RequestMapping(method = RequestMethod.PUT, endpoint = "/create", isSecured = false)
    public Response createTournament(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "name") String name
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {

            final String clientUuid = clientToken.get().getClientUuid();
            final Map<String, List<String>> result = tournamentManagement.createTournament(
                    clientUuid, name
            );

            checkErrors(result);
            entityManagement.getEntityManager().getTransaction().commit();
            entityManagement.beginTransaction();

            return getResponseWithTournament(entityManagement.getTournamentByName(name).orElseThrow(), clientUuid);
        });
    }

    @RequestMapping(method = RequestMethod.DELETE, endpoint = "/delete", isSecured = false)
    public Response deleteTournament(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
            final Map<String, List<String>> result = tournamentManagement.deleteTournament(
                    clientToken.get().getClientUuid(), getTournament(tournamentId)
            );

            checkErrors(result);
            return new Response();
        });
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/join", isSecured = false)
    public Response joinTournament(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId,
            @RequestParam(name = "teamId") Integer teamId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
            final String clientUuid = clientToken.get().getClientUuid();
            final Tournament tournament = getTournament(tournamentId);

            final Map<String, List<String>> result = tournamentManagement.joinTournament(
                    clientUuid, tournament, getTeam(teamId)
            );

            checkErrors(result);
            return getResponseWithTournament(tournament, clientUuid);
        });
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/ready", isSecured = false)
    public Response readyTournament(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId,
            @RequestParam(name = "teamId") Integer teamId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
            final String clientUuid = clientToken.get().getClientUuid();
            final Tournament tournament = getTournament(tournamentId);

            final Map<String, List<String>> result = tournamentManagement.readyTournament(
                    clientUuid, tournament, getTeam(teamId)
            );

            checkErrors(result);
            return getResponseWithTournament(tournament, clientUuid);
        });
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/start", isSecured = false)
    public Response startTournament(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
            final String clientUuid = clientToken.get().getClientUuid();
            final Tournament tournament = getTournament(tournamentId);
            final Map<String, List<String>> result = tournamentManagement.startTournament(clientUuid, tournament);

            checkErrors(result);
            return getResponseWithTournament(tournament, clientUuid);
        });
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/param", isSecured = false)
    public Response paramTournament(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId,
            @RequestParam(name = "key") String key,
            @RequestParam(name = "value") String value

    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
            final String clientUuid = clientToken.get().getClientUuid();
            final Tournament tournament = getTournament(tournamentId);
            final Map<String, List<String>> result =
                    tournamentManagement.paramTournament(clientUuid, tournament, key, value);

            checkErrors(result);
            return getResponseWithTournament(tournament, clientUuid);
        });
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/finish", isSecured = false)
    public Response finishTournament(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
            final String clientUuid = clientToken.get().getClientUuid();
            final Tournament tournament = getTournament(tournamentId);
            final Map<String, List<String>> result = tournamentManagement.finishTournament(clientUuid, tournament);

            checkErrors(result);
            return getResponseWithTournament(tournament, clientUuid);
        });
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/add-team", isSecured = false)
    public Response addTeam(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId,
            @RequestParam(name = "teamId") Integer teamId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
            final String clientUuid = clientToken.get().getClientUuid();
            final Tournament tournament = getTournament(tournamentId);

            final Map<String, List<String>> result = tournamentManagement.addTeam(
                    clientUuid, tournament, getTeam(teamId)
            );

            checkErrors(result);
            return getResponseWithTournament(tournament, clientUuid);
        });
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/add-single-player-team", isSecured = false)
    public Response addSinglePlayerTeam(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId,
            @RequestParam(name = "playerUuid") String playerUuid
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
            final String clientUuid = clientToken.get().getClientUuid();
            final Tournament tournament = getTournament(tournamentId);

            final Map<String, List<String>> result = tournamentManagement.addSinglePlayerTeam(
                    clientUuid, tournament, playerUuid
            );

            checkErrors(result);
            return getResponseWithTournament(tournament, clientUuid);
        });
    }


    @RequestMapping(method = RequestMethod.POST, endpoint = "/remove-team", isSecured = false)
    public Response removeTeam(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId,
            @RequestParam(name = "teamId") Integer teamId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
            final String clientUuid = clientToken.get().getClientUuid();
            final Tournament tournament = getTournament(tournamentId);

            final Map<String, List<String>> result = tournamentManagement.removeTeam(
                    clientUuid, tournament, getTeam(teamId)
            );

            checkErrors(result);
            return getResponseWithTournament(tournament, clientUuid);
        });
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/leader-token", isSecured = false)
    public Response leaderToken(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId,
            @RequestParam(name = "leader-token") String leaderToken
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        if (leaderToken == null) {
            return new Response("leader-token is necessary");
        }

        return wrapInTransaction(() -> {

                    final String clientUuid = clientToken.get().getClientUuid();
                    final Tournament tournament = getTournament(tournamentId);
                    final Map<String, List<String>> result = tournamentManagement.leaderTournament(
                            clientUuid, tournament, leaderToken
                    );

                    checkErrors(result);
                    return getResponseWithTournament(tournament, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/generate-leader-token", isSecured = false)
    public Response generateLeaderToken(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {

                    final String clientUuid = clientToken.get().getClientUuid();
                    final Tournament tournament = getTournament(tournamentId);
                    final Map<String, List<String>> result = tournamentManagement.leaderTournament(
                            clientUuid, tournament, null
                    );

                    checkErrors(result);
                    return getResponseWithTournament(tournament, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/remove-leader", isSecured = false)
    public Response removeLeader(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId,
            @RequestParam(name = "leaderUuid") String leaderUuid
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {

                    final String clientUuid = clientToken.get().getClientUuid();
                    final Tournament tournament = getTournament(tournamentId);
                    final Map<String, List<String>> result = tournamentManagement.leaderRemove(
                            clientUuid, tournament, leaderUuid
                    );

                    checkErrors(result);
                    return getResponseWithTournament(tournament, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/re-roll-stage", isSecured = false)
    public Response reRollCurrentStage(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {

                    final String clientUuid = clientToken.get().getClientUuid();
                    final Tournament tournament = getTournament(tournamentId);
                    final Map<String, List<String>> result = tournamentManagement.reRollCurrentStage(
                            clientUuid, tournament
                    );

                    checkErrors(result);
                    return getResponseWithTournament(tournament, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/start-stage", isSecured = false)
    public Response startCurrentStage(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {

                    final String clientUuid = clientToken.get().getClientUuid();
                    final Tournament tournament = getTournament(tournamentId);
                    final Map<String, List<String>> result = tournamentManagement.startCurrentStage(
                            clientUuid, tournament
                    );

                    checkErrors(result);
                    return getResponseWithTournament(tournament, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/claim-leadership", isSecured = false)
    public Response claimLeadership(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
                    final String clientUuid = clientToken.get().getClientUuid();
                    final Tournament tournament = getTournament(tournamentId);
                    final Map<String, List<String>> result = tournamentManagement.claimTournament(clientUuid, tournament);

                    checkErrors(result);

                    return getResponseWithTournament(tournament, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/disqualify", isSecured = false)
    public Response disqualifyTeam(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId,
            @RequestParam(name = "teamId") Integer teamId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
            final Tournament tournament = getTournament(tournamentId);
            final String uuid = clientToken.get().getClientUuid();
            final Map<String, List<String>> result = tournamentManagement.disqualifyTeam(
                    uuid, tournament, getTeam(teamId)
            );

            checkErrors(result);
            return getResponseWithTournament(tournament, uuid);
        });
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/forfeit", isSecured = false)
    public Response forfeitForTeam(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId,
            @RequestParam(name = "teamId") Integer teamId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
            final String uuid = clientToken.get().getClientUuid();
            final Tournament tournament = getTournament(tournamentId);

            final Map<String, List<String>> result = tournamentManagement.forfeitForTeam(
                    uuid, tournament, getTeam(teamId)
            );

            checkErrors(result);
            return getResponseWithTournament(tournament, uuid);
        });
    }

    @RequestMapping(method = RequestMethod.GET, endpoint = "/list-classifier", isSecured = false)
    public Response listClassifiers(
            @RequestParam(name = "token") String token
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(this::getClassifiersResponse);
    }

    private Response getClassifiersResponse() {
        final Response response = new Response();
        response.addDataProperty(
                "classifiers",
                entityManagement.getEntityManager()
                        .createQuery("SELECT tc FROM TournamentClassifier tc", TournamentClassifier.class)
                        .getResultList().stream()
                        .map(TournamentClassifier::getClassifier)
                        .collect(Collectors.toList())
        );
        return response;
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/create-classifier", isSecured = false)
    public Response createClassifier(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "classifier") String classifier,
            @RequestParam(name = "description") String description
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
            final Map<String, List<String>> result = tournamentManagement.createClassifier(classifier, description);

            checkErrors(result);
            return getClassifiersResponse();
        });
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/set-classifier", isSecured = false)
    public Response setClassifier(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId,
            @RequestParam(name = "classifier") String classifier
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
            final String uuid = clientToken.get().getClientUuid();
            final Tournament tournament = getTournament(tournamentId);

            final Map<String, List<String>> result = tournamentManagement.setClassifier(uuid, tournament, classifier);

            checkErrors(result);
            return getResponseWithTournament(tournament, uuid);
        });
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/set-season", isSecured = false)
    public Response setSeason(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "tournamentId") Integer tournamentId,
            @RequestParam(name = "seasonId") Integer seasonId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
            final String uuid = clientToken.get().getClientUuid();
            final Tournament tournament = getTournament(tournamentId);
            final Season season = entityManagement.getEntityManager().find(Season.class, seasonId);

            if (season == null) {
                throw new RequestException(Map.of("seasonId", List.of("The season does not exists!")));
            }

            final Map<String, List<String>> result = tournamentManagement.setSeason(uuid, tournament, season);

            checkErrors(result);
            return getResponseWithTournament(tournament, uuid);
        });
    }
}
