package de.fearnixx.jeak.tournament.controller.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Error {

    @JsonProperty
    private String errorCode;

    public Error(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
