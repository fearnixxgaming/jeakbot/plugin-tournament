package de.fearnixx.jeak.tournament.controller.serialization;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.Tournament;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentClassifier;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentLeadershipToken;
import de.fearnixx.jeak.tournament.enums.ParticipationState;

import java.util.*;
import java.util.stream.Collectors;

public class TournamentSerialization extends AbstractEntitySerializer<Tournament> {

    @Inject
    private StageSerialization stageSerialization;

    @Inject
    private SeasonSerialization seasonSerialization;

    @Override
    public Map<String, Object> serialize(Tournament tournament, String uuid) {
        Map<String, Object> tournamentSerialization = new HashMap<>();

        tournamentSerialization.put("id", tournament.getId());
        tournamentSerialization.put("name", tournament.getName());
        tournamentSerialization.put("creator", clientDisplay(tournament.getCreatorUuid()));

        boolean isCreator = tournament.getCreatorUuid().equals(uuid);
        tournamentSerialization.put("is-creator", isCreator);

        Set<String> tournamentLeaders = tournament.getLeaders();
        final boolean isLeader = tournamentLeaders.contains(uuid);
        tournamentSerialization.put("is-leader", isLeader);

        tournamentSerialization.put("state", tournament.getState());
        tournamentSerialization.put(
                "leaders",
                tournamentLeaders.stream()
                        .map(this::clientDisplay)
                        .collect(Collectors.toList())
        );

        tournament.optClassifier().map(TournamentClassifier::getClassifier).ifPresent(
                classifier -> tournamentSerialization.put("classifier", classifier)
        );

        tournament.optSeason().ifPresent(
                season -> tournamentSerialization.put("season", seasonSerialization.serialize(season, uuid))
        );

        if (isLeader || isCreator) {
            tournamentSerialization.put("leaderToken",
                    tournament.getTournamentLeadershipTokens()
                            .stream()
                            .map(TournamentLeadershipToken::getToken).collect(Collectors.toList()));
        }

        tournamentSerialization.put("parameters", tournament.getParameters());

        tournamentSerialization.put("stages",
                tournament.getStages().stream().map(stage -> stageSerialization.serialize(stage, uuid)).collect(Collectors.toList())
        );

        final List<Object> teams = tournament.getTournamentParticipations()
                .stream()
                .sorted(Comparator.comparing(ep -> ep.getTeam().getName()))
                .map(
                        tp -> {
                            Map<String, Object> teamParticipationInfo = new HashMap<>();
                            final Team team = tp.getTeam();
                            teamParticipationInfo.put("id", team.getId());
                            teamParticipationInfo.put("name", getTeamName(team));
                            teamParticipationInfo.put("tag", team.getTag());

                            final boolean isReady = tp.getState().equals(ParticipationState.READY);

                            teamParticipationInfo.put("state", tp.getState());
                            teamParticipationInfo.put("ready", isReady);
                            teamParticipationInfo.put("can-ready", !isReady &&
                                    (tournamentLeaders.contains(uuid) || team.isLeader(uuid))
                                    && tp.getState() == ParticipationState.JOINED
                            );

                            return teamParticipationInfo;
                        }
                )
                .collect(Collectors.toList());


        tournamentSerialization.put("teams", teams);

        return tournamentSerialization;
    }
}
