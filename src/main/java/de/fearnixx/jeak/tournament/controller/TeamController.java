package de.fearnixx.jeak.tournament.controller;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.RequestMapping;
import de.fearnixx.jeak.reflect.RequestParam;
import de.fearnixx.jeak.reflect.RestController;
import de.fearnixx.jeak.service.controller.RequestMethod;
import de.fearnixx.jeak.service.teamspeak.IUserService;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.data.IUser;
import de.fearnixx.jeak.tournament.commands.CommandPermissions;
import de.fearnixx.jeak.tournament.controller.responses.Response;
import de.fearnixx.jeak.tournament.controller.serialization.TeamSerialization;
import de.fearnixx.jeak.tournament.entities.TournamentApiToken;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.management.TeamManagement;
import de.fearnixx.jeak.tournament.management.services.TournamentEntityManagementService;

import javax.persistence.TypedQuery;
import java.util.*;
import java.util.stream.Collectors;

@RestController(pluginId = "tournament", endpoint = "/teams")
public class TeamController extends AbstractController {

    private static final String INVALID_TOKEN_MSG = "invalid token";

    @Inject
    private TournamentEntityManagementService entityManagement;

    @Inject
    private TeamSerialization teamSerialization;

    @Inject
    private IDataCache cache;

    @Inject
    private IUserService userService;

    private final TeamManagement teamManagement;

    public TeamController(TeamManagement teamManagement) {
        this.teamManagement = teamManagement;
    }

    private Response getResponseWithTeam(Team team, String uuid) {
        final Response response = new Response();
        response.addDataProperty("team", teamSerialization.serialize(team, uuid));

        boolean addTeamMemberPerm = false;
        boolean canClaimTeams = false;

        if (!uuid.isBlank()) {
            final IUser user = userService.findUserByUniqueID(uuid).stream().findFirst().orElseThrow(
                    () -> new IllegalStateException("User for uuid: " + uuid + " not found!")
            );
            addTeamMemberPerm = user.hasPermission(CommandPermissions.TEAM_MEMBER_ADD);
            canClaimTeams = !team.isLeader(uuid) && user.hasPermission(CommandPermissions.TEAM_CLAIM);
        }

        response.addDataProperty("can-add-team-member", addTeamMemberPerm);
        response.addDataProperty("can-claim-teams", canClaimTeams);

        return response;
    }

    @RequestMapping(method = RequestMethod.GET, endpoint = "/list", isSecured = false)
    public Response getTeamListWithInfo(@RequestParam(name = "token") String token) {
        String clientUuid = getClientUuidFromToken(token);

        return wrapInTransaction(() -> {
            TypedQuery<Team> teamQuery =
                    entityManagement.getEntityManager().createQuery(
                            "SELECT t FROM Team t order by t.name ASC", Team.class
                    );

            List<Object> teamInfoList = new ArrayList<>();

            teamQuery.getResultList().forEach(
                    t -> teamInfoList.add(teamSerialization.serialize(t, clientUuid))
            );

            final Response response = new Response();
            response.addDataProperty("teams", teamInfoList);

            return response;
        });
    }

    @RequestMapping(method = RequestMethod.GET, endpoint = "/info", isSecured = false)
    public Response getTeamInfo(@RequestParam(name = "token") String token,
                                @RequestParam(name = "teamId") Integer teamId) {
        String clientUuid = getClientUuidFromToken(token);

        return wrapInTransaction(() -> getResponseWithTeam(getTeam(teamId), clientUuid));
    }

    @RequestMapping(method = RequestMethod.PUT, endpoint = "/create", isSecured = false)
    public Response createTeam(@RequestParam(name = "token") String token, @RequestParam(name = "name") String name) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MSG);
        }

        return wrapInTransaction(() -> {

                    final String clientUuid = clientToken.get().getClientUuid();
                    final Map<String, List<String>> result = teamManagement.createTeam(
                            clientUuid, name
                    );

                    checkErrors(result);
                    entityManagement.getEntityManager().getTransaction().commit();
                    entityManagement.beginTransaction();

                    return getResponseWithTeam(entityManagement.getTeamByName(name).orElseThrow(), clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/join", isSecured = false)
    public Response joinTeam(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "teamId") Integer teamId,
            @RequestParam(name = "joinToken") String joinToken
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MSG);
        }

        return wrapInTransaction(() -> {

                    final Team team = getTeam(teamId);
                    final String clientUuid = clientToken.get().getClientUuid();

                    final Map<String, List<String>> result = teamManagement.joinTeam(clientUuid, team, joinToken);

                    checkErrors(result);

                    return getResponseWithTeam(team, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/leave", isSecured = false)
    public Response leaveTeam(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "teamId") Integer teamId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MSG);
        }

        return wrapInTransaction(() -> {
                    final String clientUuid = clientToken.get().getClientUuid();
                    final Team team = getTeam(teamId);

                    final Map<String, List<String>> result = teamManagement.leaveTeam(clientUuid, team);

                    checkErrors(result);
                    return getResponseWithTeam(team, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/add-member", isSecured = false)
    public Response addMember(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "teamId") Integer teamId,
            @RequestParam(name = "playerUuid") String playerUuid,
            @RequestParam(name = "playerName") String playerName
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MSG);
        }

        return wrapInTransaction(() -> {
                    final String clientUuid = clientToken.get().getClientUuid();
                    final Team team = getTeam(teamId);
                    final Map<String, List<String>> result = teamManagement.addTeamMember(
                            clientUuid,
                            team,
                            nullIfEmpty(playerUuid),
                            nullIfEmpty(playerName)
                    );

                    checkErrors(result);
                    return getResponseWithTeam(team, clientUuid);
                }
        );
    }

    private String nullIfEmpty(String str) {
        return str == null || str.isEmpty() ? null : str;
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/remove-member", isSecured = false)
    public Response removeMember(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "teamId") Integer teamId,
            @RequestParam(name = "playerId") int playerId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MSG);
        }

        return wrapInTransaction(() -> {
                    final String clientUuid = clientToken.get().getClientUuid();
                    final Team team = getTeam(teamId);

                    final Map<String, List<String>> result = teamManagement.removeTeamMember(
                            clientUuid, team, getPlayer(playerId)
                    );

                    checkErrors(result);
                    return getResponseWithTeam(team, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.DELETE, endpoint = "/delete", isSecured = false)
    public Response deleteTeam(@RequestParam(name = "token") String token, @RequestParam(name = "teamId") Integer teamId) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MSG);
        }

        return wrapInTransaction(() -> {
                    final String clientUuid = clientToken.get().getClientUuid();
                    final Team team = getTeam(teamId);
                    final Map<String, List<String>> result = teamManagement.deleteTeam(clientUuid, team);

                    checkErrors(result);
                    return new Response();
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/change-join-token", isSecured = false)
    public Response changeJoinToken(@RequestParam(name = "token") String token,
                                    @RequestParam(name = "teamId") Integer teamId,
                                    @RequestParam(name = "newJoinToken") String newJoinToken) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MSG);
        }

        return wrapInTransaction(() -> {
                    final String clientUuid = clientToken.get().getClientUuid();
                    final Team team = getTeam(teamId);
                    final Map<String, List<String>> result = teamManagement.token(clientUuid, team, newJoinToken);

                    checkErrors(result);
                    return getResponseWithTeam(team, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/leader-token", isSecured = false)
    public Response useLeaderToken(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "teamId") Integer teamId,
            @RequestParam(name = "leader-token") String leaderToken
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MSG);
        }

        return wrapInTransaction(() -> {
                    final String clientUuid = clientToken.get().getClientUuid();
                    final Team team = getTeam(teamId);
                    final Map<String, List<String>> result = teamManagement.leaderToken(clientUuid, team, leaderToken);

                    checkErrors(result);

                    return getResponseWithTeam(team, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/generate-leader-token", isSecured = false)
    public Response generateLeaderToken(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "teamId") Integer teamId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MSG);
        }

        return wrapInTransaction(() -> {
                    final String clientUuid = clientToken.get().getClientUuid();
                    final Team team = getTeam(teamId);
                    final Map<String, List<String>> result = teamManagement.leaderToken(clientUuid, team, null);

                    checkErrors(result);

                    return getResponseWithTeam(team, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/remove-leader", isSecured = false)
    public Response removeLeader(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "teamId") Integer teamId,
            @RequestParam(name = "leaderUuid") String leaderUuid
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
                    final String clientUuid = clientToken.get().getClientUuid();
                    final Team team = getTeam(teamId);
                    final Map<String, List<String>> result = teamManagement.leaderRemove(
                            clientUuid, team, leaderUuid
                    );

                    checkErrors(result);
                    return getResponseWithTeam(team, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/claim-leadership", isSecured = false)
    public Response claimLeadership(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "teamId") Integer teamId
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MSG);
        }

        return wrapInTransaction(() -> {
                    final String clientUuid = clientToken.get().getClientUuid();
                    final Team team = getTeam(teamId);
                    final Map<String, List<String>> result = teamManagement.claimTeam(clientUuid, team);

                    checkErrors(result);

                    return getResponseWithTeam(team, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/set-player-info", isSecured = false)
    public Response setPlayerInfo(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "teamId") Integer teamId,
            @RequestParam(name = "playerId") int playerId,
            @RequestParam(name = "playerUuid") String playerUuid,
            @RequestParam(name = "playerName") String playerName,
            @RequestParam(name = "playerActive") Boolean playerActive
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MSG);
        }

        return wrapInTransaction(() -> {
                    final String clientUuid = clientToken.get().getClientUuid();
                    final Team team = getTeam(teamId);

                    final Map<String, List<String>> result =
                            teamManagement.setPlayerInfo(
                                    clientUuid, team, getPlayer(playerId),
                                    nullIfEmpty(playerUuid), nullIfEmpty(playerName), playerActive
                            );

                    checkErrors(result);

                    return getResponseWithTeam(team, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/set-description", isSecured = false)
    public Response setDescription(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "teamId") Integer teamId,
            @RequestParam(name = "description") String description
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MSG);
        }

        return wrapInTransaction(() -> {
                    final String clientUuid = clientToken.get().getClientUuid();
                    final Team team = getTeam(teamId);
                    final Map<String, List<String>> result = teamManagement.setTeamDescription(clientUuid, team, description);

                    checkErrors(result);
                    return getResponseWithTeam(team, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/set-logo-url", isSecured = false)
    public Response setLogoUrl(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "teamId") Integer teamId,
            @RequestParam(name = "logoUrl") String logoUrl
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MSG);
        }

        return wrapInTransaction(() -> {
                    final String clientUuid = clientToken.get().getClientUuid();
                    final Team team = getTeam(teamId);
                    final Map<String, List<String>> result = teamManagement.setTeamLogoUrl(clientUuid, team, logoUrl);

                    checkErrors(result);
                    return getResponseWithTeam(team, clientUuid);
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/set-tag", isSecured = false)
    public Response setTag(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "teamId") Integer teamId,
            @RequestParam(name = "tag") String tag
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MSG);
        }

        return wrapInTransaction(() -> {
                    final String clientUuid = clientToken.get().getClientUuid();
                    final Team team = getTeam(teamId);
                    final Map<String, List<String>> result = teamManagement.setTag(clientUuid, team, tag);

                    checkErrors(result);
                    return getResponseWithTeam(team, clientUuid);
                }
        );
    }

    //FIXME: Move to framework or other controller

    /**
     * @deprecated Since this endpoints is a useful tool for many applications it should not be located here
     */
    @Deprecated(forRemoval = true)
    @RequestMapping(method = RequestMethod.GET, endpoint = "/clients", isSecured = false)
    public Response getClients(@RequestParam(name = "token") String token) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN_MSG);
        }

        return wrapInTransaction(() -> {
            List<IClient> clients = new ArrayList<>(cache.getClients());
            clients.sort(Comparator.comparing(IUser::getNickName));

            final List<Map<String, Object>> clientList = clients.stream()
                    .map(
                            c -> {
                                Map<String, Object> ro = new HashMap<>();
                                ro.put("uuid", c.getClientUniqueID());
                                ro.put("nickname", c.getNickName());

                                return ro;
                            }
                    )
                    .collect(Collectors.toList());

            final Response response = new Response();
            response.addDataProperty("clients", clientList);
            return response;
        });
    }
}
