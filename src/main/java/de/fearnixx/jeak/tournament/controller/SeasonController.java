package de.fearnixx.jeak.tournament.controller;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.RequestMapping;
import de.fearnixx.jeak.reflect.RequestParam;
import de.fearnixx.jeak.reflect.RestController;
import de.fearnixx.jeak.service.controller.RequestMethod;
import de.fearnixx.jeak.tournament.controller.responses.Response;
import de.fearnixx.jeak.tournament.controller.serialization.SeasonSerialization;
import de.fearnixx.jeak.tournament.entities.Season;
import de.fearnixx.jeak.tournament.entities.TournamentApiToken;
import de.fearnixx.jeak.tournament.entities.encounter.EncounterParticipation;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.Tournament;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentParticipation;
import de.fearnixx.jeak.tournament.management.TournamentManagement;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

@RestController(pluginId = "tournament", endpoint = "/season")
public class SeasonController extends AbstractController {

    private static final String TOURNAMENTS_PARAM = "tournaments";

    @Inject
    private SeasonSerialization seasonSerialization;

    private TournamentManagement tournamentManagement;

    public SeasonController(TournamentManagement tournamentManagement) {
        this.tournamentManagement = tournamentManagement;
    }

    @RequestMapping(method = RequestMethod.GET, endpoint = "/list", isSecured = false)
    public Response getSeasons() {
        return wrapInTransaction(this::getSeasonsResponse);
    }

    private Response getSeasonsResponse() {
        final Response response = new Response();
        response.addDataProperty(
                "seasons",
                getEntityManager()
                        .createQuery("SELECT s FROM Season s", Season.class)
                        .getResultList().stream()
                        .map(s -> seasonSerialization.serialize(s, UUID_NOT_REQUIRED))
                        .collect(Collectors.toList())
        );
        return response;
    }

    @RequestMapping(method = RequestMethod.GET, endpoint = "/info", isSecured = false)
    public Response getSeasonInformation(@RequestParam(name = "seasonId") Integer seasonId) {

        return wrapInTransaction(() -> {
                    final Response response = new Response();
                    EntityManager entityManager = getEntityManager();

                    final List<Tournament> tournaments =
                            Optional.ofNullable(entityManager.find(Season.class, seasonId))
                                    .map(season ->
                                            entityManager.createQuery("SELECT t FROM Tournament t WHERE season = :season", Tournament.class)
                                                    .setParameter("season", season)
                                                    .getResultList())
                                    .orElse(new ArrayList<>());

                    final List<Team> teams = entityManager.createQuery(
                            "SELECT DISTINCT tp.team FROM TournamentParticipation tp WHERE tp.tournament IN (:tournaments)", Team.class
                    ).setParameter(TOURNAMENTS_PARAM, tournaments).getResultList();

                    final TypedQuery<TournamentParticipation> tournamentParticipationQuery = entityManager.createQuery(
                            "SELECT tp FROM TournamentParticipation tp WHERE team = :team AND tp.tournament IN (:tournaments)", TournamentParticipation.class
                    );

                    final TypedQuery<EncounterParticipation> encounterWonParticipationQuery = entityManager.createQuery(
                            "SELECT ep FROM EncounterParticipation ep WHERE team = :team AND ep.encounter.stage.tournament IN (:tournaments) AND encounterResult = 1", EncounterParticipation.class
                    );

                    final TypedQuery<TournamentParticipation> tournamentsWonParticipationQuery = entityManager.createQuery(
                            "SELECT tp FROM TournamentParticipation tp WHERE team = :team AND tp.tournament IN (:tournaments) AND state = 'WON'", TournamentParticipation.class
                    );

                    Map<String, Integer> seasonScores = new HashMap<>();

                    for (Team team : teams) {
                        int score = team.getAdditionalSeasonScore();

                        score += tournamentParticipationQuery.setParameter("team", team).setParameter(TOURNAMENTS_PARAM, tournaments).getResultList().size();
                        score += encounterWonParticipationQuery.setParameter("team", team).setParameter(TOURNAMENTS_PARAM, tournaments).getResultList().size() * 2;
                        score += tournamentsWonParticipationQuery.setParameter("team", team).setParameter(TOURNAMENTS_PARAM, tournaments).getResultList().size();

                        seasonScores.put(team.getName(), score);
                    }

                    List<Map.Entry<String, Integer>> list = new ArrayList<>(seasonScores.entrySet());
                    list.sort(Map.Entry.comparingByValue());
                    Collections.reverse(list);

                    Map<String, Integer> leaderboard = new LinkedHashMap<>();
                    for (Map.Entry<String, Integer> entry : list) {
                        leaderboard.put(entry.getKey(), entry.getValue());
                    }

                    response.addDataProperty(
                            "tournaments",
                            tournaments.stream().map(t -> {
                                Map<String, Object> tournamentInfo = new HashMap<>();
                                tournamentInfo.put("id", t.getId());
                                tournamentInfo.put("name", t.getName());
                                return tournamentInfo;
                            }).collect(Collectors.toList())
                    );
                    response.addDataProperty("leaderboard", leaderboard);

                    return response;
                }
        );
    }

    @RequestMapping(method = RequestMethod.POST, endpoint = "/create-season", isSecured = false)
    public Response createSeason(
            @RequestParam(name = "token") String token,
            @RequestParam(name = "name") String name,
            @RequestParam(name = "start") String start,
            @RequestParam(name = "end") String end
    ) {
        Optional<TournamentApiToken> clientToken = getToken(token);

        if (clientToken.isEmpty()) {
            return new Response(INVALID_TOKEN);
        }

        return wrapInTransaction(() -> {
            final Map<String, List<String>> result = tournamentManagement.createSeason(
                    name,
                    ZonedDateTime.parse(start),
                    ZonedDateTime.parse(end)
            );

            checkErrors(result);

            return getSeasonsResponse();
        });
    }

}
