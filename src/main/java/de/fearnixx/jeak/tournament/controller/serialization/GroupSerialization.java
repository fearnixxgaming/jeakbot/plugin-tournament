package de.fearnixx.jeak.tournament.controller.serialization;

import de.fearnixx.jeak.tournament.entities.encounter.Encounter;
import de.fearnixx.jeak.tournament.entities.stage.Group;
import de.fearnixx.jeak.tournament.entities.stage.GroupMembership;
import de.fearnixx.jeak.tournament.entities.stage.Stage;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentParticipation;
import de.fearnixx.jeak.tournament.enums.ParticipationState;

import java.util.*;
import java.util.stream.Collectors;

public class GroupSerialization extends AbstractEntitySerializer<Group> {

    @Override
    public Map<String, Object> serialize(Group group, String uuid) {
        Map<String, Object> groupSerialization = new HashMap<>();

        groupSerialization.put("id", group.getId());
        groupSerialization.put("name", group.getName());

        List<Map<String, Object>> memberships = new ArrayList<>();

        final List<GroupMembership> membershipsSorted = group.getMembershipsSorted();

        membershipsSorted.forEach(
                gm -> {
                    Map<String, Object> groupMembership = new HashMap<>();

                    groupMembership.put("id", gm.getId());
                    groupMembership.put("team", getTeamName(gm.getTeam()));
                    groupMembership.put("points", gm.getPoints());
                    groupMembership.put("encountersWon", gm.getEncountersWon());
                    groupMembership.put("matchesWon", gm.getMatchesWon());
                    groupMembership.put("roundsWon", gm.getRoundsWon());
                    groupMembership.put("isGroupWon", gm.isGroupWon());

                    final TournamentParticipation tp = gm.getTournamentParticipation();
                    final Optional<Stage> optLostStage = tp.optLostStage();

                    groupMembership.put(
                            "wasForfeited",
                            (tp.getState() == ParticipationState.FORFEITED || tp.getState() == ParticipationState.DISQUALIFIED)
                                    && optLostStage.orElseThrow().equals(gm.getGroup().getStage())
                    );

                    memberships.add(groupMembership);
                }
        );

        groupSerialization.put("memberships", memberships);

        List<Team> teams = membershipsSorted.stream().map(GroupMembership::getTeam).collect(Collectors.toList());
        groupSerialization.put(
                "encounterIds",
                group.getStage().getEncounters().stream()
                        .filter(e -> !e.getTeams().isEmpty() && teams.contains(e.getTeams().get(0)))
                        .map(Encounter::getId)
                        .collect(Collectors.toList()));

        return groupSerialization;
    }
}
