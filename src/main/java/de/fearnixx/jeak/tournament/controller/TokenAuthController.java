package de.fearnixx.jeak.tournament.controller;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.RequestMapping;
import de.fearnixx.jeak.reflect.RequestParam;
import de.fearnixx.jeak.reflect.RestController;
import de.fearnixx.jeak.service.controller.RequestMethod;
import de.fearnixx.jeak.service.permission.base.IPermission;
import de.fearnixx.jeak.service.teamspeak.IUserService;
import de.fearnixx.jeak.teamspeak.data.IUser;
import de.fearnixx.jeak.tournament.commands.CommandPermissions;
import de.fearnixx.jeak.tournament.controller.responses.Error;
import de.fearnixx.jeak.tournament.controller.responses.Response;
import de.fearnixx.jeak.tournament.management.ManagementPermissions;
import de.fearnixx.jeak.tournament.management.services.TournamentEntityManagementService;

import java.util.Optional;

@RestController(pluginId = "tournament", endpoint = "/token")
public class TokenAuthController extends AbstractController {

    @Inject
    private IUserService userService;

    @Inject
    private TournamentEntityManagementService entityManagement;

    @RequestMapping(method = RequestMethod.GET, endpoint = "/authenticate", isSecured = false)
    public Response authenticate(@RequestParam(name = "token") String token) {
        final Response response = new Response();

        if (isValidToken(token)) {
            response.addDataProperty("authenticated", true);
        } else {
            response.addError(new Error("TOKEN INVALID"));
        }

        return response;
    }

    @RequestMapping(method = RequestMethod.GET, endpoint = "/permissions", isSecured = false)
    public Response permissions(@RequestParam(name = "token") String token) {
        String clientUuid = getClientUuidFromToken(token);

        return wrapInTransaction(() -> {
            final Response response = new Response();

            boolean canCreateTeam = false;
            boolean canCreateTournament = false;
            boolean canAddTeamMember = false;
            boolean canClaimTeams = false;
            boolean canClaimTournaments = false;

            if (!clientUuid.isBlank()) {
                final IUser user = userService.findUserByUniqueID(clientUuid).stream().findFirst().orElseThrow(
                        () -> new IllegalStateException("User for uuid: " + clientUuid + " not found!")
                );

                final Optional<IPermission> maxTeamPermission = user.getPermission(ManagementPermissions.TOURNAMENT_CREATE_MAX_TEAMS);

                if (maxTeamPermission.isPresent()) {
                    int createdTeamsOfUser = entityManagement.getCreatedTeamsByUser(clientUuid);

                    if (createdTeamsOfUser < maxTeamPermission.get().getValue()) {
                        canCreateTeam = true;
                    }
                }

                canCreateTournament = user.hasPermission(CommandPermissions.TOURNAMENT_CREATE);
                canAddTeamMember = user.hasPermission(CommandPermissions.TEAM_MEMBER_ADD);
                canClaimTeams = user.hasPermission(CommandPermissions.TEAM_CLAIM);
                canClaimTournaments = user.hasPermission(CommandPermissions.TOURNAMENT_CLAIM);
            }

            response.addDataProperty("can-create-team", canCreateTeam);
            response.addDataProperty("can-create-tournament", canCreateTournament);
            response.addDataProperty("can-add-team-member", canAddTeamMember);
            response.addDataProperty("can-claim-teams", canClaimTeams);
            response.addDataProperty("can-claim-tournaments", canClaimTournaments);

            return response;
        });
    }
}
