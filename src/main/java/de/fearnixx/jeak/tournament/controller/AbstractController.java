package de.fearnixx.jeak.tournament.controller;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.tournament.controller.responses.Error;
import de.fearnixx.jeak.tournament.controller.responses.RequestException;
import de.fearnixx.jeak.tournament.controller.responses.Response;
import de.fearnixx.jeak.tournament.controller.responses.ValidationError;
import de.fearnixx.jeak.tournament.entities.TournamentApiToken;
import de.fearnixx.jeak.tournament.entities.encounter.Encounter;
import de.fearnixx.jeak.tournament.entities.team.Player;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.Tournament;
import de.fearnixx.jeak.tournament.management.services.TournamentEntityManagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

public class AbstractController {

    protected Logger logger = LoggerFactory.getLogger(AbstractController.class);

    protected boolean isValidToken(String token) {
        return getToken(token).isPresent();
    }

    protected static final String INVALID_TOKEN = "invalid token";
    protected static final String UUID_NOT_REQUIRED = "";

    private static final Map<String, List<String>> TOURNAMENT_NOT_PRESENT_ERROR = Map.of("TOURNAMENT_NOT_PRESENT",
            Collections.singletonList("The tournament does not exist!"));
    private static final Map<String, List<String>> ENCOUNTER_NOT_PRESENT_ERROR = Map.of("ENCOUNTER_NOT_PRESENT",
            Collections.singletonList("The encounter does not exist!"));
    private static final Map<String, List<String>> TEAM_NOT_PRESENT_ERROR = Map.of("TEAM_NOT_PRESENT",
            Collections.singletonList("The team does not exist!"));
    private static final Map<String, List<String>> PLAYER_NOT_PRESENT_ERROR = Map.of("PLAYER_NOT_PRESENT",
            Collections.singletonList("The player does not exist!"));

    protected static final Map<String, List<String>> INDEX_NOT_NUMERICAL_ERROR = Map.of("INDEX_NOT_NUMERICAL",
            Collections.singletonList("The given index is not numerical!"));

    @Inject
    private TournamentEntityManagementService entityManagement;

    private TypedQuery<TournamentApiToken> tokenQuery;

    public void init() {
        tokenQuery = entityManagement.getEntityManager().createQuery(
                "SELECT t FROM TournamentApiToken t WHERE token = :token",
                TournamentApiToken.class
        );
    }

    protected Optional<TournamentApiToken> getToken(String token) {
        synchronized (entityManagement.getEntityManager()) {

            final EntityManager entityManager = entityManagement.getEntityManager();
            try {
                entityManager.getTransaction().begin();

                tokenQuery.setParameter("token", token);
                List<TournamentApiToken> resultList = tokenQuery.getResultList();

                Optional<TournamentApiToken> out;

                if (resultList.size() == 1) {
                    out = Optional.of(resultList.get(0));
                } else {
                    logger.debug("Token {} not found.", token);
                    out = Optional.empty();
                }

                entityManager.flush();
                entityManager.getTransaction().commit();

                return out;

            } catch (Exception e) {
                logger.error("An exception occurred while trying to validate the token!", e);
                entityManager.getTransaction().rollback();

                return Optional.empty();
            }

        }
    }

    protected Response wrapInTransaction(Supplier<Response> responseSupplier) {
        return wrapInTransaction(
                responseSupplier,
                e -> {
                    logger.warn("Exception during request", e);
                    final Response response = new Response();

                    response.addError(new Error("UNEXPECTED EXCEPTION: " + e.getMessage()));

                    return response;
                }
        );
    }

    private Response wrapInTransaction(Supplier<Response> responseSupplier, Function<Exception, Response> onError) {
        synchronized (entityManagement.getEntityManager()) {

            final EntityManager entityManager = entityManagement.getEntityManager();
            entityManager.getTransaction().begin();

            try {
                Response out = responseSupplier.get();
                entityManager.flush();
                entityManager.getTransaction().commit();
                return out;

            } catch (Exception e) {
                entityManager.getTransaction().rollback();

                if (e instanceof RequestException) {
                    final Response response = new Response();

                    ((RequestException) e).getErrors().forEach(
                            (k, v) -> {
                                Error error = null;

                                if (k.startsWith("field:")) {
                                    ValidationError ve = new ValidationError();
                                    ve.addValidation(k.split(":")[1], v);
                                    error = ve;
                                } else {
                                    if (!v.isEmpty()) {
                                        error = new Error(v.get(0));
                                    }
                                }

                                if (error != null) {
                                    response.addError(error);
                                }
                            }
                    );

                    return response;
                }

                return onError.apply(e);
            }
        }
    }

    protected EntityManager getEntityManager() {
        return entityManagement.getEntityManager();
    }

    protected Encounter getEncounter(int encounterId) {
        return Optional.ofNullable(entityManagement.getEntityManager().find(Encounter.class, encounterId))
                .orElseThrow(() -> new RequestException(ENCOUNTER_NOT_PRESENT_ERROR));
    }

    protected Tournament getTournament(int tournamentId) {
        return Optional.ofNullable(entityManagement.getEntityManager().find(Tournament.class, tournamentId)).orElseThrow(
                () -> new RequestException(TOURNAMENT_NOT_PRESENT_ERROR));
    }

    protected Team getTeam(int teamId) {
        return Optional.ofNullable(entityManagement.getEntityManager().find(Team.class, teamId)).orElseThrow(
                () -> new RequestException(TEAM_NOT_PRESENT_ERROR));
    }

    protected Player getPlayer(int playerId) {
        return Optional.ofNullable(entityManagement.getEntityManager().find(Player.class, playerId)).orElseThrow(
                () -> new RequestException(PLAYER_NOT_PRESENT_ERROR));
    }

    protected void checkErrors(Map<String, List<String>> result) {
        if (!result.isEmpty()) {
            throw new RequestException(result);
        }
    }

    protected String getClientUuidFromToken(String token) {
        return Optional.ofNullable(token)
                .map(this::getToken)
                .map(opt -> opt.map(TournamentApiToken::getClientUuid).orElse("")).orElse("");
    }
}
