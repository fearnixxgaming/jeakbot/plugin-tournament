package de.fearnixx.jeak.tournament.controller.responses;

import java.util.List;
import java.util.Map;

public class RequestException extends RuntimeException {

    private final Map<String, List<String>> errors;

    public RequestException(Map<String, List<String>> errors) {
        this.errors = errors;
    }

    public Map<String, List<String>> getErrors() {
        return errors;
    }
}
