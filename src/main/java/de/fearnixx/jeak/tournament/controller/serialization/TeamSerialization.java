package de.fearnixx.jeak.tournament.controller.serialization;

import de.fearnixx.jeak.tournament.entities.team.Player;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.team.TeamLeadershipToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TeamSerialization extends AbstractEntitySerializer<Team> {

    @Override
    public Map<String, Object> serialize(Team team, String uuid) {
        HashMap<String, Object> teamSerialization = new HashMap<>();

        teamSerialization.put("id", team.getId());
        teamSerialization.put("name", getTeamName(team));
        teamSerialization.put("tag", team.getTag());

        teamSerialization.put("single-player", team.isSinglePlayer());
        teamSerialization.put("description", team.getDescription());
        teamSerialization.put("logo-url", team.getLogoUrl());

        String creatorUuid = team.getCreatorUuid();
        teamSerialization.put("creator", clientDisplay(creatorUuid));

        final boolean isCreator = creatorUuid.equals(uuid);
        teamSerialization.put("is-creator", isCreator);

        final List<String> teamLeaders = team.getLeaders();
        teamSerialization.put(
                "leaders",
                teamLeaders.stream().map(
                        teamLeaderUid -> {
                            final Map<String, Object> playerInfo = new HashMap<>();
                            playerInfo.put("uuid", teamLeaderUid);
                            playerInfo.put("display", clientDisplay(teamLeaderUid));
                            return playerInfo;
                        }
                ).collect(Collectors.toList())
        );

        final boolean isLeader = teamLeaders.contains(uuid);
        teamSerialization.put("is-leader", isLeader);

        final boolean isMember = team.optTeamMembershipByTsUid(uuid).isPresent();
        teamSerialization.put("is-member", isMember);

        teamSerialization.put("has-active-tournaments", entityManagement.checkActiveTournaments(team));

        if (isLeader || isCreator) {
            teamSerialization.put("joinToken", team.getJoinToken());
            final List<TeamLeadershipToken> teamLeadershipTokens = team.getTeamLeadershipTokens();

            teamSerialization.put("leaderToken",
                    teamLeadershipTokens.stream().map(TeamLeadershipToken::getToken).collect(Collectors.toList())
            );
        }

        teamSerialization.put(
                "members",
                team.getMemberships().stream()
                        .map(
                                ms -> {
                                    final Player player = ms.getPlayer();
                                    final Map<String, Object> playerInfo = new HashMap<>();

                                    playerInfo.put("id", player.getId());

                                    player.optTsUid().ifPresent(
                                            tsUid -> {
                                                playerInfo.put("uuid", tsUid);
                                                playerInfo.put("display", clientDisplay(tsUid));
                                            }
                                    );

                                    player.optName().ifPresent(name -> playerInfo.put("playerName", name));
                                    playerInfo.put("active", ms.isActive());
                                    playerInfo.put("is-users-player", player.optTsUid().map(tsUid -> tsUid.equals(uuid)).orElse(false));

                                    return playerInfo;
                                }
                        )
                        .collect(Collectors.toList())

        );

        return teamSerialization;
    }
}
