package de.fearnixx.jeak.tournament.controller.serialization;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.teamspeak.IUserService;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.tournament.entities.encounter.Encounter;
import de.fearnixx.jeak.tournament.enums.EncounterState;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EncounterSerialization extends AbstractEntitySerializer<Encounter> {

    @Inject
    private IUserService userService;

    @Inject
    private MatchSerialization matchSerialization;

    @Inject
    private EncounterParticipationSerialization encounterParticipationSerialization;

    @Override
    public Map<String, Object> serialize(Encounter encounter, String uuid) {
        Map<String, Object> encounterSerialization = new HashMap<>();

        encounterSerialization.put("id", encounter.getId());
        encounterSerialization.put("state", encounter.getState());

        String hoster = encounter.getHoster();

        if (hoster != null) {
            final List<IClient> matchingClient = userService.findClientByUniqueID(hoster);
            if (!matchingClient.isEmpty()) {
                hoster = matchingClient.get(0).getNickName();
            }
            encounterSerialization.put("hoster", hoster);
        }

        List<Map<String, Object>> encounterParticipationSerializations = encounter.getEncounterParticipations().stream()
                .map(ep -> encounterParticipationSerialization.serialize(ep, uuid))
                .collect(Collectors.toList());
        encounterSerialization.put("participations", encounterParticipationSerializations);

        encounterSerialization.put(
                "blocked",
                encounter.getStage().getEncounters().stream()
                        .filter(e -> !e.equals(encounter))
                        .filter(e -> e.getTeams().stream().anyMatch(encounter.getTeams()::contains))
                        .anyMatch(e -> e.getState() == EncounterState.STARTED
                                || e.getState() == EncounterState.READY
                                || e.getState() == EncounterState.PREPARING)
        );

        encounterSerialization.put(
                "matches",
                encounter.getMatches().stream()
                        .map(match -> matchSerialization.serialize(match, uuid))
                        .collect(Collectors.toList())
        );

        return encounterSerialization;
    }
}
