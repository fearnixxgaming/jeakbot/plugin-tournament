package de.fearnixx.jeak.tournament.controller.serialization;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.tournament.entities.encounter.Encounter;
import de.fearnixx.jeak.tournament.entities.stage.Stage;
import de.fearnixx.jeak.tournament.entities.stage.StageFreePass;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.enums.StageType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StageSerialization extends AbstractEntitySerializer<Stage> {

    @Inject
    private EncounterSerialization encounterSerialization;

    @Inject
    private GroupSerialization groupSerialization;

    @Override
    public Map<String, Object> serialize(Stage stage, String uuid) {

        Map<String, Object> stageSerialization = new HashMap<>();

        stageSerialization.put("id", stage.getId());
        stageSerialization.put("state", stage.getState());

        stageSerialization.put("isCurrentStage", stage.getTournament().getCurrentStage().equals(stage));

        final List<Encounter> encounters = stage.getEncounters();

        List<Object> encounterSerializations = encounters.stream()
                .map(e -> encounterSerialization.serialize(e, uuid))
                .collect(Collectors.toList());

        stageSerialization.put("encounters", encounterSerializations);

        final StageType type = stage.getType();
        stageSerialization.put("type", type.toString());

        if (type.isGroup()) {
            stageSerialization.put(
                    "groups",
                    stage.getGroups().stream().map(group -> groupSerialization.serialize(group, uuid))
                            .collect(Collectors.toList())
            );
        }

        stageSerialization.put(
                "freepasses",
                stage.getFreePasses().stream().map(StageFreePass::getTeam).map(Team::getName).collect(Collectors.toList())
        );

        return stageSerialization;
    }
}
