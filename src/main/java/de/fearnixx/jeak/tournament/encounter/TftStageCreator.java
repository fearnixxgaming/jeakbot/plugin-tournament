package de.fearnixx.jeak.tournament.encounter;

import de.fearnixx.jeak.tournament.entities.encounter.Encounter;
import de.fearnixx.jeak.tournament.entities.stage.Group;
import de.fearnixx.jeak.tournament.entities.stage.Stage;

import java.util.List;

public class TftStageCreator extends GroupStageCreator {

    public TftStageCreator(List<Integer> params) {
        super(params);
    }

    @Override
    protected void generateStageFromGroups(Stage stage) {
        for (Group group : stage.getGroups()) {
            final Encounter encounter = stage.createEncounter();
            encounter.createEncounterParticipations(group.getTeams());
        }
    }
}
