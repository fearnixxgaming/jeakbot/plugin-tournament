package de.fearnixx.jeak.tournament.encounter;

import de.fearnixx.jeak.tournament.entities.encounter.Encounter;
import de.fearnixx.jeak.tournament.entities.stage.Stage;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentParameter;
import de.fearnixx.jeak.tournament.enums.StageType;

import java.util.*;
import java.util.stream.Collectors;

public class KoStageCreator extends AbstractStageCreator {

    private int teamsInOneEncounter;
    private boolean shouldGenerateGameForThirdPlace = false;

    public KoStageCreator(List<Integer> params) {
        super(params);
        teamsInOneEncounter = params.get(0);

        if (params.size() == 2 && params.get(1) > 0) {
            shouldGenerateGameForThirdPlace = true;
        }
    }

    @Override
    public void createStage(List<Team> participatingTeams, Stage stage) {
        Collections.shuffle(participatingTeams);
        boolean generateParticipations = stage.getTournament().optParameter(TournamentParameter.SUPPRESS_PART_GEN).isEmpty();

        for (int i = 0; i < participatingTeams.size() / teamsInOneEncounter; i++) {
            List<Team> teamsInThisMatch = new ArrayList<>();
            for (int j = 0; j < teamsInOneEncounter; j++) {
                teamsInThisMatch.add(participatingTeams.get(i * teamsInOneEncounter + j));
            }
            Encounter encounter = stage.createEncounter();

            if (generateParticipations) {
                encounter.createEncounterParticipations(teamsInThisMatch);
            }
        }

        if (shouldGenerateGameForThirdPlace && participatingTeams.size() == 2) {
            Stage prevStage = stage.getTournament().getCurrentStage();

            if (stage.equals(prevStage)) {
                final ArrayList<Stage> allStages = new ArrayList<>(stage.getTournament().getStages());
                allStages.sort(Comparator.comparingInt(Stage::getId).reversed());

                if (allStages.size() < 2) {
                    return;
                }

                prevStage = allStages.get(1);
            }

            if (prevStage == null || prevStage.getType() != StageType.KO || !prevStage.getFreePasses().isEmpty()) {
                return;
            }

            final Set<Team> teamsOfPreviousStage = prevStage.getEncounters().stream()
                    .flatMap(e -> e.getTeams().stream())
                    .collect(Collectors.toSet());
            teamsOfPreviousStage.removeAll(participatingTeams);

            if (teamsOfPreviousStage.size() != 2) {
                return;
            }

            final Encounter gameForThirdPlace = stage.createEncounter();

            if (generateParticipations) {
                gameForThirdPlace.createEncounterParticipations(new ArrayList<>(teamsOfPreviousStage));
            }
        }
    }

    @Override
    protected boolean validateParams(List<Integer> params) {
        return (params.size() == 1 || params.size() == 2) && params.get(0) > 1;
    }
}
