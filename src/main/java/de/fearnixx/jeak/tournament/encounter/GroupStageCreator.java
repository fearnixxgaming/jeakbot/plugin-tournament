package de.fearnixx.jeak.tournament.encounter;

import de.fearnixx.jeak.tournament.entities.encounter.Encounter;
import de.fearnixx.jeak.tournament.entities.stage.Group;
import de.fearnixx.jeak.tournament.entities.stage.Stage;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.TournamentParameter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GroupStageCreator extends AbstractStageCreator {

    private int winningTeamsPerGroup;
    private int winningTeamsPerEncounter;
    private int groupCount;
    private int teamsPerGroup = 0;

    public GroupStageCreator(List<Integer> params) {
        super(params);

        winningTeamsPerGroup = params.get(0);
        winningTeamsPerEncounter = params.get(1);
        groupCount = params.get(2);

        if (params.size() == 4) {
            teamsPerGroup = params.get(3);
            groupCount = 0;
        }
    }

    @Override
    public void createStage(List<Team> participatingTeams, Stage stage) {
        Collections.shuffle(participatingTeams);

        if (groupCount > 0) {
            generateGroupsByGroupCount(participatingTeams, stage);
        } else if (teamsPerGroup > 0) {
            generateGroupsByTeamsPerGroup(participatingTeams, stage);
        }

        stage.setWinningTeamsPerEncounter(winningTeamsPerEncounter);
        generateStageFromGroups(stage);
    }

    private void generateGroupsByGroupCount(List<Team> participatingTeams, Stage stage) {

        if (participatingTeams.size() < groupCount) {
            return;
        }

        List<Group> groups = new ArrayList<>();

        for (int i = 0; i < groupCount; i++) {
            groups.add(stage.createGroup(winningTeamsPerGroup));
        }

        int groupIndex = 0;

        for (int i = 0; i < participatingTeams.size(); i++) {
            if (i % groupCount == 0) {
                groupIndex = 0;
            }

            groups.get(groupIndex).createMembership(participatingTeams.get(i));
            groupIndex++;
        }
    }

    private void generateGroupsByTeamsPerGroup(List<Team> participatingTeams, Stage stage) {
        if (participatingTeams.size() < teamsPerGroup) {
            return;
        }

        List<Group> groups = new ArrayList<>();

        int groupIndex = -1;

        for (int i = 0; i < participatingTeams.size(); i++) {
            if (i % teamsPerGroup == 0) {
                groups.add(stage.createGroup(winningTeamsPerGroup));
                groupIndex++;
            }

            groups.get(groupIndex).createMembership(participatingTeams.get(i));
        }
    }

    protected void generateStageFromGroups(Stage stage) {
        final List<Group> groups = stage.getGroups();
        boolean generateParticipations = stage.getTournament().optParameter(TournamentParameter.SUPPRESS_PART_GEN).isEmpty();

        for (Group group : groups) {
            List<Team> teamsOfGroup = group.getTeams();

            for (int i = 0; i < teamsOfGroup.size(); i++) {
                final Team teamA = teamsOfGroup.get(i);

                for (int j = i + 1; j < teamsOfGroup.size(); j++) {
                    final Team teamB = teamsOfGroup.get(j);

                    final Encounter encounter = stage.createEncounter();
                    if (generateParticipations) {
                        encounter.createEncounterParticipations(teamA, teamB);
                    }
                }
            }
        }
    }

    @Override
    protected boolean validateParams(List<Integer> params) {
        return (params.size() == 3 && params.get(2) > 0) ||
                (params.size() == 4 && params.get(2) == 0 && params.get(3) > 0) && params.get(1) == 1;
    }
}
