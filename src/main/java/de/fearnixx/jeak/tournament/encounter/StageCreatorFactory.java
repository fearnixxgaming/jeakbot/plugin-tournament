package de.fearnixx.jeak.tournament.encounter;

import de.fearnixx.jeak.tournament.enums.StageType;

import java.util.List;

public class StageCreatorFactory {

    private StageCreatorFactory() {
    }

    private static AbstractStageCreator getEncounterFactory(StageType tournamentType, List<Integer> params) {
        switch (tournamentType) {
            case KO:
                return new KoStageCreator(params);
            case GR:
                return new GroupStageCreator(params);
            case TFT:
                return new TftStageCreator(params);
            default:
                throw new IllegalArgumentException("StageType not known!");
        }
    }

    public static AbstractStageCreator getEncounterFactory(String tournamentType, List<Integer> params) {
        return getEncounterFactory(StageType.valueOf(tournamentType), params);
    }

}
