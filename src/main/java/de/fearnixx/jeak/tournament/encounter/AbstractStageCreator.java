package de.fearnixx.jeak.tournament.encounter;

import de.fearnixx.jeak.tournament.entities.stage.Stage;
import de.fearnixx.jeak.tournament.entities.team.Team;

import java.util.List;

public abstract class AbstractStageCreator {

    public AbstractStageCreator(List<Integer> params) {
        if (!validateParams(params)) {
            throw new IllegalArgumentException("The parameters are not valid for this encounter generation!");
        }
    }

    public abstract void createStage(List<Team> participatingTeams, Stage stage);

    protected abstract boolean validateParams(List<Integer> params);
}
