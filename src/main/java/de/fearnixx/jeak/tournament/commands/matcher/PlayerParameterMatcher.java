package de.fearnixx.jeak.tournament.commands.matcher;

import de.fearnixx.jeak.service.command.ICommandExecutionContext;
import de.fearnixx.jeak.service.command.spec.matcher.BasicMatcherResponse;
import de.fearnixx.jeak.service.command.spec.matcher.IMatcherResponse;
import de.fearnixx.jeak.service.command.spec.matcher.IMatchingContext;
import de.fearnixx.jeak.service.command.spec.matcher.MatcherResponseType;
import de.fearnixx.jeak.tournament.commands.CommandParameters;
import de.fearnixx.jeak.tournament.entities.team.Player;

import java.util.Optional;

public class PlayerParameterMatcher extends AbstractTournamentEntityMatcher<Player> {

    @Override
    protected IMatcherResponse doParse(ICommandExecutionContext ctx, IMatchingContext matchingContext, String extracted) {
        final Integer playerId;

        try {
            playerId = Integer.valueOf(extracted);
        } catch (NumberFormatException e) {
            return new BasicMatcherResponse(
                    MatcherResponseType.ERROR,
                    ctx.getParameterIndex().get(),
                    "The player id must be numerical!"
            );
        }

        final Optional<Player> player = entityManagement.getPlayerById(playerId);

        if (player.isEmpty()) {
            return new BasicMatcherResponse(
                    MatcherResponseType.ERROR,
                    ctx.getParameterIndex().get(),
                    "The player does not exist!"
            );
        }

        ctx.putOrReplaceOne(CommandParameters.PLAYER, player.get());

        ctx.getParameterIndex().incrementAndGet();
        return BasicMatcherResponse.SUCCESS;
    }

    @Override
    public Class<Player> getSupportedType() {
        return Player.class;
    }
}
