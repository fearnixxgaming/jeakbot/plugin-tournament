package de.fearnixx.jeak.tournament.commands.matcher;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.LocaleUnit;
import de.fearnixx.jeak.service.command.ICommandExecutionContext;
import de.fearnixx.jeak.service.command.spec.matcher.*;
import de.fearnixx.jeak.service.locale.ILocalizationUnit;
import de.fearnixx.jeak.tournament.management.services.TournamentEntityManagementService;

public abstract class AbstractTournamentEntityMatcher<T> extends AbstractTypeMatcher<T> {

    @Inject
    protected TournamentEntityManagementService entityManagement;

    @Inject
    @LocaleUnit("default")
    private ILocalizationUnit defaultLocaleUnit;

    @Override
    protected ILocalizationUnit getLocaleUnit() {
        return defaultLocaleUnit;
    }

    @Override
    protected IMatcherResponse parse(ICommandExecutionContext ctx, IMatchingContext matchingContext, String extracted) {
        entityManagement.beginTransaction();
        try {
            final IMatcherResponse response = doParse(ctx, matchingContext, extracted);
            entityManagement.getEntityManager().getTransaction().commit();
            return response;
        } catch (Exception e) {
            entityManagement.rollbackTransaction();
            return new BasicMatcherResponse(
                    MatcherResponseType.ERROR,
                    ctx.getParameterIndex().get(),
                    "An exception occurred while parsing the parameter!\n" + e.getMessage()
            );
        }

    }

    /**
     * Wraps the implementation of parsing the parameter in a transaction. The wrapping includes opening and
     * committing (therefor closing) the transaction.
     */
    protected abstract IMatcherResponse doParse(ICommandExecutionContext ctx, IMatchingContext matchingContext, String extracted);
}
