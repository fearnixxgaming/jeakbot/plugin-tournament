package de.fearnixx.jeak.tournament.commands.matcher;

import de.fearnixx.jeak.service.command.ICommandExecutionContext;
import de.fearnixx.jeak.service.command.spec.matcher.BasicMatcherResponse;
import de.fearnixx.jeak.service.command.spec.matcher.IMatcherResponse;
import de.fearnixx.jeak.service.command.spec.matcher.IMatchingContext;
import de.fearnixx.jeak.service.command.spec.matcher.MatcherResponseType;
import de.fearnixx.jeak.tournament.commands.CommandParameters;
import de.fearnixx.jeak.tournament.entities.Season;

import java.util.Optional;

public class SeasonParameterMatcher extends AbstractTournamentEntityMatcher<Season> {

    @Override
    protected IMatcherResponse doParse(ICommandExecutionContext ctx, IMatchingContext matchingContext, String extracted) {
        final Optional<Season> season = entityManagement.getSeasonByName(extracted);

        if (season.isEmpty()) {
            return new BasicMatcherResponse(
                    MatcherResponseType.ERROR,
                    ctx.getParameterIndex().get(),
                    "The season '" + extracted + "' does not exist!"
            );
        }

        ctx.putOrReplaceOne(CommandParameters.SEASON, season.get());

        ctx.getParameterIndex().incrementAndGet();
        return BasicMatcherResponse.SUCCESS;
    }

    @Override
    public Class<Season> getSupportedType() {
        return Season.class;
    }
}
