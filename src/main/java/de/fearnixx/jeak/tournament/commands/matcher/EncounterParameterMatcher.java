package de.fearnixx.jeak.tournament.commands.matcher;

import de.fearnixx.jeak.service.command.ICommandExecutionContext;
import de.fearnixx.jeak.service.command.spec.matcher.BasicMatcherResponse;
import de.fearnixx.jeak.service.command.spec.matcher.IMatcherResponse;
import de.fearnixx.jeak.service.command.spec.matcher.IMatchingContext;
import de.fearnixx.jeak.service.command.spec.matcher.MatcherResponseType;
import de.fearnixx.jeak.tournament.commands.CommandParameters;
import de.fearnixx.jeak.tournament.entities.encounter.Encounter;

import java.util.Optional;

public class EncounterParameterMatcher extends AbstractTournamentEntityMatcher<Encounter> {

    @Override
    protected IMatcherResponse doParse(ICommandExecutionContext ctx, IMatchingContext matchingContext, String extracted) {
        final Integer encounterId;

        try {
            encounterId = Integer.valueOf(extracted);
        } catch (NumberFormatException e) {
            return new BasicMatcherResponse(
                    MatcherResponseType.ERROR,
                    ctx.getParameterIndex().get(),
                    "The encounter id must be numerical!"
            );
        }

        final Optional<Encounter> encounter = entityManagement.getEncounterById(encounterId);

        if (encounter.isEmpty()) {
            return new BasicMatcherResponse(
                    MatcherResponseType.ERROR,
                    ctx.getParameterIndex().get(),
                    "The encounter does not exist!"
            );
        }

        ctx.putOrReplaceOne(CommandParameters.ENCOUNTER, encounter.get());

        ctx.getParameterIndex().incrementAndGet();
        return BasicMatcherResponse.SUCCESS;
    }

    @Override
    public Class<Encounter> getSupportedType() {
        return Encounter.class;
    }
}
