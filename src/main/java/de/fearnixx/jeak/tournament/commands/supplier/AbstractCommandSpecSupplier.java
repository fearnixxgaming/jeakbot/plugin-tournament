package de.fearnixx.jeak.tournament.commands.supplier;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.command.CommandException;
import de.fearnixx.jeak.service.command.spec.ICommandSpec;
import de.fearnixx.jeak.tournament.management.services.TournamentEntityManagementService;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public abstract class AbstractCommandSpecSupplier {

    @Inject
    private TournamentEntityManagementService entityManagement;

    public abstract List<ICommandSpec> getAllCommandSpecs();

    protected final void executeInTransaction(Supplier<Map<String, List<String>>> businessCode) throws CommandException {
        synchronized (entityManagement.getEntityManager()) {
            try {
                final Map<String, List<String>> result = businessCode.get();

                if (!result.isEmpty()) {
                    entityManagement.rollbackTransaction();

                    StringBuilder messageBuilder = new StringBuilder("Failed to execute command. Errors:" + System.lineSeparator());

                    result.forEach((k, v) ->
                            messageBuilder
                                    .append(k)
                                    .append(": ")
                                    .append(System.lineSeparator())
                                    .append(v.stream().collect(Collectors.joining(System.lineSeparator())))
                                    .append(System.lineSeparator())
                    );

                    throw new CommandException(messageBuilder.toString());
                }
            } catch (Exception e) {
                entityManagement.rollbackTransaction();
                throw new CommandException(e.getMessage(), e);
            }
        }
    }
}
