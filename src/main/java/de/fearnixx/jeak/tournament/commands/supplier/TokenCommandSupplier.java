package de.fearnixx.jeak.tournament.commands.supplier;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.command.ICommandExecutionContext;
import de.fearnixx.jeak.service.command.spec.Commands;
import de.fearnixx.jeak.service.command.spec.ICommandSpec;
import de.fearnixx.jeak.teamspeak.IServer;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.tournament.commands.CommandPermissions;
import de.fearnixx.jeak.tournament.config.TournamentConfigService;
import de.fearnixx.jeak.tournament.entities.TournamentApiToken;
import de.fearnixx.jeak.tournament.management.services.TournamentEntityManagementService;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class TokenCommandSupplier extends AbstractCommandSpecSupplier {

    @Inject
    private IServer server;

    @Inject
    private TournamentEntityManagementService entityManagement;

    @Inject
    private TournamentConfigService configService;

    private void execute(ICommandExecutionContext ctx) {
        IClient client = ctx.getSender();
        String uuid = client.getClientUniqueID();

        final boolean transactionWasActive = entityManagement.beginTransaction();

        final EntityManager entityManager = entityManagement.getEntityManager();

        TypedQuery<TournamentApiToken> existingTokens = entityManager.createQuery(
                "SELECT t FROM TournamentApiToken t WHERE clientUuid = :uuid",
                TournamentApiToken.class
        );
        existingTokens.setParameter("uuid", uuid);

        if (!existingTokens.getResultList().isEmpty()) {
            TournamentApiToken existingToken = existingTokens.getResultList().get(0);
            entityManager.remove(existingToken);
        }

        entityManager.flush();

        final TournamentApiToken newToken = new TournamentApiToken(uuid);
        entityManager.persist(newToken);

        if (!transactionWasActive) {
            entityManager.getTransaction().commit();
        }

        server.getConnection().sendRequest(
                client.sendMessage(configService.getTokenMessage().replaceAll("%\\[token]", newToken.getToken()))
        );
    }

    @Override
    public List<ICommandSpec> getAllCommandSpecs() {
        return Collections.singletonList(
                Commands.commandSpec("tournament-token")
                        .executor(
                                ctx -> executeInTransaction(() -> {
                                            this.execute(ctx);
                                            return new HashMap<>();
                                        }
                                )
                        )
                        .permission(CommandPermissions.GENERATE_API_TOKEN)
                        .build()
        );
    }
}
