package de.fearnixx.jeak.tournament.commands;

public abstract class CommandParameters {

    private CommandParameters() {
    }

    public static final String TEAM_NAME = "teamName";
    public static final String TEAM = "team";
    public static final String TEAM_TAG = "teamTag";

    public static final String TOURNAMENT_NAME = "tournamentName";
    public static final String TOURNAMENT = "tournament";
    public static final String PARAM_KEY = "paramKey";
    public static final String PARAM_VALUE = "paramValue";

    public static final String ENCOUNTER = "encounter";

    public static final String MESSAGE = "message";
    public static final String TOKEN = "token";
    public static final String HOSTER = "hoster";
    public static final String SCORE = "score";
    public static final String CLASSIFIER = "classifier";
    public static final String DESCRIPTION = "description";

    public static final String LEADER_UUID_TO_REMOVE = "leaderUuidToRemove";
    public static final String PLAYER = "player";
    public static final String PLAYER_UID = "playerUid";
    public static final String PLAYER_NAME = "playerName";
    public static final String PLAYER_ACTIVE = "playerActive";

    public static final String SEASON = "season";
    public static final String SEASON_NAME = "seasonName";
    public static final String SEASON_START_DATE = "seasonStartDate";
    public static final String SEASON_END_DATE = "seasonEndDate";
}
