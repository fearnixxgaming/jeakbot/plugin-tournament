package de.fearnixx.jeak.tournament.commands.matcher;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.LocaleUnit;
import de.fearnixx.jeak.service.command.ICommandExecutionContext;
import de.fearnixx.jeak.service.command.spec.matcher.*;
import de.fearnixx.jeak.service.locale.ILocalizationUnit;

import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;

public class ZonedDateTimeMatcher extends AbstractTypeMatcher<ZonedDateTime> {

    @Inject
    @LocaleUnit("default")
    private ILocalizationUnit defaultLocaleUnit;

    @Override
    protected ILocalizationUnit getLocaleUnit() {
        return defaultLocaleUnit;
    }

    @Override
    protected IMatcherResponse parse(ICommandExecutionContext ctx, IMatchingContext matchingContext, String extracted) {

        ZonedDateTime parsed;

        try {
            parsed = ZonedDateTime.parse(extracted);
        } catch (DateTimeParseException e) {
            return new BasicMatcherResponse(
                    MatcherResponseType.ERROR,
                    ctx.getParameterIndex().get(),
                    "The input is not in a valid date time format!"
            );
        }

        ctx.putOrReplaceOne(matchingContext.getArgumentOrParamName(), parsed);

        ctx.getParameterIndex().incrementAndGet();
        return BasicMatcherResponse.SUCCESS;
    }

    @Override
    public Class<ZonedDateTime> getSupportedType() {
        return ZonedDateTime.class;
    }
}
