package de.fearnixx.jeak.tournament.commands;

public abstract class CommandPermissions {

    private CommandPermissions() {
    }



    public static final String GENERATE_API_TOKEN = "tournaments.command.token";
    public static final String TOURNAMENT_INFO = "tournaments.command.tournament.info";
    public static final String TOURNAMENT_LIST_ENCOUNTERS = "tournaments.command.tournament.list_encounters";
    public static final String TOURNAMENT_CREATE = "tournaments.command.tournament.create";
    public static final String TOURNAMENT_PARAM = "tournaments.command.tournament.param";
    public static final String TOURNAMENT_DELETE = "tournaments.command.tournament.delete";
    public static final String TOURNAMENT_START = "tournaments.command.tournament.start";
    public static final String TOURNAMENT_FINISH = "tournaments.command.tournament.finish";
    public static final String TOURNAMENT_LEADER = "tournaments.command.tournament.leader";
    public static final String TOURNAMENT_JOIN = "tournaments.command.tournament.join";
    public static final String TOURNAMENT_LEAVE = "tournaments.command.tournament.leave";
    public static final String TOURNAMENT_READY = "tournaments.command.tournament.ready";
    public static final String TOURNAMENT_MESSAGE = "tournaments.command.tournament.message";
    public static final String TOURNAMENT_REROLL = "tournaments.command.tournament.reroll";
    public static final String TOURNAMENT_CLAIM = "tournaments.command.tournament.claim";
    public static final String TOURNAMENT_FORFEIT = "tournaments.command.tournament.forfeit";
    public static final String TOURNAMENT_DISQUALIFY = "tournaments.command.tournament.disqualify";
    public static final String TOURNAMENT_CLASSIFIER_CREATE = "tournaments.command.tournament.classifier.create";
    public static final String TOURNAMENT_CLASSIFIER_SET = "tournaments.command.tournament.classifier.set";
    public static final String TOURNAMENT_SEASON_CREATE = "tournaments.command.tournament.season.create";
    public static final String TOURNAMENT_SEASON_SET = "tournaments.command.tournament.season.set";

    public static final String TEAM_CREATE = "tournaments.command.team.create";
    public static final String TEAM_DELETE = "tournaments.command.team.delete";
    public static final String TEAM_RENAME = "tournaments.command.team.rename";
    public static final String TEAM_JOIN = "tournaments.command.team.join";
    public static final String TEAM_LEAVE = "tournaments.command.team.leave";
    public static final String TEAM_MEMBER_ADD = "tournaments.command.team.add_member";
    public static final String TEAM_MEMBER_DEL = "tournaments.command.team.del_member";
    public static final String TEAM_INFO = "tournaments.command.team.info";
    public static final String TEAM_TOKEN_JOIN = "tournaments.command.team.token";
    public static final String TEAM_TOKEN_LEADER = "tournaments.command.team.token_leader";
    public static final String TEAM_LEADER_DEL = "tournaments.command.team.leader_del";
    public static final String TEAM_MESSAGE = "tournaments.command.team.message";
    public static final String TEAM_CLAIM = "tournaments.command.team.claim";

    public static final String ENCOUNTER_INFO = "tournaments.command.encounter.info";
    public static final String ENCOUNTER_MESSAGE = "tournaments.command.encounter.message";
    public static final String ENCOUNTER_HOSTER = "tournaments.commmand.encounter.set_hoster";
    public static final String ENCOUNTER_START = "tournaments.command.encounter.start";
    public static final String ENCOUNTER_FINISH = "tournaments.command.encounter.finish";
    public static final String ENCOUNTER_EDIT_SCORES = "tournaments.command.encounter.edit_scores";
    public static final String ENCOUNTER_READY = "tournaments.command.encounter.ready";
    public static final String ENCOUNTER_FORFEIT = "tournaments.command.encounter.forfeit";
}
