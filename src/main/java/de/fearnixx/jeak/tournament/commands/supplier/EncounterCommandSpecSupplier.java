package de.fearnixx.jeak.tournament.commands.supplier;

import de.fearnixx.jeak.service.command.spec.Commands;
import de.fearnixx.jeak.service.command.spec.ICommandParamSpec;
import de.fearnixx.jeak.service.command.spec.ICommandSpec;
import de.fearnixx.jeak.tournament.commands.CommandParameters;
import de.fearnixx.jeak.tournament.commands.CommandPermissions;
import de.fearnixx.jeak.tournament.entities.encounter.Encounter;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.management.EncounterManagement;

import java.util.ArrayList;
import java.util.List;

public class EncounterCommandSpecSupplier extends AbstractCommandSpecSupplier {

    private static final String CMD_PREFIX = "encounter:";

    private final EncounterManagement encounterManagement;

    public EncounterCommandSpecSupplier(EncounterManagement encounterManagement) {
        this.encounterManagement = encounterManagement;
    }

    @Override
    public List<ICommandSpec> getAllCommandSpecs() {
        List<ICommandSpec> commandSpecs = new ArrayList<>();

        final ICommandParamSpec encounterParamSpec = Commands.paramSpec(CommandParameters.ENCOUNTER, Encounter.class);

        commandSpecs.add(Commands.commandSpec(CMD_PREFIX + "info")
                .parameters(encounterParamSpec)
                .executor(
                        ctx ->
                                executeInTransaction(
                                        () -> encounterManagement.encounterInfo(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.ENCOUNTER, Encounter.class)
                                        )
                                )
                )
                .permission(CommandPermissions.ENCOUNTER_INFO)
                .build()
        );

        commandSpecs.add(Commands.commandSpec(CMD_PREFIX + "message")
                .parameters(
                        encounterParamSpec,
                        Commands.paramSpec(CommandParameters.MESSAGE, String.class)
                )
                .executor(
                        ctx ->
                                executeInTransaction(
                                        () -> encounterManagement.messageEncounter(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.ENCOUNTER, Encounter.class),
                                                ctx.getRequiredOne(CommandParameters.MESSAGE, String.class)
                                        )
                                )
                )
                .permission(CommandPermissions.ENCOUNTER_MESSAGE)
                .build()
        );

        commandSpecs.add(Commands.commandSpec(CMD_PREFIX + "set-hoster")
                .parameters(
                        encounterParamSpec,
                        Commands.paramSpec(CommandParameters.HOSTER, String.class)
                )
                .executor(
                        ctx ->
                                executeInTransaction(
                                        () -> encounterManagement.messageEncounter(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.ENCOUNTER, Encounter.class),
                                                ctx.getRequiredOne(CommandParameters.HOSTER, String.class)
                                        )
                                )
                )
                .permission(CommandPermissions.ENCOUNTER_HOSTER)
                .build()
        );

        commandSpecs.add(Commands.commandSpec(CMD_PREFIX + "start")
                .parameters(encounterParamSpec)
                .executor(
                        ctx ->
                                executeInTransaction(
                                        () -> encounterManagement.startEncounter(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.ENCOUNTER, Encounter.class)
                                        )
                                )
                )
                .permission(CommandPermissions.ENCOUNTER_START)
                .build()
        );

        commandSpecs.add(Commands.commandSpec(CMD_PREFIX + "add-score")
                .parameters(
                        encounterParamSpec,
                        Commands.paramSpec(CommandParameters.SCORE, String.class)
                )
                .executor(
                        ctx ->
                                executeInTransaction(
                                        () -> encounterManagement.addMatch(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.ENCOUNTER, Encounter.class),
                                                ctx.getRequiredOne(CommandParameters.SCORE, String.class)
                                        )
                                )
                )
                .permission(CommandPermissions.ENCOUNTER_EDIT_SCORES)
                .build()
        );

        commandSpecs.add(Commands.commandSpec(CMD_PREFIX + "finish")
                .parameters(encounterParamSpec)
                .executor(
                        ctx ->
                                executeInTransaction(
                                        () -> encounterManagement.finishEncounter(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.ENCOUNTER, Encounter.class)
                                        )
                                )
                )
                .permission(CommandPermissions.ENCOUNTER_FINISH)
                .build()
        );

        commandSpecs.add(Commands.commandSpec(CMD_PREFIX + "ready")
                .parameters(encounterParamSpec, Commands.paramSpec(CommandParameters.TEAM, Team.class))
                .executor(
                        ctx ->
                                executeInTransaction(
                                        () -> encounterManagement.readyEncounter(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.ENCOUNTER, Encounter.class),
                                                ctx.getRequiredOne(CommandParameters.TEAM, Team.class)
                                        )
                                )
                )
                .permission(CommandPermissions.ENCOUNTER_READY)
                .build()
        );

        commandSpecs.add(Commands.commandSpec(CMD_PREFIX + "forfeit")
                .parameters(encounterParamSpec, Commands.paramSpec(CommandParameters.TEAM, Team.class))
                .executor(
                        ctx ->
                                executeInTransaction(
                                        () -> encounterManagement.forfeitEncounter(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.ENCOUNTER, Encounter.class),
                                                ctx.getRequiredOne(CommandParameters.TEAM, Team.class)
                                        )
                                )
                )
                .permission(CommandPermissions.ENCOUNTER_FORFEIT)
                .build()
        );

        return commandSpecs;
    }
}
