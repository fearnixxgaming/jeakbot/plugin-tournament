package de.fearnixx.jeak.tournament.commands.supplier;

import de.fearnixx.jeak.service.command.spec.Commands;
import de.fearnixx.jeak.service.command.spec.ICommandParamSpec;
import de.fearnixx.jeak.service.command.spec.ICommandSpec;
import de.fearnixx.jeak.tournament.commands.CommandParameters;
import de.fearnixx.jeak.tournament.commands.CommandPermissions;
import de.fearnixx.jeak.tournament.entities.Season;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.Tournament;
import de.fearnixx.jeak.tournament.management.TournamentManagement;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class TournamentCommandSpecSupplier extends AbstractCommandSpecSupplier {

    private static final String CMD_PREFIX = "tournament:";
    private final TournamentManagement tournamentManagement;

    public TournamentCommandSpecSupplier(TournamentManagement tournamentManagement) {
        this.tournamentManagement = tournamentManagement;
    }

    @Override
    public List<ICommandSpec> getAllCommandSpecs() {
        List<ICommandSpec> commandSpecs = new ArrayList<>();

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "create")
                        .parameters(Commands.paramSpec(CommandParameters.TOURNAMENT_NAME, String.class))
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.createTournament(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT_NAME, String.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_CREATE)
                        .build()
        );

        final ICommandParamSpec tournamentParamSpec = Commands.paramSpec(CommandParameters.TOURNAMENT, Tournament.class);
        final ICommandParamSpec teamParamSpec = Commands.paramSpec(CommandParameters.TEAM, Team.class);

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "delete")
                        .parameters(tournamentParamSpec)
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.deleteTournament(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_DELETE)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "join")
                        .parameters(tournamentParamSpec, teamParamSpec)
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.joinTournament(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class),
                                                ctx.getRequiredOne(CommandParameters.TEAM, Team.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_JOIN)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "leave")
                        .parameters(tournamentParamSpec, teamParamSpec)
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.leaveTournament(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class),
                                                ctx.getRequiredOne(CommandParameters.TEAM, Team.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_LEAVE)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "ready")
                        .parameters(tournamentParamSpec, teamParamSpec)
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.readyTournament(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class),
                                                ctx.getRequiredOne(CommandParameters.TEAM, Team.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_READY)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "info")
                        .parameters(tournamentParamSpec)
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.infoTournament(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_INFO)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "start")
                        .parameters(tournamentParamSpec)
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.startTournament(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_START)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "finish")
                        .parameters(tournamentParamSpec)
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.finishTournament(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_FINISH)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "message")
                        .parameters(
                                tournamentParamSpec,
                                Commands.paramSpec(CommandParameters.MESSAGE, String.class)
                        )
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.messageTournament(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class),
                                                ctx.getRequiredOne(CommandParameters.MESSAGE, String.class)

                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_MESSAGE)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "param")
                        .parameters(
                                tournamentParamSpec,
                                Commands.paramSpec(CommandParameters.PARAM_KEY, String.class),
                                Commands.paramSpec().optional(Commands.paramSpec(CommandParameters.PARAM_VALUE, String.class))
                        )
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.paramTournament(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class),
                                                ctx.getRequiredOne(CommandParameters.PARAM_KEY, String.class),
                                                ctx.getOne(CommandParameters.PARAM_VALUE, String.class).orElse(null)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_PARAM)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "leader-token")
                        .parameters(
                                tournamentParamSpec,
                                Commands.paramSpec().optional(Commands.paramSpec(CommandParameters.TOKEN, String.class))
                        )
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.leaderTournament(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class),
                                                ctx.getOne(CommandParameters.TOKEN, String.class).orElse(null)

                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_LEADER)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "leader-remove")
                        .parameters(
                                tournamentParamSpec,
                                Commands.paramSpec().optional(Commands.paramSpec(CommandParameters.LEADER_UUID_TO_REMOVE, String.class))
                        )
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.leaderRemove(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class),
                                                ctx.getOne(CommandParameters.LEADER_UUID_TO_REMOVE, String.class).orElse(null)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_LEADER)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "list-encounters")
                        .parameters(tournamentParamSpec)
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.listEncounters(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_LIST_ENCOUNTERS)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "re-roll-stage")
                        .parameters(tournamentParamSpec)
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.reRollCurrentStage(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_REROLL)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "claim-leadership").parameters(tournamentParamSpec)
                        .permission(CommandPermissions.TOURNAMENT_CLAIM)
                        .executor(
                                ctx -> executeInTransaction(
                                        () -> tournamentManagement.claimTournament(ctx.getInvokerUID(), ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class))
                                )
                        )
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "forfeit")
                        .parameters(tournamentParamSpec, teamParamSpec)
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.forfeitForTeam(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class),
                                                ctx.getRequiredOne(CommandParameters.TEAM, Team.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_FORFEIT)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "disqualify")
                        .parameters(tournamentParamSpec, teamParamSpec)
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.disqualifyTeam(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class),
                                                ctx.getRequiredOne(CommandParameters.TEAM, Team.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_DISQUALIFY)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "set-classifier")
                        .parameters(tournamentParamSpec, Commands.paramSpec(CommandParameters.CLASSIFIER, String.class))
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.setClassifier(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class),
                                                ctx.getRequiredOne(CommandParameters.CLASSIFIER, String.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_CLASSIFIER_SET)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "create-classifier")
                        .parameters(
                                Commands.paramSpec(CommandParameters.CLASSIFIER, String.class),
                                Commands.paramSpec(CommandParameters.DESCRIPTION, String.class)
                        )
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.createClassifier(
                                                ctx.getRequiredOne(CommandParameters.CLASSIFIER, String.class),
                                                ctx.getRequiredOne(CommandParameters.DESCRIPTION, String.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_CLASSIFIER_CREATE)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "set-season")
                        .parameters(tournamentParamSpec, Commands.paramSpec(CommandParameters.SEASON_NAME, String.class))
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.setSeason(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TOURNAMENT, Tournament.class),
                                                ctx.getRequiredOne(CommandParameters.SEASON_NAME, Season.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_SEASON_SET)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "create-season")
                        .parameters(
                                Commands.paramSpec(CommandParameters.SEASON_NAME, String.class),
                                Commands.paramSpec(CommandParameters.SEASON_START_DATE, ZonedDateTime.class),
                                Commands.paramSpec(CommandParameters.SEASON_END_DATE, ZonedDateTime.class)
                        )
                        .executor(ctx ->
                                executeInTransaction(
                                        () -> tournamentManagement.createSeason(
                                                ctx.getRequiredOne(CommandParameters.SEASON_NAME, String.class),
                                                ctx.getRequiredOne(CommandParameters.SEASON_START_DATE, ZonedDateTime.class),
                                                ctx.getRequiredOne(CommandParameters.SEASON_END_DATE, ZonedDateTime.class)
                                        )
                                ))
                        .permission(CommandPermissions.TOURNAMENT_SEASON_CREATE)
                        .build()
        );


        return commandSpecs;
    }
}
