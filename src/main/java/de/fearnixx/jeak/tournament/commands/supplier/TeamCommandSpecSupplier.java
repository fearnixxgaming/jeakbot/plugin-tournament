package de.fearnixx.jeak.tournament.commands.supplier;

import de.fearnixx.jeak.service.command.spec.Commands;
import de.fearnixx.jeak.service.command.spec.ICommandParamSpec;
import de.fearnixx.jeak.service.command.spec.ICommandSpec;
import de.fearnixx.jeak.tournament.commands.CommandParameters;
import de.fearnixx.jeak.tournament.commands.CommandPermissions;
import de.fearnixx.jeak.tournament.entities.team.Player;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.management.TeamManagement;

import java.util.ArrayList;
import java.util.List;

public class TeamCommandSpecSupplier extends AbstractCommandSpecSupplier {

    private static final String CMD_PREFIX = "team:";

    private final TeamManagement teamManagement;

    public TeamCommandSpecSupplier(TeamManagement teamManagement) {
        this.teamManagement = teamManagement;
    }

    @Override
    public List<ICommandSpec> getAllCommandSpecs() {
        List<ICommandSpec> commandSpecs = new ArrayList<>();

        final ICommandParamSpec teamParamSpec = Commands.paramSpec(CommandParameters.TEAM, Team.class);
        final ICommandParamSpec tokenParamSpec = Commands.paramSpec(CommandParameters.TOKEN, String.class);

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "create")
                        .parameters(Commands.paramSpec(CommandParameters.TEAM_NAME, String.class))
                        .executor(
                                ctx -> executeInTransaction(
                                        () -> teamManagement.createTeam(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TEAM_NAME, String.class)
                                        )
                                ))
                        .permission(CommandPermissions.TEAM_CREATE)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "delete")
                        .parameters(teamParamSpec)
                        .executor(
                                ctx -> executeInTransaction(
                                        () ->
                                                teamManagement.deleteTeam(
                                                        ctx.getInvokerUID(),
                                                        ctx.getRequiredOne(CommandParameters.TEAM, Team.class)
                                                )
                                ))
                        .permission(CommandPermissions.TEAM_DELETE)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "rename")
                        .parameters(
                                teamParamSpec,
                                Commands.paramSpec(CommandParameters.TEAM_NAME, String.class)
                        )
                        .executor(
                                ctx -> executeInTransaction(
                                        () ->
                                                teamManagement.renameTeam(
                                                        ctx.getInvokerUID(),
                                                        ctx.getRequiredOne(CommandParameters.TEAM, Team.class),
                                                        ctx.getRequiredOne(CommandParameters.TEAM_NAME, String.class)
                                                )
                                ))
                        .permission(CommandPermissions.TEAM_RENAME)
                        .build()
        );


        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "join")
                        .parameters(
                                teamParamSpec,
                                tokenParamSpec
                        )
                        .executor(
                                ctx -> executeInTransaction(
                                        () ->
                                                teamManagement.joinTeam(
                                                        ctx.getInvokerUID(),
                                                        ctx.getRequiredOne(CommandParameters.TEAM, Team.class),
                                                        ctx.getRequiredOne(CommandParameters.TOKEN, String.class)
                                                )
                                ))
                        .permission(CommandPermissions.TEAM_JOIN)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "add-member")
                        .parameters(
                                teamParamSpec,
                                Commands.paramSpec(CommandParameters.PLAYER_UID, String.class),
                                Commands.paramSpec(CommandParameters.PLAYER_NAME, String.class)
                        )
                        .executor(
                                ctx -> executeInTransaction(
                                        () ->
                                                teamManagement.addTeamMember(
                                                        ctx.getInvokerUID(),
                                                        ctx.getRequiredOne(CommandParameters.TEAM, Team.class),
                                                        ctx.getOne(CommandParameters.PLAYER_UID, String.class).orElse(null),
                                                        ctx.getOne(CommandParameters.PLAYER_NAME, String.class).orElse(null)
                                                )
                                )
                        )
                        .permission(CommandPermissions.TEAM_MEMBER_ADD)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "remove-member")
                        .parameters(
                                teamParamSpec,
                                Commands.paramSpec(CommandParameters.PLAYER, Player.class)
                        )
                        .executor(
                                ctx -> executeInTransaction(
                                        () ->
                                                teamManagement.removeTeamMember(
                                                        ctx.getInvokerUID(),
                                                        ctx.getRequiredOne(CommandParameters.TEAM, Team.class),
                                                        ctx.getRequiredOne(CommandParameters.PLAYER, Player.class)
                                                )
                                ))
                        .permission(CommandPermissions.TEAM_MEMBER_DEL)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "edit-player")
                        .parameters(
                                teamParamSpec,
                                Commands.paramSpec(CommandParameters.PLAYER, Player.class),
                                Commands.paramSpec(CommandParameters.PLAYER_UID, String.class),
                                Commands.paramSpec(CommandParameters.PLAYER_NAME, String.class)
                        )
                        .executor(
                                ctx -> executeInTransaction(
                                        () ->
                                                teamManagement.setPlayerInfo(
                                                        ctx.getInvokerUID(),
                                                        ctx.getRequiredOne(CommandParameters.TEAM, Team.class),
                                                        ctx.getRequiredOne(CommandParameters.PLAYER, Player.class),
                                                        ctx.getRequiredOne(CommandParameters.PLAYER_UID, String.class),
                                                        ctx.getRequiredOne(CommandParameters.PLAYER_NAME, String.class),
                                                        Boolean.parseBoolean(ctx.getRequiredOne(CommandParameters.PLAYER_ACTIVE, String.class))
                                                )
                                ))
                        .permission(CommandPermissions.TEAM_MEMBER_DEL)
                        .build()
        );


        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "leave")
                        .parameters(teamParamSpec)
                        .executor(
                                ctx -> executeInTransaction(
                                        () ->
                                                teamManagement.leaveTeam(
                                                        ctx.getInvokerUID(),
                                                        ctx.getRequiredOne(CommandParameters.TEAM, Team.class)
                                                )
                                ))
                        .permission(CommandPermissions.TEAM_LEAVE)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "info")
                        .parameters(teamParamSpec)
                        .executor(
                                ctx -> executeInTransaction(
                                        () ->
                                                teamManagement.infoTeam(
                                                        ctx.getInvokerUID(),
                                                        ctx.getRequiredOne(CommandParameters.TEAM, Team.class)
                                                )
                                ))
                        .permission(CommandPermissions.TEAM_INFO)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "token")
                        .parameters(
                                teamParamSpec,
                                Commands.paramSpec().optional(tokenParamSpec)
                        )
                        .executor(
                                ctx -> executeInTransaction(
                                        () ->
                                                teamManagement.token(
                                                        ctx.getInvokerUID(),
                                                        ctx.getRequiredOne(CommandParameters.TEAM, Team.class),
                                                        ctx.getOne(CommandParameters.TOKEN, String.class).orElse(null)
                                                )
                                ))
                        .permission(CommandPermissions.TEAM_TOKEN_JOIN)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "leader-token")
                        .parameters(
                                teamParamSpec,
                                Commands.paramSpec().optional(tokenParamSpec)
                        )
                        .executor(
                                ctx -> executeInTransaction(
                                        () ->
                                                teamManagement.leaderToken(
                                                        ctx.getInvokerUID(),
                                                        ctx.getRequiredOne(CommandParameters.TEAM, Team.class),
                                                        ctx.getOne(CommandParameters.TOKEN, String.class).orElse(null)
                                                )
                                ))
                        .permission(CommandPermissions.TEAM_TOKEN_LEADER)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "leader-remove")
                        .parameters(
                                teamParamSpec,
                                Commands.paramSpec().optional(Commands.paramSpec(CommandParameters.LEADER_UUID_TO_REMOVE, String.class))
                        )
                        .executor(
                                ctx -> executeInTransaction(
                                        () ->
                                                teamManagement.leaderRemove(
                                                        ctx.getInvokerUID(),
                                                        ctx.getRequiredOne(CommandParameters.TEAM, Team.class),
                                                        ctx.getOne(CommandParameters.LEADER_UUID_TO_REMOVE, String.class).orElse(null)
                                                )
                                ))
                        .permission(CommandPermissions.TEAM_LEADER_DEL)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "message")
                        .parameters(
                                teamParamSpec,
                                Commands.paramSpec(CommandParameters.MESSAGE, String.class)
                        )
                        .executor(
                                ctx -> executeInTransaction(
                                        () ->
                                                teamManagement.messageTeam(
                                                        ctx.getInvokerUID(),
                                                        ctx.getRequiredOne(CommandParameters.TEAM, Team.class),
                                                        ctx.getRequiredOne(CommandParameters.MESSAGE, String.class)
                                                )
                                ))
                        .permission(CommandPermissions.TEAM_MESSAGE)
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "claim-leadership").parameters(teamParamSpec)
                        .permission(CommandPermissions.TEAM_CLAIM)
                        .executor(
                                ctx -> executeInTransaction(
                                        () -> teamManagement.claimTeam(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TEAM, Team.class)
                                        )
                                )
                        )
                        .build()
        );

        commandSpecs.add(
                Commands.commandSpec(CMD_PREFIX + "set-tag")
                        .parameters(
                                teamParamSpec,
                                Commands.paramSpec(CommandParameters.TEAM_TAG, String.class)
                        )
                        .permission(CommandPermissions.TEAM_CREATE)
                        .executor(
                                ctx -> executeInTransaction(
                                        () -> teamManagement.setTag(
                                                ctx.getInvokerUID(),
                                                ctx.getRequiredOne(CommandParameters.TEAM, Team.class),
                                                ctx.getRequiredOne(CommandParameters.TEAM_TAG, String.class)
                                        )
                                )
                        )
                        .build()
        );

        return commandSpecs;
    }
}
