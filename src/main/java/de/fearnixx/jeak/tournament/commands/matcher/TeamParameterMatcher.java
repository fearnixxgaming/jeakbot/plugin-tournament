package de.fearnixx.jeak.tournament.commands.matcher;

import de.fearnixx.jeak.service.command.ICommandExecutionContext;
import de.fearnixx.jeak.service.command.spec.matcher.BasicMatcherResponse;
import de.fearnixx.jeak.service.command.spec.matcher.IMatcherResponse;
import de.fearnixx.jeak.service.command.spec.matcher.IMatchingContext;
import de.fearnixx.jeak.service.command.spec.matcher.MatcherResponseType;
import de.fearnixx.jeak.tournament.commands.CommandParameters;
import de.fearnixx.jeak.tournament.entities.team.Team;

import java.util.Optional;

public class TeamParameterMatcher extends AbstractTournamentEntityMatcher<Team> {

    @Override
    protected IMatcherResponse doParse(ICommandExecutionContext ctx, IMatchingContext matchingContext, String extracted) {
        final Optional<Team> team = entityManagement.getTeamByName(extracted);

        if (team.isEmpty()) {
            return new BasicMatcherResponse(
                    MatcherResponseType.ERROR,
                    ctx.getParameterIndex().get(),
                    "The team '" + extracted + "' does not exist!"
            );
        }

        ctx.putOrReplaceOne(CommandParameters.TEAM, team.get());

        ctx.getParameterIndex().incrementAndGet();
        return BasicMatcherResponse.SUCCESS;
    }

    @Override
    public Class<Team> getSupportedType() {
        return Team.class;
    }
}
