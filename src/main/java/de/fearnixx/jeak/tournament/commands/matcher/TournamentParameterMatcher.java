package de.fearnixx.jeak.tournament.commands.matcher;

import de.fearnixx.jeak.service.command.ICommandExecutionContext;
import de.fearnixx.jeak.service.command.spec.matcher.BasicMatcherResponse;
import de.fearnixx.jeak.service.command.spec.matcher.IMatcherResponse;
import de.fearnixx.jeak.service.command.spec.matcher.IMatchingContext;
import de.fearnixx.jeak.service.command.spec.matcher.MatcherResponseType;
import de.fearnixx.jeak.tournament.commands.CommandParameters;
import de.fearnixx.jeak.tournament.entities.tournament.Tournament;

import java.util.Optional;

public class TournamentParameterMatcher extends AbstractTournamentEntityMatcher<Tournament> {

    @Override
    protected IMatcherResponse doParse(ICommandExecutionContext ctx, IMatchingContext matchingContext, String extracted) {
        final Optional<Tournament> tournament = entityManagement.getTournamentByName(extracted);

        if (tournament.isEmpty()) {
            return new BasicMatcherResponse(
                    MatcherResponseType.ERROR,
                    ctx.getParameterIndex().get(),
                    "The tournament '" + extracted + "' does not exist!"
            );
        }

        ctx.putOrReplaceOne(CommandParameters.TOURNAMENT, tournament.get());

        ctx.getParameterIndex().incrementAndGet();
        return BasicMatcherResponse.SUCCESS;
    }

    @Override
    public Class<Tournament> getSupportedType() {
        return Tournament.class;
    }
}
