package de.fearnixx.jeak.tournament.enums;

public enum ParticipationState {
    //Eligible to play
    JOINED,
    READY,
    ACTIVE,
    WON,
    //Not eligible to play
    KNOCKED_OUT,
    FORFEITED,
    DISQUALIFIED
}
