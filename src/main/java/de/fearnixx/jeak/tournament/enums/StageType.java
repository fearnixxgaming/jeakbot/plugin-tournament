package de.fearnixx.jeak.tournament.enums;

public enum StageType {
    KO,
    GR(true),
    TFT(true),
    UNDEFINED;

    boolean isGroup;

    StageType() {
    }

    StageType(boolean isGroup) {
        this.isGroup = isGroup;
    }

    public boolean isGroup() {
        return isGroup;
    }
}