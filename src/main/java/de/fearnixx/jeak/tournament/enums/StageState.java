package de.fearnixx.jeak.tournament.enums;

public enum StageState {
    CREATED,
    STARTED,
    FINISHED
}
