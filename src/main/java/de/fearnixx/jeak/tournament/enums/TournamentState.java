package de.fearnixx.jeak.tournament.enums;

public enum TournamentState {
    CREATED,
    STARTED,
    FINISHED
}
