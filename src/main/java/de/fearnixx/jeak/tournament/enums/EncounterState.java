package de.fearnixx.jeak.tournament.enums;

public enum EncounterState {
    CREATED,
    PREPARING,
    READY,
    STARTED,
    FINISHED,
    FORFEITED
}
