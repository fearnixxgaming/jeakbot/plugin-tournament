package de.fearnixx.jeak.tournament;

import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.reflect.*;
import de.fearnixx.jeak.service.IServiceManager;
import de.fearnixx.jeak.service.command.ICommandService;
import de.fearnixx.jeak.service.command.spec.Commands;
import de.fearnixx.jeak.service.command.spec.matcher.IMatcherRegistryService;
import de.fearnixx.jeak.service.controller.IRestControllerService;
import de.fearnixx.jeak.teamspeak.IServer;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.tournament.commands.matcher.*;
import de.fearnixx.jeak.tournament.commands.supplier.EncounterCommandSpecSupplier;
import de.fearnixx.jeak.tournament.commands.supplier.TeamCommandSpecSupplier;
import de.fearnixx.jeak.tournament.commands.supplier.TokenCommandSupplier;
import de.fearnixx.jeak.tournament.commands.supplier.TournamentCommandSpecSupplier;
import de.fearnixx.jeak.tournament.config.TournamentConfigService;
import de.fearnixx.jeak.tournament.controller.*;
import de.fearnixx.jeak.tournament.controller.serialization.*;
import de.fearnixx.jeak.tournament.entities.team.Team;
import de.fearnixx.jeak.tournament.entities.tournament.Tournament;
import de.fearnixx.jeak.tournament.enums.EncounterState;
import de.fearnixx.jeak.tournament.management.EncounterManagement;
import de.fearnixx.jeak.tournament.management.TeamManagement;
import de.fearnixx.jeak.tournament.management.TournamentManagement;
import de.fearnixx.jeak.tournament.management.services.TournamentChannelManagementService;
import de.fearnixx.jeak.tournament.management.services.TournamentEntityManagementService;
import de.fearnixx.jeak.tournament.management.services.TournamentMessagingService;
import de.fearnixx.jeak.tournament.management.services.TournamentStageGenerationService;
import de.fearnixx.jeak.tournament.requestqueue.ClientRequestQueue;
import de.fearnixx.jeak.tournament.stagepattern.TournamentStageManagementService;
import de.fearnixx.jeak.util.Configurable;
import de.mlessmann.confort.api.IConfig;
import de.mlessmann.confort.api.IConfigNode;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnit;
import java.util.List;
import java.util.Optional;

@JeakBotPlugin(id = "tournament")
public class TournamentPlugin extends Configurable {

    private static final String DEFAULT_CONFIG_URI = "/tournament/defaultConfig.json";

    @Inject
    @Config
    private IConfig configRef;

    @Inject
    @PersistenceUnit(name = "tournaments")
    private EntityManager entityManager;

    @Inject
    private IInjectionService injectionService;

    @Inject
    private ICommandService commandService;

    @Inject
    private IServer server;

    @Inject
    private IServiceManager serviceManager;

    @Inject
    private IMatcherRegistryService matcherRegistryService;

    @Inject
    private IRestControllerService controllerService;

    //No injection, since we are creating them in this class
    private TournamentConfigService configService;
    private TournamentChannelManagementService channelManagement;
    private TournamentEntityManagementService entityManagement;

    private ClientRequestQueue clientRequestQueue;

    private TeamManagement teamManagement;
    private TournamentManagement tournamentManagement;
    private EncounterManagement encounterManagement;

    @Listener
    public void onInitialize(IBotStateEvent.IInitializeEvent initializeEvent) {
        if (!loadConfig()) {
            initializeEvent.cancel();
        }

        initializeServices();
        initializeManagementClasses();
        initializeParameterMatcher();
        initializeCommandReceivers();

        registerRestControllers();

        entityManagement.getDeleteLeaderTokenService().clearAllTokens();
    }

    private void initializeServices() {

        //Entity-Management-Service
        final TournamentEntityManagementService entityManagementService = new TournamentEntityManagementService(entityManager);
        injectionService.injectInto(entityManagementService);
        serviceManager.registerService(TournamentEntityManagementService.class, entityManagementService);
        this.entityManagement = entityManagementService;

        //Config-Service
        final TournamentConfigService tournamentConfigService = new TournamentConfigService(configRef);
        serviceManager.registerService(TournamentConfigService.class, tournamentConfigService);
        this.configService = tournamentConfigService;

        //Stage-Management-Service
        final TournamentStageManagementService stageManagementService = new TournamentStageManagementService();
        serviceManager.registerService(TournamentStageManagementService.class, stageManagementService);

        //Channel-Management-Service (depends on config- and entity-management-service)
        final TournamentChannelManagementService channelManagementService = new TournamentChannelManagementService();
        injectionService.injectInto(channelManagementService);
        serviceManager.registerService(TournamentChannelManagementService.class, channelManagementService);
        this.channelManagement = channelManagementService;

        //Messaging-Service (depends on entity-management-service)
        this.clientRequestQueue = new ClientRequestQueue();
        injectionService.injectInto(clientRequestQueue);
        clientRequestQueue.initialize();

        final TournamentMessagingService messagingService = new TournamentMessagingService(clientRequestQueue);
        injectionService.injectInto(messagingService);
        serviceManager.registerService(TournamentMessagingService.class, messagingService);

        //Encounter-Generation-Service (depends on messaging- and entity-management-service)
        final TournamentStageGenerationService encounterGenerationService = new TournamentStageGenerationService();
        injectionService.injectInto(encounterGenerationService);
        serviceManager.registerService(TournamentStageGenerationService.class, encounterGenerationService);

        //Serializations
        final EncounterParticipationSerialization encounterParticipationSerialization = new EncounterParticipationSerialization();
        injectionService.injectInto(encounterParticipationSerialization);
        serviceManager.registerService(EncounterParticipationSerialization.class, encounterParticipationSerialization);

        final MatchSerialization matchSerialization = new MatchSerialization();
        injectionService.injectInto(matchSerialization);
        serviceManager.registerService(MatchSerialization.class, matchSerialization);

        final EncounterSerialization encounterSerialization = new EncounterSerialization();
        injectionService.injectInto(encounterSerialization);
        serviceManager.registerService(EncounterSerialization.class, encounterSerialization);

        final GroupSerialization groupSerialization = new GroupSerialization();
        injectionService.injectInto(groupSerialization);
        serviceManager.registerService(GroupSerialization.class, groupSerialization);

        final StageSerialization stageSerialization = new StageSerialization();
        injectionService.injectInto(stageSerialization);
        serviceManager.registerService(StageSerialization.class, stageSerialization);

        final SeasonSerialization seasonSerialization = new SeasonSerialization();
        injectionService.injectInto(seasonSerialization);
        serviceManager.registerService(SeasonSerialization.class, seasonSerialization);

        final TournamentSerialization tournamentSerialization = new TournamentSerialization();
        injectionService.injectInto(tournamentSerialization);
        serviceManager.registerService(TournamentSerialization.class, tournamentSerialization);

        final TeamSerialization teamSerialization = new TeamSerialization();
        injectionService.injectInto(teamSerialization);
        serviceManager.registerService(TeamSerialization.class, teamSerialization);
    }

    private void initializeManagementClasses() {
        this.teamManagement = new TeamManagement();
        injectionService.injectInto(teamManagement);
        teamManagement.init();

        this.tournamentManagement = new TournamentManagement();
        injectionService.injectInto(tournamentManagement);
        tournamentManagement.init();

        this.encounterManagement = new EncounterManagement();
        injectionService.injectInto(encounterManagement);
    }

    private void initializeParameterMatcher() {
        final TeamParameterMatcher teamParameterMatcher = new TeamParameterMatcher();
        injectionService.injectInto(teamParameterMatcher);
        matcherRegistryService.registerMatcher(teamParameterMatcher);

        final TournamentParameterMatcher tournamentParameterMatcher = new TournamentParameterMatcher();
        injectionService.injectInto(tournamentParameterMatcher);
        matcherRegistryService.registerMatcher(tournamentParameterMatcher);

        final EncounterParameterMatcher encounterParameterMatcher = new EncounterParameterMatcher();
        injectionService.injectInto(encounterParameterMatcher);
        matcherRegistryService.registerMatcher(encounterParameterMatcher);

        final PlayerParameterMatcher playerParameterMatcher = new PlayerParameterMatcher();
        injectionService.injectInto(playerParameterMatcher);
        matcherRegistryService.registerMatcher(playerParameterMatcher);

        final ZonedDateTimeMatcher zonedDateTimeMatcher = new ZonedDateTimeMatcher();
        injectionService.injectInto(zonedDateTimeMatcher);
        matcherRegistryService.registerMatcher(zonedDateTimeMatcher);
    }

    private void initializeCommandReceivers() {

        final TournamentCommandSpecSupplier tournamentCommandSpecSupplier =
                new TournamentCommandSpecSupplier(tournamentManagement);
        injectionService.injectInto(tournamentCommandSpecSupplier);
        tournamentCommandSpecSupplier.getAllCommandSpecs()
                .forEach(commandService::registerCommand);

        final TeamCommandSpecSupplier teamCommandSpecSupplier =
                new TeamCommandSpecSupplier(teamManagement);
        injectionService.injectInto(teamCommandSpecSupplier);
        teamCommandSpecSupplier.getAllCommandSpecs()
                .forEach(commandService::registerCommand);

        final EncounterCommandSpecSupplier encounterCommandSpecSupplier =
                new EncounterCommandSpecSupplier(encounterManagement);
        injectionService.injectInto(encounterCommandSpecSupplier);
        encounterCommandSpecSupplier.getAllCommandSpecs()
                .forEach(commandService::registerCommand);

        commandService.registerCommand(
                Commands.commandSpec("tournament:refresh-config").executor(
                        ctx -> {
                            loadConfig();
                            configService.refresh();
                        }
                ).build()
        );

        final TokenCommandSupplier tokenCommandExecutor = new TokenCommandSupplier();
        injectionService.injectInto(tokenCommandExecutor);
        tokenCommandExecutor.getAllCommandSpecs().forEach(commandService::registerCommand);
    }

    private void registerRestControllers() {
        final TokenAuthController tokenAuthController = new TokenAuthController();
        injectionService.injectInto(tokenAuthController);
        controllerService.registerController(TokenAuthController.class, tokenAuthController);
        tokenAuthController.init();

        final TeamController teamController = new TeamController(teamManagement);
        injectionService.injectInto(teamController);
        controllerService.registerController(TeamController.class, teamController);
        teamController.init();

        final TournamentController tournamentController = new TournamentController(tournamentManagement);
        injectionService.injectInto(tournamentController);
        controllerService.registerController(TournamentController.class, tournamentController);
        tournamentController.init();

        final EncounterController encounterController = new EncounterController(encounterManagement);
        injectionService.injectInto(encounterController);
        controllerService.registerController(EncounterController.class, encounterController);
        encounterController.init();

        final SeasonController seasonController = new SeasonController(tournamentManagement);
        injectionService.injectInto(seasonController);
        controllerService.registerController(SeasonController.class, seasonController);
        seasonController.init();
    }

    @Listener
    public void onClientEnter(IQueryEvent.INotification.IClientEnter event) {
        IClient client = event.getTarget();

        if (clientRequestQueue.hasStoredRequests(client)) {
            clientRequestQueue.executeClientRequests(client);
        }

        synchronized (entityManagement.getEntityManager()) {
            EntityManager eManager = entityManagement.getEntityManager();
            eManager.getTransaction().begin();

            try {

                for (Tournament tournament : entityManagement.getRunningTournaments()) {

                    final List<Team> teams = tournament.getTeams();
                    final Optional<Team> team = teams.stream()
                            .filter(t -> t.optTeamMembershipByTsUid(client.getClientUniqueID()).isPresent())
                            .findFirst();

                    if (team.isPresent() &&
                            tournament.getCurrentStage().getEncounters()
                                    .stream()
                                    .filter(e -> !e.getState().equals(EncounterState.FINISHED))
                                    .anyMatch(e -> e.getTeams().contains(team.get())
                                    )
                    ) {

                        channelManagement.getTeamChannel(tournament, team.get()).ifPresent(
                                channel -> server.getConnection().sendRequest(
                                        client.moveToChannel(channel.getID())
                                )
                        );

                        break;
                    }
                }

                eManager.getTransaction().commit();
            } catch (
                    Exception e) {
                eManager.getTransaction().rollback();
            }
        }
    }

    public TournamentPlugin() {
        super(TournamentPlugin.class);
    }

    @Override
    protected String getDefaultResource() {
        return DEFAULT_CONFIG_URI;
    }

    @Override
    protected IConfigNode getConfig() {
        return configRef.getRoot();
    }

    @Override
    protected IConfig getConfigRef() {
        return configRef;
    }

    @Override
    protected boolean populateDefaultConf(IConfigNode root) {
        return false;
    }
}
