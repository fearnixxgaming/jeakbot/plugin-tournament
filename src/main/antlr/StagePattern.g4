grammar StagePattern;

BraceOpen: '(';
BraceClosing: ')';
Colon: ':';
Comma: ',';
Number: [0-9]+;
Identifier: [a-zA-Z]+;
Asterisk: '*';
WS: [\n\r \t]+ -> skip;

stagePattern
    : stageDef+ EOF;

stageDef
    : stage Number
    | stage Asterisk EOF;

stage
    : BraceOpen Identifier (Colon params)? BraceClosing;

params
    : Number (Comma Number)*;